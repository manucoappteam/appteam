<!DOCTYPE html>
<html ng-app="app">
    <head>
        <title>{{env('NAME')}}</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
      	<link href="{{ asset('/css/all.css') }}" rel="stylesheet">
      	<link rel="icon" href="{{asset('photo/icon.png')}}" /><!--include shortcut icon-->
    </head>
    <body>
	<header>
		@if(Auth::user())
		<input type="hidden" id="global_link" value="{{url('/')}}">
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation" >
		  <div class="container">
		    <!-- Grupowanie "marki" i przycisku rozwijania mobilnego menu -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Rozwiń nawigację</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="{{url('/home')}}"><span>Stage</span>Team</a>
		    </div>
		
		    <!-- Grupowanie elementów menu w celu lepszego wyświetlania na urządzeniach moblinych -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		    		<ul class="nav navbar-nav">
		    			<li><a href="{{url('/events_list')}}">Kalendarz</a></li>
		    			@can('show_equipment')
				          	<li><a href="{{url('/equipment')}}">Sprzęt</a></li>
				       	@endcan
		    			@if(Auth::user()->can('users'))
				        <li class="dropdown">
				          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Administracja <span class="caret"></span></a>
				          <ul class="dropdown-menu" role="menu">
				          	@can('users')
				          	<li><a href="{{url('/users')}}">Edytuj użytkowników</a></li>
				            <li><a href="{{url('/users/register')}}">Dodaj użytkownika</a></li>
				            @endcan
				            @can('roles')
				            <li class="divider"></li>
				            <li><a href="{{url('/roles')}}">Role</a></li>
				            @endcan
				            @can('history')
				            <li class="divider"></li>
				            <li><a href="{{url('/history')}}">Historia</a></li>
				            @endcan
				          </ul>
				        </li>
				        @endif
				        @can('absence')
				          	<li><a href="{{url('/absence')}}">Moje nieobecności</a></li>
				       	@endcan
				       	@can('timework')
				          	<li><a href="{{url('/timework')}}">Mój czas pracy</a></li>
				       	@endcan
				       	@can('timeworks')
				          	<li><a href="{{url('/timeworks')}}">Czas pracy ekipy</a></li>
				       	@endcan
				    </ul>
		    	    <ul class="nav navbar-nav navbar-right">
		    	    	<li class="dropdown mega-dropdown alerts"  ng-controller="alerts">
				          <a href="#" class="dropdown-toggle"  ng-class="a_act" ng-click="show_alerts()"><img class="icon"src="{{asset('icon/bells14-inver.svg')}}"><span class="count" ng-class="counted" style="display:none;"><%alerts.count%></span></a>
				          <ul class="dropdown-menu mega-dropdown-menu" >
				          	<li ng-repeat="alert in alerts.alerts">
				          		<h4 ng-if="alert.type=='add_to_event'">Zostałeś dodany do wydarzenia <a href="{{url('events')}}/<%alert.event.id%>"><%alert.event.name%></a></h4>
				          		<h4 ng-if="alert.type=='events'">Przypomnienie o wydarzeniu <a href="{{url('events')}}/<%alert.event.id%>"><%alert.event.name%></a></h4>
				          		<h4 ng-if="alert.type=='del_from_event'">Zostałeś usunięty z wydarzenia <%alert.event.name%></h4>
				          		<h4 ng-if="alert.type=='add_rider_to_event'">Dodano rider do wydarzenia <a href="{{url('events')}}/<%alert.event.id%>"><%alert.event.name%></a></h4>
				          		<h4 ng-if="alert.type=='add_comment'">Dodano komentarz do wydarzenia <a href="{{url('events')}}/<%alert.event.id%>"><%alert.event.name%></a></h4>
				          		<h4 ng-if="alert.type=='add_inputlist_to_event'">Dodano inputlistę do wydarzenia <a href="{{url('events')}}/<%alert.event.id%>"><%alert.event.name%></a></h4>
				          	    <h4 ng-if="alert.type=='user_ok_on_event'">Użytkownik <%alert.user_about%> zaakceptował udział w wydarzeniu <a href="{{url('events')}}/<%alert.event.id%>"><%alert.event.name%></a></h4>
				          		<h4 ng-if="alert.type=='user_no_on_event'">Użytkownik <%alert.user_about%> odrzucił udział w wydarzeniu <a href="{{url('events')}}/<%alert.event.id%>"><%alert.event.name%></a></h4>
				          		<h4 ng-if="alert.type=='add_event'"><p>Dodano wydarzenie:</p><a href="{{url('events')}}/<%alert.event.id%>"><%alert.event.name%></a></h4>
				          		<h4 ng-if="alert.type=='confirm_event'"><p>Potwierdzono wydarzenie:</p><a href="{{url('events')}}/<%alert.event.id%>"><%alert.event.name%></a></h4>
				          		 <div class="" ng-if="alert.type=='add_to_event'&&alert.pot==null">
				          		 	<button class="btn btn-info" ng-click="user_ok(alert.event_id)">Potwierdź udział</button>
				          		 	<button class="btn btn-info" ng-click="user_no(alert.event_id)">Odrzuć</button>
				          		 </div>
				          		 <button type="button" class="close" ng-click="del_alert(alert.id)">&times;</button>
				          		
				          	</li>
				          </ul>
				        </li>
				        <li class="dropdown profil" ng-controller="user">
				          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Profil <span class="caret"></span></a>
				          <ul class="dropdown-menu" role="menu">
				          	<li class="col-sm-4">
				          		<img ng-src="{{asset('img/')}}/<%user_activ.img%>" ng-if="user_activ.img!=''" class="img-responsive img-circle">
				          		<img ng-src="{{asset('img/')}}/default.png" ng-if="user_activ.img==''" class="img-responsive img-circle">
				          	</li>
				          	<li class="col-sm-8 info"><a href="{{url('/profile/show')}}"><p><%user_activ.name%><p><%user_activ.email%></p></a></li>				          
				            <li class="col-sm-6 pr"><a href="{{url('/profile/edit')}}" class="btn">Edytuj<img class="icon a" src="{{asset('icon/pencil78_inver.svg')}}"><img class="icon b" src="{{asset('icon/pencil78.svg')}}" style="display: none"></a></li>
				            <li class="col-sm-6 pr"><a href="{{url('/logout')}}" class="btn">Wyloguj<img class="icon a" src="{{asset('icon/rightarrow97_inver.svg')}}"><img class="icon b" src="{{asset('icon/rightarrow97.svg')}}" style="display:none;"></a></li>
				       
				          </ul>
				        </li>
				    </ul>
		    </div>
		  </div>
		</nav>
		@else
		@endif
	</header>
	@if(Session('info'))
	<div class="alert container alert-success alert-dismissable">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {{Session('info')}}
	</div>
	@endif
   @yield('content')
   @if(Auth::user())
   <footer>
   	@section('footer')
	<div class="container">
		<div class="col-sm-2">
			<a href="http://manuco.pl"><img class="img-responsive" src="{{url('photo/logo_footer.png')}}"></a>
		</div>
		<div class="col-sm-6  col-sm-offset-4" style="margin-bottom:10px; text-align: right">
		<h5>System w wersji testowej,<br> wszelkie problemy prosimy zgłaszać na:</h5>	
		<p><a href="mailto:ppiskorek@manuco.pl" style="color:#fff">ppiskorek@manuco.pl</a></p>
		<p><a href="mailto:bedziecha@manuco.pl" style="color:#fff">bedziecha@manuco.pl</a></p>
		</div>
		
		<div class="col-sm-6">
			<p style="text-align: left;padding-top:5px;">© StageTeam</p>
		</div>
		<div class="col-sm-6">
			<p style="text-align: right;padding-top:5px;">wersja 1.0.1</p>
		</div>
	</div>
	 @show
   </footer>
   @endif
    <script src="{{ asset('/js/all.js') }}"></script>
  
 
    </body>
	
</html>
