@extends('app')

@section('content')
	<div class="container home">
		<div class="row">
			<div class="col-md-12">
				<h3>Witaj w systemie {{env('NAME')}} - Strona Główna</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<h2><div class="btn-default"> </div>Obecne wydarzenia</h2>
				@can('show_events')	
				<h4>Twoje:</h4>
				@endcan
					@if(count($events_today_my)==0)
					Brak wydarzeń
					@else
					@foreach($events_today_my as $event)
					<p><a href="{{url('events/'.$event->id)}}">{{$event->name}}</a></p>
					@endforeach
					@endif
				@can('show_events')	
				<h4>Wszystkie:</h4>
					@if(count($events_today)==0)
					Brak wydarzeń
					@else
					<table class="table table-striped events_week">
					@foreach($events_today as $event)
					<tr>
						<td>
						<a href="{{url('events/'.$event->id)}}">{{$event->name}}</a>
						</td>
						<td>
							<?php $user_check=0;$user_null=0;$user_count=0;?>
							@foreach($event->users as $user)
								<?php 
								if($user->pivot->check==1)
								$user_check++;
								if($user->pivot->check=='0')
								$user_null++;
								if($user->pivot->check==1||$user->pivot->check===null)
								$user_count++;
								?>
							@endforeach
							<span class="badge" @if($user_count>$user_check) style="background: rgb(213,53,53)" @else style="background: rgb(10,10,10)" @endif data-toggle="tooltip" data-placement="top" 
							title="Udział potwierdziło {{$user_check}} z {{$user_count}} pracowników @if($user_null>0) | {{$user_null}} odmówił/o @endif">
								{{$user_count}}
							</span>
						</td>
						<td>
							<span class="badge" style="background: rgb(10,10,10)" data-toggle="tooltip" data-placement="top" title="Liczba sprzętu">
								{{count($event->add_equipment)}}
							</span>
						</td>
						<td>
							<span class="badge" style="background: rgb(10,10,10)" data-toggle="tooltip" data-placement="top" title="Liczba riderów">
								{{count($event->riders)}}
							</span>
						</td>
						<td>
							<span class="badge" style="background: rgb(10,10,10)" data-toggle="tooltip" data-placement="top" title="Liczba inputlist">
								{{count($event->inputlist)}}
							</span>
						</td>
					</tr>
					@endforeach
					</table>
					@endif
				@endcan	
			</div>
			<div class="col-md-6">
				<h2><div class="btn-default"> </div>Wydarzenia w najbliższym tygodniu</h2>
				@can('show_events')	
				<h4>Twoje:</h4>
				@endcan
					@if(count($events_week_my)==0)
					Brak wydarzeń
					@else
					@foreach($events_week_my as $event)
					<p><a href="{{url('events/'.$event->id)}}">{{$event->name}}</a></p>
					@endforeach
					@endif
				@can('show_events')	
				<h4>Wszystkie:</h4>
					@if(count($events_week)==0)
					Brak wydarzeń
					@else
					<table class="table table-striped events_week">
					@foreach($events_week as $event)
					<tr>
						<td>
						<a href="{{url('events/'.$event->id)}}">{{$event->name}}</a>
						</td>
						<td>
							<?php $user_check=0;$user_null=0;$user_count=0;?>
							@foreach($event->users as $user)
								<?php 
								if($user->pivot->check==1)
								$user_check++;
								if($user->pivot->check=='0')
								$user_null++;
								if($user->pivot->check==1||$user->pivot->check===null)
								$user_count++;
								?>
							@endforeach
							<span class="badge" @if($user_count>$user_check) style="background: rgb(213,53,53)" @else style="background: rgb(10,10,10)" @endif  data-toggle="tooltip" data-placement="top" 
							title="Udział potwierdziło {{$user_check}} z {{$user_count}} pracowników @if($user_null>0) | {{$user_null}} odmówił/o @endif">
								{{$user_count}}
							</span>
						</td>
						<td>
							<span class="badge" style="background: rgb(10,10,10)" data-toggle="tooltip" data-placement="top" title="Liczba sprzętu">
								{{count($event->add_equipment)}}
							</span>
						</td>
						<td>
							<span class="badge" style="background: rgb(10,10,10)" data-toggle="tooltip" data-placement="top" title="Liczba riderów">
								{{count($event->riders)}}
							</span>
						</td>
						<td>
							<span class="badge" style="background: rgb(10,10,10)" data-toggle="tooltip" data-placement="top" title="Liczba inputlist">
								{{count($event->inputlist)}}
							</span>
						</td>
					</tr>
					@endforeach
					</table>
					@endif
					
				@endcan	
			</div>
		</div>
		@can('add_events')
		<div class="row">
			<div class="col-md-12">
				<h2><div class="btn-default"> </div>Wydarzenia oczekujące na potwierdzenie</h2>
					@if(count($events_wait)==0)
					Brak wydarzeń
					@else
					<table class="table table-striped">
					@foreach($events_wait as $event)
					<tr @if(\Carbon\Carbon::now()->diffInDays(new \Carbon\Carbon($event->start))<=2)class="danger" @endif>
						<td style="line-height: 40px;">
							<a href="{{url('events/'.$event->id)}}">{{$event->name}}</a>
						</td>
						<td>
							{!! Form::model($event, array('route' => array('events.confirmation', $event->id),'method' => 'post')) !!}
								{!! Form::submit('Potwierdź wydarzenie',array('class'=>'btn btn-info'))!!}
							{!! Form::close() !!}
						</td>
					</tr>	
					@endforeach
					</table>
					@endif
			</div>
		</div>
		@endcan
		@if(count($events_for_check)>0)
			<div class="row">
				<div class="col-md-12" ng-controller="conf_by_user">
					<h2><div class="btn-default"> </div>Potwierdź swój udział w wydarzeniach</h2>
						<div class="alert alert-success alert-dismissable" ng-if="info!=null">
						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						  <%info%>
						</div>
					<table class="table table-striped">
					@foreach($events_for_check as $event)
					<tr>
						<td>
							<a href="{{url('events/'.$event->id)}}">{{$event->name}}</a>
						</td>
						<td >
							<button class="btn btn-info" ng-click="user_ok({{$event->id}})">Potwierdź udział</button>
				          	<button class="btn btn-info" ng-click="user_no({{$event->id}})">Odrzuć</button>
						</td>
					</tr>	
					@endforeach
					</table>
				</div>
			</div>
		@endif
	</div>
@endsection
