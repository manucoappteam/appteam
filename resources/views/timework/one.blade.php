@extends('app')

@section('content')
	<div class="container">
		<div class="col-sm-12">
		<a href="{{URL::previous()}}" class="back btn"><img class="icon" src="{{asset('icon/arrow.svg')}}">Powrót</a>
		<h2 style="margin-bottom:10px;"><div class="btn-default"> </div>Czas pracy użytkownika: {{$data->name}}</h2>
		<h4 style="margin-bottom:15px;">Typ rozliczania: {{$data->type}}</h4>
		<div class="timeworks" ng-controller="timeworks">
		<div class="align-right"><button class="btn btn-default" data-toggle="modal" data-target="#addModal">Dodaj</button>
			<p style="margin-top:10px; margin-bottom:10px;">Dodawanie czasu pracy na podstawie wydarzeń możliwe jest z widoku wydarzenia.</p>
		</div>	
		<!--okienko do dodawania-->
		<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
			        <h4 class="modal-title" id="myModalLabel">Dodaj czas pracy</h4>
			      </div>
			      <div class="modal-body">
			       <div class="alert alert-success alert-dismissable" ng-if="info_out!=null">
					  <button type="button" class="close" ng-click="close_out()">&times;</button>
					  <%info_out%>
					</div>
					<form id="oddo">
					<div class="form-group">
					  	<label style="display:block;">Od</label>
					  	{!!Form::selectRange('start_day',1,31,date('d'))!!}
						{!!Form::select('start_month', array('Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'),date('m')-1)!!}
						{!!Form::selectRange('start_year',2015,2035,date('Y'))!!}
						- 
						{!!Form::selectRange('start_hour',0,23,8)!!}
						{!!Form::selectRange('start_minutes',00,59,00)!!}
					</div>	
					<div class="form-group">
					  	<label style="display:block;">Do</label>
					  	{!!Form::selectRange('end_day',1,31,date('d'))!!}
						{!!Form::select('end_month', array('Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'),date('m')-1)!!}
						{!!Form::selectRange('end_year',2015,2035,date('Y'))!!}
						-
						{!!Form::selectRange('end_hour',0,23,16)!!}
						{!!Form::selectRange('end_minutes',00,59,00)!!}
					</div>
					   <div class="checkbox">
				  		 <label>
						{!!Form::checkbox('all_day',1)!!} Pełny dzień/dni
						</label>
					</div>
					<div class="form-group">
					  	<label>Opis</label>
						<input type="text" class="form-control" ng-model="description" placeholder="Opis">
					</div>						
					</form>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
			        <button type="button" class="btn btn-default" ng-click="add(description)">Dodaj</button>
			      </div>
			    </div>
			  </div>
			</div>	
			<div class="box">
				<table class="table table-striped">
				  <thead>
				    <tr>
				      <th>Wydarzenie/Opis</th>
				      <th>{{$data->type}}</th>
				      <th>Zapłacono/usuń</th>
				    </tr>
				  </thead>
				  <tbody>
				  		<div class="loading-spiner-holder" data-loading ><div class="loader">Trwa ładowanie...</div></div>
						<tr ng-repeat="data in timeworks.elements">
							<td ng-if="data.event.name!=null"><%data.event.name%></td>
							<td ng-if="data.event.name==null"><%data.description%></td>
							<td><%data.hours%></td>
							<td><button class="btn btn-success" ng-click="del(data.id)">Zapłacono/Usuń</button></td>
						</tr>
				  </tbody>
				  <tr>
				  	<td>Suma</td>
				  	<td><%timeworks.sum%></td>
				  	<td><button class="btn btn-success" ng-click="del_all()">Zapłacono/Usuń wszystko</button></td>
				  </tr>
				</table>
				<script>
					function obl(o){
						if(!isNaN(o.value))
						document.getElementById('kw').innerHTML="Kwota: "+(o.value*document.getElementById('sum').value);
						}
				</script>
				<div class="">
					<h4>Oblicz kwotę</h4>
					<input type="hidden" id="sum" value="<%timeworks.sum%>">
					<input type="text" id="pay" class="form-control" placeholder="Stawka" onchange="obl(this)">
					<h4 id="kw">Kwota: </h4>
				</div>

			</div>
		</div>
	</div>	
		
	</div>
@endsection
