@extends('app')

@section('content')
	<div class="container">
		<h2><div class="btn-default"> </div>Mój czas pracy</h2>
		<div class="timework" ng-controller="timework">
		@can('timework_self')	
			<div class="align-right"><button class="btn btn-default" data-toggle="modal" data-target="#addModal">Dodaj</button>
			<p style="margin-top:10px; margin-bottom:10px;">Dodawanie czasu pracy na podstawie wydarzeń możliwe jest z widoku wydarzenia.</p>
		</div>	
		<!--okienko do dodawania-->
		<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
			        <h4 class="modal-title" id="myModalLabel">Dodaj czas pracy</h4>
			      </div>
			      <div class="modal-body">
			        <div class="alert alert-success alert-dismissable" ng-if="info_out!=null">
					  <button type="button" class="close" ng-click="close_out()">&times;</button>
					  <%info_out%>
					</div>
					<form id="oddo">
					<input type="hidden" id="user_id" value="{{Auth::user()->id}}">
					<div class="form-group">
					  	<label style="display:block;">Od</label>
					  	{!!Form::selectRange('start_day',1,31,date('d'))!!}
						{!!Form::select('start_month', array('Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'),date('m')-1)!!}
						{!!Form::selectRange('start_year',2015,2035,date('Y'))!!}
						- 
						{!!Form::selectRange('start_hour',0,23,8)!!}
						{!!Form::selectRange('start_minutes',00,59,00)!!}
					</div>	
					<div class="form-group">
					  	<label style="display:block;">Do</label>
					  	{!!Form::selectRange('end_day',1,31,date('d'))!!}
						{!!Form::select('end_month', array('Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'),date('m')-1)!!}
						{!!Form::selectRange('end_year',2015,2035,date('Y'))!!}
						-
						{!!Form::selectRange('end_hour',0,23,16)!!}
						{!!Form::selectRange('end_minutes',00,59,00)!!}
					</div>
					   <div class="checkbox">
				  		 <label>
						{!!Form::checkbox('all_day',1)!!} Pełny dzień/dni
						</label>
					</div>
					<div class="form-group">
					  	<label>Opis</label>
						<input type="text" class="form-control" ng-model="description" placeholder="Opis">
					</div>						
					</form>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
			        <button type="button" class="btn btn-default" ng-click="add(description)">Dodaj</button>
			      </div>
			    </div>
			  </div>
			</div>	
			@endcan
			<div class="box">
				<table class="table table-striped">
				  <thead>
				    <tr>
				      <th>Wydarzenie/Opis</th>
				      <th>{{$type}}</th>
				    </tr>
				  </thead>
				  <tbody>
				  		<div class="loading-spiner-holder" data-loading ><div class="loader">Trwa ładowanie...</div></div>
						<tr ng-repeat="data in timework.elements">
							<td ng-if="data.event.name!=null"><%data.event.name%></td>
							<td ng-if="data.event.name==null"><%data.description%></td>
							<td><%data.hours%></td>
						</tr>
				  </tbody>
				  <tr>
				  	<td>Suma</td>
				  	<td><%timework.sum%></td>
				  </tr>
				</table>
			</div>
		</div>
		
		
	</div>
@endsection
