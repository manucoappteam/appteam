@extends('app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">	
					<h2><div class="btn-default"> </div>Czas pracy ekipy</h2>
					<div class="timeworks" ng-controller="timeworks">
						<div class="box">
							<table class="table table-striped">
							  <thead>
							    <tr>
							      <th>Imię i nazwisko</th>
							      <th>Obecna liczba godzin/dni</th>
							       <th>Typ rozliczania</th>
							    </tr>
							  </thead>
							  <tbody>
							  	@foreach($users as $user)
									<tr>
										<td><a href="{{url('timeworks/'.$user['id'])}}">{{$user['name']}}</a></td>
										<td>{{$user['time']}}</td>
										<td>{{$user['type']}}</td>	
									</tr>
								@endforeach	
							  </tbody>
							</table>
						</div>
					</div>
			</div>
		</div>
	</div>
@endsection
