@extends('app')

@section('content')
	<div class="container">
		<a href="{{url('/roles')}}" class="back btn"><img class="icon" src="{{asset('icon/arrow.svg')}}">Powrót</a>
		
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<h2><div class="btn-default"> </div>Nowa rola</h2>
		
		<div class="roles">
		{!! Form::open( array('route' => array('roles.store'))) !!}
		 <div class="form-group">
		   {!! Form::text('name',null,array('placeholder'=>'Nazwa','class'=>'form-control'))!!}
		 </div>  
		 <div class="form-group">
		   {!! Form::textarea('description',null,array('placeholder'=>'Opis','class'=>'form-control'))!!}
		 </div>  
		  <h4>Uprawnienia</h4>
		  <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('users', true ,null)!!} Edycja i tworzenie użytkowników 
   			 </label>	
		  </div>
		  <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('add_events', true , null)!!} Tworzenie wydarzeń
   			 </label>	
		  </div>
		   <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('show_events', true , null)!!} Wyświetlanie wydarzeń
   			 </label>	
		  </div>
		   <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('add_equipment', true , null)!!} Dodawanie sprzętu
   			 </label>	
		  </div>
		   <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('edit_equipment', true , null)!!} Edycja sprzętu
   			 </label>	
		  </div>
		   <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('delete_equipment', true , null)!!} Usuwanie sprzętu
   			 </label>	
		  </div>
		   <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('show_equipment', true , null)!!} Wyświetlanie sprzętu
   			 </label>	
		  </div>
		   <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('equipment_to_event', true , null)!!} Dodawanie sprzętu do wydarzenia
   			 </label>	
		  </div>
		   <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('show_equipment_to_event', true , null)!!} Wyświetlanie sprzętu w wydarzeniu
   			 </label>	
		  </div>
		   <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('check_equipment_to_event', true , null)!!} Sprawdzanie sprzętu
   			 </label>	
		  </div>
		    <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('absence', true , null)!!} Nieobecności
   			 </label>	
		  </div>
		     <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('users_to_event', true , null)!!} Dodawanie użytkowanika do wydarzenia
   			 </label>	
		  </div>
		    <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('check_users_on_event', true , null)!!} Dodawanie czasu pracy użytkownika - wydarzenie
   			 </label>	
		  </div>
		    <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('riders', true , null)!!} Dodawanie i edycja riderów
   			 </label>	
		  </div>
		    <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('riders_show', true , null)!!} Wyświetlanie riderów
   			 </label>	
		  </div>
		   <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('timework', true , null)!!} Własny czas pracy
   			 </label>	
		  </div>
		   <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('timeworks', true , null)!!} Zarządzanie czasem pracy
   			 </label>	
		  </div>
		   <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('inputlist', true , null)!!} Dodawanie i edycja inputlist
   			 </label>	
		  </div>
		  <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('inputlist_show', true , null)!!} Wyświetlanie inputlist
   			 </label>	
		  </div>
		  <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('history', true , null)!!} Wyświetlanie historii
   			 </label>	
		  </div>
		  <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('timework_self', true , null)!!} Dodawanie własnego czasu pracy
   			 </label>	
		  </div>
		  <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('comments', true , null)!!} Komentarze
   			 </label>	
		  </div>
		   <div class="checkbox">
		  	 <label>
     		{!!Form::checkbox('want_event', true , null)!!} Prośby o dodanie do wydarzenia
   			 </label>	
		  </div>
		  {!! Form::submit('Dodaj',array('class'=>'btn btn-success'))!!}
		{!! Form::close() !!}
		</div>
	</div>
@endsection
