@extends('app')

@section('content')
	<div class="container">
		<h2><div class="btn-default"> </div>Role</h2>
		<div class="align-right"><a href="{{url('/roles/create')}}" class="btn btn-default">Dodaj role</a></div>
		<div class="roles">
			<ul class="list-group" >
				@foreach($roles as $role)
				<li class="list-group-item" style="height:52px;line-height:30px">{{$role->name}}<a href="{{url('/roles/'.$role->id.'/edit')}}" class="btn btn-info">Edytuj</a></li>
				@endforeach
			</ul>
		</div>
	</div>
@endsection
