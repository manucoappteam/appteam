<body>
<h4>Twoje dane w systemie {{$sys}} zostały zmienione</h4>
<h4>Dane do logowania</h4>
<div>
<p>Login: {{$login}}</p>
<p>Hasło: {{$pass}}</p>
<p>Adres: <a href="{{url('/')}}">Link</a></p>
</div>
<br>
<h5>
Jeśli uważasz że nie powinieneś otrzymać tej wiadomości skontaktuj się z administratorem systemu lub zignoruj ją.
</h5>
</body>