<body>
<h4>W systemie {{$sys}} zostało utworzone konto powiązane z tym adresem e-mail</h4>
<h4>Dane do logowania</h4>
<div>
<p>Login: {{$login}}</p>
<p>Hasło: {{$pass}}</p>
<p>Adres: <a href="{{url('/')}}">Link</a></p>
</div>
<br>
<h5>
Pamiętaj o zmianie hasła, po zalogowaniu do systemu.
</h5>
<h5>
Jeśli uważasz że nie powinieneś otrzymać tej wiadomości skontaktuj się z administratorem stystemu lub zignoruj ją.
</h5>
</body>