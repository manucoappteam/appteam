﻿<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<title>{{$event->name}}</title>
		<style>
		.page-break {
    		page-break-after: always;
		}
		.info p, .time p{
			font-size:15px;
			margin-top:2px;
			margin-bottom:2px;
		}
		h5{
			margin-bottom:10px;
		}
		td{
			height:30px;
			width:100px;
			padding:4px;
			border-top:1px solid #000;
		}
		th{
			text-align: left;
		}
		</style>
	</head>
	<body style="position: relative;">
		<h3>Sprzęt - {{$event->name}} - @if($cat!='all'){{$cat}}@endif</h3>
		<div>
			<table class="table table-striped">
				  <thead>
				    <tr style="font-size:10px;">
				      <th>Nazwa</th>
				      <th>Ilość</th>
				      <th>Kategoria</th>
				      <th>Przed</th>
				      <th>Po</th>
				      <th>Ilość zwrócona</th>
				    </tr>
				  </thead>
				  <tbody>
				@foreach($event->equipment as $eq)	
					@if($cat=='all'||$eq->type==$cat)
						<tr>
							<td>{{$eq->name}}</td>
							<td>{{$eq->pivot->count}}</td>
							<td>{{$eq->type}}</td>
							<td><input type="checkbox" <?php if($eq->pivot->check_before==1) echo 'checked';?>></td>
							<td><input type="checkbox" <?php if($eq->pivot->check_after==1) echo 'checked';?>></td>
							<td><input type="text" value="{{$eq->count_after}}" style="width:30px; border:1px solid #000;height:20px;"></td>
						</tr>	
					@endif				 
				@endforeach
				@if($cat=='all')
				@foreach($event->add_equipment as $eq)
				@if($eq->equipment_id==null)	
						<tr >
							<td>{{$eq->oder}}</td>
							<td>{{$eq->count}}</td>
							<td>-</td>
							<td><input type="checkbox" <?php if($eq->check_before==1) echo 'checked';?>></td>
							<td><input type="checkbox" <?php if($eq->check_after==1) echo 'checked';?>></td>
							<td><input type="text" value="{{$eq->count_after}}" style="width:30px; border:1px solid #000;height:20px;"></td>
						</tr>
				@endif						 
				@endforeach
				@endif
				</tbody>
				</table>
		</div>
		</div>
	</body>
</html>