﻿<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<title>{{$event->name}}</title>
		<style>
		.page-break {
    		page-break-after: always;
		}
		.info p, .time p{
			font-size:15px;
			margin-top:2px;
			margin-bottom:2px;
		}
		h5{
			margin-bottom:10px;
		}
		td{
			height:30px;
			width:100px;
			padding:4px;
			border-top:1px solid #000;
		}
		th{
			text-align: left;
		}
		</style>
	</head>
	<body style="position: relative;">
		<img src="{{asset('photo/logo.png')}}" style="width:200px;">
		<div style="position:absolute; top:0cm; left:250px"><h3 style="margin-bottom:0px;">Wydarzenie:</h3><h2 style="margin-top:0.2cm;">{{$event->name}}</h2></div>
		<div class="time" style="position:absolute; top:130px; left:0px; width:280px;">
			<h5>Termin:</h5>
			<p>Rozpoczęcie: {{substr($event->start, 0, -3)}}</p>
			<p>Zakończenie: {{substr($event->end, 0, -3)}}</p>
			<p>Gotowość: {{substr($event->check, 0, -3)}}</p>
		</div>
		<div class="info" style="position:absolute; top:130px; left:300px">
		<h5>Lokalizacja:</h5><p>{{$event->localization}}</p>
		<h5>Typ wydarzenia: </h5><p>{{$event->type}}</p>
		</div>
		<div style="position:relative; margin-top:260px;"><h5>Opis:</h5>
			{{$event->description}}
		</div>
		<div style="margin-top:30px;">
		<h3>Ekipa</h3>
		@foreach($event->users as $user)
		@if($user->pivot->check==1)
		@if($user->pivot->function=="Szef ekipy")
			<p>Szef ekipy: {{$user->name}}</p>
		@endif
		@endif
		@endforeach
		<div>
			@foreach($event->users as $user)
			@if($user->pivot->check==1&&$user->pivot->function!="Szef ekipy")
			{{$user->name}} 
				@if($user->pivot->function!="Brak")
					({{$user->pivot->function}})
				@endif
				, 
			@endif
			@endforeach
		</div>
		<div class="page-break"></div>
		<h3>Sprzęt - {{$event->name}}</h3>
		<div>
			<table class="table table-striped">
				  <thead>
				    <tr style="font-size:10px;">
				      <th>Nazwa</th>
				      <th>Ilość</th>
				      <th>Kategoria</th>
				      <th>Przed</th>
				      <th>Po</th>
				      <th>Ilość zwrócona</th>
				    </tr>
				  </thead>
				  <tbody>
				@foreach($event->equipment as $eq)	
						<tr >
							<td>{{$eq->name}}</td>
							<td>{{$eq->pivot->count}}</td>
							<td>{{$eq->type}}</td>
							<td><input type="checkbox" <?php if($eq->pivot->check_before==1) echo 'checked';?>></td>
							<td><input type="checkbox" <?php if($eq->pivot->check_after==1) echo 'checked';?>></td>
							<td><input type="text" value="{{$eq->count_after}}" style="width:30px; border:1px solid #000;height:20px;"></td>
						</tr>				 
				@endforeach
				@foreach($event->add_equipment as $eq)
				@if($eq->equipment_id==null)	
						<tr >
							<td>{{$eq->oder}}</td>
							<td>{{$eq->count}}</td>
							<td>-</td>
							<td><input type="checkbox" <?php if($eq->check_before==1) echo 'checked';?>></td>
							<td><input type="checkbox" <?php if($eq->check_after==1) echo 'checked';?>></td>
							<td><input type="text" value="{{$eq->count_after}}" style="width:30px; border:1px solid #000;height:20px;"></td>
						</tr>
				@endif						 
				@endforeach
				</tbody>
				</table>
		</div>
		</div>
		@foreach($event->inputlist as $input)
		
		<div class="page-break"></div>
		<h3>Inputlista {{$input->name}} do wydarzenia {{$event->name}}</h3>
		<?php $list=explode(';',$input->content);?>
		<table>
			<thead>
				<tr>
				<th>Channel</th>
				<th>Nazwa</th>
				</tr>
			</thead>
			<tbody>
				@foreach($list as $key=>$lis)
				<tr>
					<td>{{($key+1)}}</td>
					<td>{{$lis}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	
		@endforeach
	</body>
</html>