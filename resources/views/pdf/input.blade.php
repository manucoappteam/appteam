﻿<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<title>{{$input->name}}</title>
		<style>
		.page-break {
    		page-break-after: always;
		}
		.info p, .time p{
			font-size:15px;
			margin-top:2px;
			margin-bottom:2px;
		}
		h5{
			margin-bottom:10px;
		}
		td{
			height:30px;
			width:200px;
			padding:4px;
			border-top:1px solid #000;
		}
		th{
			text-align: left;
		}
		</style>
	</head>
	<body style="position: relative;">
		<h3>Inputlista {{$input->name}}</h3>
		<?php $list=explode(';',$input->content);?>
		<table>
			<thead>
				<tr>
				<th>Channel</th>
				<th>Nazwa</th>
				</tr>
			</thead>
			<tbody>
				@foreach($list as $key=>$lis)
				<tr>
					<td>{{($key+1)}}</td>
					<td>{{$lis}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>

	</body>
</html>