@extends('app')

@section('content')
<div class="container" ng-controller="inputlist">
		<a href="{{url('/events/'.$data->event->id)}}" class="back btn"><img class="icon" src="{{asset('icon/arrow.svg')}}">Powrót</a>
				<h2><div class="btn-default"> </div>Inputlista {{$data->name}} należąca do wydarzenia {{$data->event->name}}</h2>
				<div class="alert alert-success alert-dismissable" ng-if="info!=null">
				  <button type="button" class="close" ng-click="close()">&times;</button>
				  <%info%>
				</div>
				<table class="table table-striped">
				  <thead>
				    <tr>
				      <th>Channel</th>
				      <th>Nazwa</th>
				      @can('inputlist')
				      <th>Zapisz</th>
				      @endcan
				    </tr>
				  </thead>
				  <tbody  @can('inputlist') ui-sortable="sortableOptions" ng-model="channels" @endcan>
					<tr ng-repeat="data in channels track by $index">
						<td class="ch"><%data.id%></td>
						@can('inputlist')
						<td><input type="text" ng-model="data.text"class="form-control"></td>
						<td><button class="btn btn-success" ng-click="change(channels)" >Zapisz</button></td>
						@else
						<td><%data.text%></td>
						@endcan
					</tr>
				  </tbody>
				</table>
				 @can('inputlist')
				<div class="form-group">
					<button ng-click="add_channel(channels)" class="btn btn-success">Dodaj pozycję</button>
				</div>
				@endcan
					
	
</div>
@endsection
