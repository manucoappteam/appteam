@extends('app')

@section('content')
<div class="container">
		<a href="{{url('/events/'.$_GET['event_id'])}}" class="back btn"><img class="icon" src="{{asset('icon/arrow.svg')}}">Powrót</a>
		
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
				<h2><div class="btn-default"> </div>Dodaj inputlistę</h2>
				{!!Form::open( array('route' => array('inputlist.store')))!!}
				{!!Form::hidden('event_id',$_GET['event_id'])!!}
				<div class="form-group">
				{!!Form::text('name',null,array('placeholder'=>'Nazwa','class'=>'form-control'))!!}
				</div>
				<table class="table table-striped">
				  <thead>
				    <tr>
				      <th>Channel</th>
				      <th>Nazwa</th>
				    </tr>
				  </thead>
				  <tbody>
					@for($i=1;$i<=10;$i++)
					<tr>
						<td class="ch">{{$i}}</td>
						<td>{!!Form::text('ch'.$i,null,array('placeholder'=>'Podaj nazwę','class'=>'form-control'))!!}</td>
					</tr>
					@endfor
				  </tbody>
				</table>
				<div class="form-group">
					<button id="add_channel" class="btn btn-success">Dodaj pozycję</button>
				</div>
				
				{!!Form::submit('Zapisz',array('class'=>'btn btn-success'))!!}
				{!!Form::close()!!}

					
	
</div>
@endsection
