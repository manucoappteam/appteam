@extends('app')

@section('content')
	<div class="container">
		<h2><div class="btn-default"> </div>Sprzęt</h2>
		@can('add_equipment')
		<div class="align-right"><a href="{{url('/equipment/create')}}" class="btn btn-default">Dodaj sprzęt</a></div>
		@endcan
		<div class="equipment" ng-controller="eq">
		
		
			<div class="nav" style="margin-top:20px;">
				<ul class=".form-inline">
					<li>Kategoria: </li>
					<li>
						<select id="category" class="form-control" ng-model="search.type">
							<option value="">Wyszystkie</option>
							@foreach($category as $cat)
								<option value="{{$cat}}">{{$cat}}</option>
							@endforeach
						</select>
					</li>
					<li>
						<input class="form-control" id="search_input" placeholder="Szukaj.." ng-model="search.name">
					</li>
				</ul>
			</div>
			<div class="box">
				<table class="table table-striped">
				  <thead>
				    <tr>
				      <th ng-click="order('name')">Nazwa</th>
				      <th ng-click="order('description')">Opis</th>
				      <th ng-click="order('count')">Liczba</th>
				      <th ng-click="order('type')">Kategoria</th>
				      <th ng-click="order('info')">Status obecny (dzisiaj)</th>
				    </tr>
				  </thead>
				  <tbody>
				  		<div class="loading-spiner-holder" data-loading ><div class="loader">Trwa ładowanie...</div></div>
						<tr ng-repeat="data in eq | filter:search |orderBy:predicate:reverse">
							<td><a href="{{url('/equipment/<%data.id%>')}}"><%data.name%></a></td>
							<td><%data.description%></td>
							<td><%data.count | number%></td>
							<td><%data.type%></td>	
							<td><%data.info | number%></td>	
						</tr>
				  </tbody>
				</table>
			</div>
		</div>
		
		
	</div>
@endsection
