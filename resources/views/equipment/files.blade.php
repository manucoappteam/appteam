	 <div class="tab-pane" id="files">
		@can('edit_equipment')
		  	<button class="btn btn-default" data-toggle="modal" data-target="#add_file">
							 Dodaj
			</button>
			@endcan
			<!--dodawanie rideru-->
			<div class="modal fade" id="add_file" tabindex="-1" role="dialog" aria-labelledby="Dodaj plik" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
			        <h4 class="modal-title" id="myModalLabel"> Dodaj plik</h4>
			      </div>
			      <div class="modal-body">
			      	<div class="alert alert-success alert-dismissable" ng-if="info_out!=null">
					  <button type="button" class="close" ng-click="close_out()">&times;</button>
					  <%info_out%>
					</div>
			      	<div class="form-group"><input ng-model="name_file" type="text" placeholder="Nazwa" class="form-control"></div>
			        <div class="form-group"><select class="form-control" ng-model="type_file">
			        	<option value="manual">Manual</option>
			        	<option value="specyfikacja">Specyfikacja</option>
			        	<option value="inne">Inny</option>
			        </select></div>
			        <input type="hidden" id="file_rider">
			        <span class="btn btn-success fileinput-button">
				        <i class="glyphicon glyphicon-plus"></i>
				        <span>Wybierz plik...</span>
				        <!-- The file input field used as target for the file upload widget -->
				        <input id="fileupload" type="file" name="files[]">
				    </span>
				    <div id="progress" class="progress" style="margin-top:10px;">
				        <div class="progress-bar progress-bar-success"></div>
				    </div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
			        <button type="button" class="btn btn-default" ng-click="add_file(name_file,type_file)">Dodaj</button>
			      </div>
			    </div>
			  </div>
			</div>
			<!--dodawanie eideru END-->
			<table class="table table-striped">
				  <thead>
				    <tr>
				      <th ng-click="order('name')">Nazwa</th>
				      <th ng-click="order('type')">Typ</th>
				      @can('edit_equipment')<th>Usuń</th>@endcan
				    </tr>
				  </thead>
				  <tbody>
				  		<div class="loading-spiner-holder" data-loading ><div class="loader">Trwa ładowanie...</div></div>
						<tr ng-repeat="data in files | filter:search |orderBy:predicate:reverse track by $index "  ng-if="data!=null">
							
							<td><a href="{{asset('<%data.file_url%>')}}"><%data.name%></a></td>
							<td><%data.type%></td>	
							@can('edit_equipment')
						       <td><button class="btn btn-danger" ng-click="del_file(data.id)">Usuń</button></td>
						      @endcan	
							
						</tr>
				  </tbody>
			</table>

		</div>	