@extends('app')

@section('content')
	<div class="container">
		<a href="{{URL::previous()}}" class="back btn"><img class="icon" src="{{asset('icon/arrow.svg')}}">Powrót</a>
		
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		
		<h2><div class="btn-default"> </div>{{$equipment->name}} - edycja</h2>
		
		<div class="roles">
		{!! Form::model($equipment, array('route' => array('equipment.update', $equipment->id),'method' => 'put')) !!}
		<div class="form-group">
			   {!! Form::text('name',null,array('placeholder'=>'Nazwa','class'=>'form-control'))!!}
			 </div>  
			 <div class="form-group">
			   {!! Form::textarea('description',null,array('placeholder'=>'Opis','class'=>'form-control'))!!}
			 </div> 
			 <div class="form-group">
			 	<label>Liczba</label>
			   {!! Form::text('count',null,array('placeholder'=>'Liczba','class'=>'form-control'))!!}
			 </div>
			  <div class="form-group">
			  	<label>Moc [W]</label>
			   {!! Form::number('power',null,array('placeholder'=>'Moc','class'=>'form-control'))!!}
			 </div>
			 <div class="form-group">
			  	<label>Cena wypożyczenia [zł]</label>
			   {!! Form::number('price',null,array('placeholder'=>'Cena','class'=>'form-control'))!!}
			 </div>
			  <div class="form-group">
			  	<label>Kategoria:</label>
			 <select class="form-control" name="type">
								@foreach($category as $type)
									<option value="{{$type}}" <?php if($equipment->type==$type) echo 'selected';?>>{{$type}}</option>
								@endforeach
								<option value="add">Dodaj...</option>
			</select>
			</div>
			<div class="form-group">
			{!! Form::text('other_type',null,array('placeholder'=>'Inna kategoria','class'=>'form-control','style'=>'display:none'))!!}
			</div>
		  {!! Form::submit('Zapisz zmiany',array('class'=>'btn btn-success','style'=>'float:left; margin-right:20px'))!!}
		{!! Form::close() !!}
		
		@can('delete_equipment')
		{!! Form::model($equipment, array('route' => array('equipment.destroy', $equipment->id),'method' => 'delete')) !!}
		<div class="form-group" style="margin-top:15px;">
		  {!! Form::submit('Usuń sprzęt (nieodwracalne)',array('class'=>'btn btn-danger','style'=>'width:200px'))!!}
		</div>
		{!! Form::close() !!}
		@endcan
		</div>
	</div>
@endsection
