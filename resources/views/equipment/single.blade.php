@extends('app')

@section('content')
	<div class="container single equipment" ng-controller="eq_single">
		<a href="{{url('equipment')}}" class="back btn"><img class="icon" src="{{asset('icon/arrow.svg')}}">Powrót</a>
		<input type="hidden" value="{{$data->id}}" id="id_hidden">
		<h2><div class="btn-default"> </div>Sprzęt: {{$data->name}}</h2>
		<div class="nav">
			@can('edit_equipment')
			<div class="align-right"><a href="{{url('/equipment/'.$data->id.'/edit')}}" class="btn btn-default">Edytuj</a></div>
			@endcan
		</div>
		<div class="row">
		<div class="col-md-3">
		<p>{{$data->description}}</p>
		<h4>Kategoria: {{$data->type}}</h4>
		<h4>Liczba: {{$data->count}}</h4>
		<h4>Stan obecny (dzisiaj): <%info%></h4>
		</div>
		<div class="col-md-6">
			<h4>Informacje dodatkowe</h4>
			<p>Moc [W]: @if($data->power){{$data->power}}@else -- @endif</p>
			<p>Cena wypożyczenia:  @if($data->price){{$data->price}}zł @else --@endif</p>
		</div>	
		</div>
		<div class="box">
			<ul class="nav nav-pills" role="tablist" style="float: none; margin-top:20px;">
				  <li class="active"><a href="#history" role="tab" data-toggle="tab" ng-click="history_load()">Historia wydarzeń</a></li>
				  <li><a href="#files" role="tab" data-toggle="tab" ng-click="file_load()">Pliki</a></li>
			</ul>
			<div class="tab-content" style="margin-top:20px;"> 
				@include('equipment.history')
				@include('equipment.files')
			</div>
		</div>
	</div>
@endsection
