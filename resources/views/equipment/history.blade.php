	 <div class="tab-pane active" id="history">

				<table class="table table-striped">
				  <thead>
				    <tr>
				      <th ng-click="order('name')">Nazwa</th>
				      <th ng-click="order('termin')">Termin</th>
				      <th ng-click="order('count')">Liczba</th>
				      <th ng-click="order('c_after')">Liczba zwrócona </th>
				    </tr>
				  </thead>
				  <tbody>
						<tr ng-repeat="dat in data | orderBy:predicate:reverse">
							<td><a href="{{url('/events/<%dat.id%>')}}"><%dat.name%></a></td>
							<td><%dat.termin%></td>
							<td><%dat.pivot.count%></td>
							<td><%dat.c_after%></td>	
						</tr>
				  </tbody>
				</table>

		</div>	