@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
				<h2><div class="btn-default"> </div>Dodaj użytkownika</h2>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							Wystąpił problem<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/users/register') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Nazwa</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="name" value="{{ old('name') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">E-mail</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Hasło</label>
							<div class="col-md-6">
								<input type="password" class="form-control pass 1" name="password">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button  class="btn btn-default col-xs-4 gen_password_btn" id="1" style="height:34px; line-height:24px;">
									Generuj hasło
								</button>
								<div class="col-xs-6">
								<input type="text" id="1" name="gen_password" class="form-control gen_password 1" disabled>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Powtórz hasło</label>
							<div class="col-md-6">
								<input type="password" class="form-control pass 1" name="password_confirmation">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Rola użytkownika</label>
							<div class="col-md-6">
								<select class="form-control" name="role">
								@foreach($roles as $role)
									<option value="{{$role->id}}">{{$role->name}}</option>
								@endforeach
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Forma pracy</label>
							<div class="col-md-6">
								<select class="form-control" name="type">
									<option value="godziny">Godziny</option>
									<option value="dni">Dni</option>
									<option value="etat">Etat</option>
									<option value="inne">Inne</option>
								</select>
							</div>
						</div>
						<div class=" col-md-6 col-md-offset-4" style="margin-bottom:20px;">
						 <div class="checkbox ">
						    <label>
						      <input type="checkbox" name="send_mail" value="true"> Wyśli wiadomość z danymi do użytkownika
						    </label>
						  </div>
						 </div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-default">
									Dodaj
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
