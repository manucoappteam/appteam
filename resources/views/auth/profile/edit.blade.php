@extends('app')

@section('content')
<div class="container">
	<div class="row">
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		
				<h2><div class="btn-default"> </div>{{$data->name}} - edycja</h2>
			<div class="col-md-8 col-md-offset-2">
					<div class="profile_edit">
						{!! Form::model($data, array('url' => url('/profile/edit'),'files' => true,'method' => 'put')) !!}
						 <div class="form-group">
						 	<label>Nazwa użytkownika:</label>
						   {!! Form::text('name',null,array('placeholder'=>'Nazwa','class'=>'form-control'))!!}
						 </div>  
						  <div class="form-group">
						  	<label>Nowy adres e-mail (i login):</label>
						   {!! Form::email('mail',null,array('placeholder'=>'Nowy e-mail','class'=>'form-control'))!!}
						 </div> 
						 <div class="form-group"> 
						 	<label>Nowe hasło:</label>
						   {!! Form::password('new_password',array('placeholder'=>'Nowe hasło','class'=>'form-control'))!!}
						 </div>
						 <div class="form-group"> 
						 	<label>Powtórz nowe hasło:</label>
						   {!! Form::password('new_password_confirmation',array('placeholder'=>'Powtórz nowe hasło','class'=>'form-control'))!!}
						 </div>  
						 <div class="form-group">
						 	<label>Telefon:</label>
						   {!! Form::text('phone',null,array('placeholder'=>'Telefon','class'=>'form-control'))!!}
						 </div>  
						 <div class="form-group">
						 	<span class="btn btn-default btn-file">
							   Zmień zdjęcie <input type="file" name="img">
							</span>
						 </div>
						  {!! Form::submit('Zapisz zmiany',array('class'=>'btn btn-success'))!!}
						{!! Form::close() !!}
				</div>
		</div>
	</div>
</div>
@endsection
