@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h2><div class="btn-default"> </div>Profil użytkownika {{$data->name}}</h2>

				<div>
					<div class="col-sm-3">
					<img src="@if(!empty($data->img)){{asset('img/'.$data->img)}}@else{{asset('img/default.png')}}@endif" class="img-responsive">
					</div>
					<div class="col-sm-9">
					<h4>Adres e-mail: {{$data->email}}</h4>
					<h4>Tel: {{$data->phone}}</h4>
					<h4>Forma rozliczania: {{$data->type}}</h4>
					@if(Auth::user()->api)
					<div style="margin-top:50px;">
						<p>Masz już skonfigrowane połączenie z google.</p>
						<p>Jeśli występują jakieś problemy prosimy skontaktować się z administratorem systemu.</p>
					</div>
					@else
					<div style="margin-top:50px;">
						<p>Konfiguracja konta google (kalendarz prywatny)</p>
						<a class="btn-default btn" href="{{url('googleapi_login')}}">Zaloguj</a>
					</div>
					@endif
					</div>
				</div>
		</div>	
	</div>
</div>

@endsection
