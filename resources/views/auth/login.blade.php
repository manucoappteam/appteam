@extends('app')

@section('content')
<?php
$los=rand(1,3);
$photo=asset('photo/'.$los.'-min.JPG');
?>
<style>
	body{
		background-attachment: fixed; background-size: cover; background-image: url('{{$photo}}');
	}
</style>
<div class="container-fluid" >
	<div class="row">
		<div class="container" style="margin-top:5%;">
			<div class="row col-sm-6" style="float:none;">
				<div class="col-sm-10 col-md-offset-2" >
			<h2><div class="btn-default"> </div>Logowanie</h2>
			</div>
			</div>
					@if (count($errors) > 0)
						<div class="alert alert-danger col-sm-10 col-md-offset-1" style="margin-top:30px;">
							Wystąpił problem<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal col-sm-6 row" role="form" method="POST" action="{{ url('/login') }}" style="text-align: left; margin-top:30px;">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-sm-3 control-label">E-Mail</label>
							<div class="col-sm-9">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Hasło</label>
							<div class="col-sm-9">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-9 col-md-offset-3">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="remember"> Zapamiętaj mnie
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-9 col-md-offset-3">
								<button type="submit" class="btn btn-primary" style="float: left;">Zaloguj</button>

								<a class="btn btn-link" href="{{ url('/password/email') }}">Zapomniałeś hasła?</a>
							</div>
						</div>
					</form>

		</div>
	</div>
</div>
@endsection
@section('footer')

@endsection
