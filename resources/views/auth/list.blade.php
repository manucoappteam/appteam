@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<h2><div class="btn-default"> </div>Użytkownicy</h2>
			@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Wystąpił błąd!</strong><br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
			<div class="panel-group" id="accordion">
				
				@foreach($data as $dat)
			  <div class="panel panel-default">
			    <div class="panel-heading">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$dat->id}}">
			         	{{$dat->name}}
			        </a>
			      </h4>
			    </div>
			    <div id="collapse{{$dat->id}}" class="panel-collapse collapse">
			      <div class="panel-body">
			      	{!! Form::model($dat, array('url' => url('/users/'.$dat->id.'/edit'),'files' => true,'method' => 'put')) !!}
						 <div class="form-group">
						   {!! Form::text('name',null,array('placeholder'=>'Nazwa','class'=>'form-control'))!!}
						 </div>  
						  <div class="form-group">
						   {!! Form::email('mail',null,array('placeholder'=>'Nowy e-mail','class'=>'form-control'))!!}
						 </div> 
						 <div class="form-group"> 
						   {!! Form::password('new_password',array('placeholder'=>'Nowe hasło','class'=>'form-control pass '.$dat->id))!!}
						 </div>
						 <div class="form-group" style="margin-bottom:10px; height:34px;">
								<button  class="btn btn-default col-xs-4 gen_password_btn " id="{{$dat->id}}" style="height:34px; line-height:24px;">
									Generuj nowe hasło
								</button>
								<div class="col-xs-8">
								<input type="text" id="" name="gen_password" class="form-control gen_password {{$dat->id}}" disabled>
								</div>
						</div>
						 <div class="form-group" style="margin-top:15px; clear:both"> 
						   {!! Form::password('new_password_confirmation',array('placeholder'=>'Powtórz nowe hasło','class'=>'form-control pass '.$dat->id))!!}
						 </div>  
						 <div class="form-group">
						 	<label>Forma pracy:</label>
						 	{!!Form::select('type', array('godziny'=>'Godziny','dni'=>'Dni','etat'=>'Etat','inne'=>'Inne'),null,array('class'=>'form-control'))!!}
						 </div>
						  <div class="form-group">
						 	<label>Rola użytkownia:</label>
						 	<?php foreach($roles as $role){$tab["$role->id"]=$role->name;}?>
						 	{!!Form::select('role', $tab,null,array('class'=>'form-control'))!!}
						 </div>
						 <div class="checkbox">
						    <label>
						      <input type="checkbox" name="send_mail" value="true"> Wyśli wiadomość z danymi do użytkownika
						    </label>
						  </div>
						  {!! Form::submit('Zapisz zmiany',array('class'=>'btn btn-success'))!!}
					{!! Form::close() !!}
						@can('users_del')
						{!! Form::model($dat, array('url' => url('users/delete/'.$dat->id),'method' => 'delete')) !!}
						<div class="form-group" style="margin-top:15px;">
						  {!! Form::submit('Usuń użytkownika (nieodwracalne)',array('class'=>'btn btn-danger','style'=>'width:200px'))!!}
						</div>
						{!! Form::close() !!}
						@endcan
			       </div>
			    </div>
			  </div>	
			  @endforeach	
			</div>
			<h2><div class="btn-default"> </div>Informacje o rolach</h2>
			<div>
				@foreach($roles as $role)
				<p><strong>{{$role->name}}:</strong><br>
					{{$role->description}}
				</p>
				@endforeach
			</div>
		</div>
	</div>
</div>
@endsection
