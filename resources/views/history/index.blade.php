@extends('app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">	
					<h2><div class="btn-default"> </div>Historia</h2>
					<div class="history">
						<div class="box">
							<table class="table table-striped">
							  <thead>
							    <tr>
							      <th>Typ działania</th>
							      <th>Użytkownik</th>
							      <th>Wydarzenie/Obiekt</th>
							    </tr>
							  </thead>
							  <tbody>
							  	@foreach($history as $item)
									<tr>
										<td>{{$item->type->description}}</td>
										<td>{{$item->user->name}}</td>
										@if($item->event)
										<td>{{$item->event->name}}</td>	
										@else
										<td>{{$item->description}}</td>	
										@endif
									</tr>
								@endforeach	
							  </tbody>
							</table>
						</div>
					{!! $history->render() !!}
					</div>
			</div>
		</div>
	</div>
@endsection
