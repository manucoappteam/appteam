@extends('app')

@section('content')
	<div class="container event single">
		<div class="col-sm-12">
		<a href="{{url('events_list')}}" class="back btn"><img class="icon" src="{{asset('icon/arrow.svg')}}">Powrót</a>
		<input type="hidden" value="{{$data->id}}" id="id_hidden">
		<input type="hidden" value="{{$data->start}}" id="start_hidden">
		<input type="hidden" value="{{$data->end}}" id="end_hidden">
		<div class="row">
			<div class="col-sm-7">
				<h2><div class="btn-default"> </div>Wydarzenie</h2>
				<div class="title">
					<h2>{{$data->name}}</h2>
					@if($data->confirmation!=false)
						<div class="btn p1">Wydarzenie potwierdzone</div>
					@else
						@can('add_events')
							<div>
							{!! Form::model($data, array('url' => 'events/'.$data->id.'/confirmation','method' => 'post')) !!}
									{!! Form::submit('Potwierdź wydarzenie',array('class'=>'btn p0'))!!}
								{!! Form::close() !!}
							</div>
						@else
							<div class="btn p2">Wydarzenie oczekuje na potwierdzenie</div>
						@endcan
					@endif
				</div>
				<div class="row">
					<div class="col-sm-12">
						<h4>Opis:</h4>
						<div>{{$data->description}}</div>
					</div>
				</div>
			</div>
			<div class="col-sm-5">	
				<div class="nav top" style="margin-top:15px;">
					<div class="col-md-4"><a href="{{url('/events/'.$data->id.'/pdf/all')}}" class="btn btn-default">Pobierz</a></div>
					@can('add_events')
					<div class="col-md-4"><a href="{{url('/events/'.$data->id.'/edit')}}" class="btn btn-default">Edytuj</a></div>
					@endcan
					@can('add_events')
					<div class="col-md-4">
						{!! Form::model($data, array('route' => array('events.destroy', $data->id),'method' => 'delete')) !!}
						<button class="btn btn-danger" type="submit">
							Usuń wydarzenie
						</button>
						{!! Form::close() !!}
					</div>
					@endcan
				</div>
				<h4>Termin:</h4>
				<p>Rozpoczęcie: {{substr($data->start, 0, -3)}}</p>
				<p>Zakończenie: {{substr($data->end, 0, -3)}}</p>
				<p>Gotowość: {{substr($data->check, 0, -3)}}</p>
				<h4>Lokalizacja:</h4> 
				<p>{{$data->localization}}</p>
				<h4>Typ wydarzenia:</h4> 
				<p>{{$data->type}}</p>
			</div>
		
		
		</div>
		<div style="clear:both;" ng-controller="events">
		<!-- Zakładki -->
			<ul class="nav nav-pills" role="tablist">
			  <li class="active"><a href="#ekipa" role="tab" data-toggle="tab" id="equipment_list">Ekipa</a></li>
			  @can('comments')
			  <li><a href="#comments" role="tab" data-toggle="tab" ng-click="comments_load()">Komentarze</a></li>
			  @endcan
			  @can('show_equipment_to_event',$data->id)
			  <li><a href="#sprzet" role="tab" data-toggle="tab" ng-click="load_eq()">Sprzęt</a></li>
			  @endcan
			  @can('riders_show')
			  <li><a href="#ridery" role="tab" data-toggle="tab" ng-click="load_rider()">Ridery</a></li>
			  @endcan
			  @can('inputlist_show')
			  <li><a href="#inputlist" role="tab" data-toggle="tab" ng-click="inputlist()">Inputlist</a></li>
			  @endcan
			   @can('add_events')
			  <li><a href="#alerts" role="tab" data-toggle="tab" ng-click="alerts()">Powiadomienia</a></li>
			  @endcan
			</ul>
			<div class="alert alert-success alert-dismissable" ng-if="info!=null" style="margin-top:15px">
			  <button type="button" class="close" ng-click="close()">&times;</button>
			  <%info%>
			</div>
		
		<!-- Zawartość zakładek -->
		<div class="tab-content" style="margin-top:20px;">
		  <div class="tab-pane active" id="ekipa">
		  	<ul class="nav nav-pills" role="tablist">
		  	  @can('users_to_event')
			  <li class="active"><a href="#ekipa_list" id="ekipa_list_ob" role="tab" data-toggle="tab" ng-click="load_ekipa()">Obecna ekipa</a></li>
			  <li><a href="#ekipa_add" id="ekipa_add_event" role="tab" data-toggle="tab" ng-click="load_users_all()">Dodaj</a></li>
			  @endcan
			  @can('check_users_on_event',$data->id)	
			  <button class="btn btn-success" ng-click="add_time_all()" style="width:200px; float:right;">Dodaj czas pracy (dla wszystkich)</button>
			  @endcan
			</ul>
			<div class="tab-content" style="margin-top:20px;">
			 <div class="tab-pane active" id="ekipa_list" >
			 	<table class="table table-striped">
				  <thead>
				    <tr>
				      <th ng-click="order('name')">Imię i nazwisko</th>
				      <th ng-click="order('pivot.function')">Funkcja</th>
				      <th ng-click="order('pivot.status')">Status</th>
				      @if(Auth::user()->can('check_users_on_event',$data->id)||(Auth::user()->can('timework_self')&&Auth::user()->events()->find($data->id)))
				       <th>Czas pracy</th>   
				      @endif
				      @can('users_to_event')
				       <th>Usuń</th>
				      @endcan
				    </tr>
				  </thead>
				  <tbody>
				  		<div class="loading-spiner-holder" data-loading ><div class="loader">Trwa ładowanie...</div></div>
						<tr ng-repeat="dat in ekipa | orderBy:predicate:reverse">
							<td><%dat.name%></td>
							<td><%dat.pivot.function%></td>
							<td ng-if="dat.pivot.check==null">Oczekuje na potwierdzenie</td><td ng-if="dat.pivot.check==true">Potwierdzono</td>
							  @if(Auth::user()->can('check_users_on_event',$data->id))					
							<td>	
								<button class="btn btn-info" ng-click="add_time_user_all(dat.id)" ng-if="dat.type=='godziny'||dat.type=='dni'">Całe wydarzenie</button>
								<button class="btn btn-info" data-toggle="modal" data-target="#addTime" ng-if="dat.type=='godziny'||dat.type=='dni'" id="<%dat.id%>" onclick="document.getElementById('id_addTime').value=this.id;">Inny czas</button>			
							</td>
							 @elseif(Auth::user()->can('timework_self'))
							 <td>	
								<button class="btn btn-info" ng-click="add_time_user_all(dat.id)" ng-if="(dat.type=='godziny'||dat.type=='dni')&&dat.id=={{Auth::user()->id}}">Całe wydarzenie</button>
								<button class="btn btn-info" data-toggle="modal" data-target="#addTime" ng-if="(dat.type=='godziny'||dat.type=='dni')&&dat.id=={{Auth::user()->id}}" id="<%dat.id%>" onclick="document.getElementById('id_addTime').value=this.id;">Inny czas</button>			
							</td>
							 @endif
							 
							  @can('users_to_event')
						       <td><button class="btn btn-danger" ng-click="del_user(dat.id)">Usuń</button></td>
						      @endcan	
						</tr>
				  </tbody>
				</table>
				<!--okienko do dodawania innego czasu pracy-->
				<div class="modal fade" id="addTime" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					  <div class="modal-dialog">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
					        <h4 class="modal-title" id="myModalLabel">Dodaj czas pracy</h4>
					      </div>
					      <div class="modal-body">
					        <div class="alert alert-success alert-dismissable" ng-if="info_add!=null">
							  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							  <%info_add%>
							</div>
							<form id="oddo">
							<input type="hidden" id="id_addTime" name="id">
							<div class="form-group">
							  	<label style="display:block;">Od</label>
							  	{!!Form::selectRange('start_day',1,31,date('d',strtotime($data->start)))!!}
								{!!Form::select('start_month', array('Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'),date('m',strtotime($data->start))-1)!!}
								{!!Form::selectRange('start_year',2015,2035,date('Y',strtotime($data->start)))!!}
								- 
								{!!Form::selectRange('start_hour',0,23,date('H',strtotime($data->start)))!!}
								{!!Form::selectRange('start_minutes',00,59,date('i',strtotime($data->start)))!!}
							</div>	
							<div class="form-group">
							  	<label style="display:block;">Do</label>
							  	{!!Form::selectRange('end_day',1,31,date('d',strtotime($data->end)))!!}
								{!!Form::select('end_month', array('Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'),date('m',strtotime($data->end))-1)!!}
								{!!Form::selectRange('end_year',2015,2035,date('Y',strtotime($data->end)))!!}
								-
								{!!Form::selectRange('end_hour',0,23,date('H',strtotime($data->end)))!!}
								{!!Form::selectRange('end_minutes',00,59,date('i',strtotime($data->end)))!!}
							</div>
							   <div class="checkbox">
						  		 <label>
								{!!Form::checkbox('all_day',1)!!} Pełny dzień/dni
								</label>
							</div>	
							</form>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
					        <button type="button" class="btn btn-default" ng-click="add_time_user_spec()">Dodaj</button>
					      </div>
					    </div>
					  </div>
					</div>	
			<!--okienko do dodawania innego czasu pracy END-->
			  </div>
			    @can('users_to_event')
			   <div class="tab-pane" id="ekipa_add" >
			 	<table class="table table-striped">
				  <thead>
				    <tr>
				      <th ng-click="order('name')">Imię i nazwisko</th>
				      <th ng-click="order('status')">Status</th>
				      @can('users_to_event')
				      <th>Funkcja (opcjonalnie)</th>   
				       <th>Dodaj</th>
				      @endcan
				    </tr>
				  </thead>
				  <tbody>
				  		<div class="loading-spiner-holder" data-loading ><div class="loader">Trwa ładowanie...</div></div>
						<tr ng-repeat="dat in users track by $index | orderBy:predicate:reverse" ng-class="dat.status_b" ng-if="dat!=null">
							<td><%dat.name%></td>
							<td><%dat.status%></td>
							  @can('users_to_event')
							  <td>
							  	<select id="function_user" class="form-control" ng-model="function_user">
							  	<option value="brak">Brak</option>
							  	<option value="Szef ekipy">Szef ekipy</option>
							  	<option value="Realizator dźwięku">Realizator dźwięku</option>
							  	<option value="Realizator światła">Realizator światła</option>
							  	<option value="Realizator FOH">Realizator FOH</option>
							  	<option value="Realizator MON">Realizator MON</option>
							  	<option value="Technik">Technik</option>
							  	<option value="Technik dźwieku">Technik dźwieku</option>
							  	<option value="Technik światła">Technik światła</option>
							  	<option value="Pomoc">Pomoc</option>
							    </select>
							  </td>
						       <td><button class="btn btn-info" ng-click="add_user(dat.id,dat.status_b,function_user)" >Dodaj</button></td>
						      @endcan	
						</tr>
				  </tbody>
				</table>
			  </div>
			  @endcan
			</div>
		  </div>
		  @can('comments')
		  	@include('event.comment')
		  @endcan
		   @can('show_equipment_to_event',$data->id)
		  <div class="tab-pane" id="sprzet">
		  	<ul class="nav nav-pills" role="tablist">
		  	 @can('equipment_to_event')
			  <li class="active"><a href="#eq_list" id="equipment_list" role="tab" data-toggle="tab" ng-click="load_eq()">Lista</a></li>
			  
			  <li><a href="#eq_add" id="equipment_add_event" role="tab" data-toggle="tab" ng-click="load_eq_all()">Dodaj</a></li>
			  @endcan
			  <li style="float:right;">
						<button class="btn btn-success" data-toggle="modal" data-target="#create_pdf">
							 Pobierz
						</button>
			  </li>
			</ul>
			<!--create pdf-->
			<div class="modal fade" id="create_pdf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
			        <h4 class="modal-title" id="myModalLabel">Pobierz pdf</h4>
			      </div>
			      <form method="get" action="{{url('events/'.$data->id.'/pdf/eq')}}">
			      <div class="modal-body">
			      	<select id="category" class="form-control" name="cat">
							<option value="all">Wyszystkie</option>
							@foreach($category as $cat)
								<option value="{{$cat}}">{{$cat}}</option>
							@endforeach
						</select>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
			        <button type="submit" class="btn btn-default">Pobierz</button>
			      </div>
			      </form>
			    </div>
			  </div>
			</div>
			<!--create pdf END-->
			<div class="tab-content" style="margin-top:20px;">
			 <div class="tab-pane active" id="eq_list" >
			 	<table class="table table-striped">
				  <thead>
				    <tr>
				      <th ng-click="order('name')">Nazwa</th>
				      <th ng-click="order('pivot.count')">Liczba</th>
				      <th ng-click="order('type')">Kategoria</th>
				      @can('check_equipment_to_event',$data->id)
				       <th ng-click="order('check_before')">Sprawdzenie przed</th>
				       <th ng-click="order('check_after')">Sprawdzenie po</th>
				       <th ng-click="order('cout_after')">Zwrócona liczba</th>
				      @endcan
				      @can('equipment_to_event')
				       <th>Usuń</th>
				      @endcan
				    </tr>
				  </thead>
				  <tbody>
				  		<div class="loading-spiner-holder" data-loading ><div class="loader">Trwa ładowanie...</div></div>
						<tr ng-repeat="dat in data | orderBy:predicate:reverse">
							<td ng-if="dat.name==null"><%dat.oder%></td><td ng-if="dat.name"><%dat.name%></td>
							@can('equipment_to_event')
							<td ng-if="dat.pivot.count==null"><input type="number" min="1" ng-model="dat.count" ng-change="change_count(dat.count,null,dat.id)"></td><td ng-if="dat.pivot.count"><input type="number" min="1" ng-model="dat.pivot.count" ng-change="change_count(dat.pivot.count,dat.id,dat.pivot.id)"></td>
							@else
							<td ng-if="dat.pivot.count==null"><%dat.count%></td><td ng-if="dat.pivot.count"><%dat.pivot.count%></td>
							@endcan
							<td><%dat.type%></td>
							 @can('check_equipment_to_event',$data->id)						
							<td ng-if="dat.check_before==null"><input type="checkbox"   ng-model="dat.pivot.check_before" ng-true-value='1' ng-false-value='0' ng-change="check_before(dat.pivot.check_before,dat.pivot.id,dat.id)"></td>
							<td ng-if="dat.check_after==null"><input type="checkbox"   ng-model="dat.pivot.check_after" ng-true-value='1' ng-false-value='0' ng-change="check_after(dat.pivot.check_after,dat.pivot.id,dat.id)"></td>
							<td ng-if="dat.pivot.check_before==null" ><input type="checkbox"  ng-model="dat.check_before" ng-true-value='1' ng-false-value='0' ng-change="check_before(dat.check_before,dat.pivot.id,dat.id)"></td>
							<td ng-if="dat.pivot.check_after==null"><input type="checkbox"   ng-model="dat.check_after" ng-true-value='1' ng-false-value='0' ng-change="check_after(dat.check_after,dat.pivot.id,dat.id)"></td>
							<td ng-if="dat.pivot!=null"><input type="text" ng-model="dat.pivot.count_after" ng-change="count_after(dat.pivot.count_after,dat.pivot.id,dat.pivot.count,dat.id)"></td>
							<td ng-if="dat.pivot==null"><input type="text" ng-model="dat.count_after" ng-change="count_after(dat.count_after,dat.pivot.id,dat.count,dat.id)"></td>
							 @endcan
							  @can('equipment_to_event')
						       <td><button ng-if="dat.pivot.id==null" class="btn btn-danger" ng-click="del_eq(dat.id)">Usuń</button><button ng-if="dat.pivot.id" class="btn btn-danger" ng-click="del_eq(dat.pivot.id)">Usuń</button></td>
						      @endcan	
						</tr>
				  </tbody>
				</table>
			  </div>
		  	<div class="tab-pane equipment" id="eq_add">
		  		<div class="nav" style="margin-top:20px;">
				<ul class="form-inline">
					<li>Kategoria: </li>
					<li>
						<select id="category" class="form-control" ng-model="search.type">
							<option value="">Wyszystkie</option>
							@foreach($category as $cat)
								<option value="{{$cat}}">{{$cat}}</option>
							@endforeach
						</select>
					</li>
					<li>
						<input class="form-control" id="search_input" placeholder="Szukaj.." ng-model="search.name">
					</li>
					<li style="float:right;">
						<button class="btn btn-success" style="width:180px;" data-toggle="modal" data-target="#add_eg_out">
							 Dodaj (sprzęt spoza magazynu)
						</button>
					</li>
				</ul>
			</div>
			<!--dodawanie sprzetu spoza magazynu-->
			<div class="modal fade" id="add_eg_out" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
			        <h4 class="modal-title" id="myModalLabel"> Dodaj sprzęt spoza magazynu</h4>
			      </div>
			      <div class="modal-body">
			      	<div class="alert alert-success alert-dismissable" ng-if="info_out!=null">
					  <button type="button" class="close" ng-click="close_out()">&times;</button>
					  <%info_out%>
					</div>
			      	<div class="form-group"><input ng-model="name_out" type="text" placeholder="Nazwa" class="form-control"></div>
			        <div class="form-group"><input ng-model="count_out" type="number" min="1" class="form-control"></div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
			        <button type="button" class="btn btn-default" ng-click="add_eq_out(count_out,name_out)">Dodaj</button>
			      </div>
			    </div>
			  </div>
			</div>
			<!--dodawanie sprzetu spoza magazynu END-->
			
			<div class="box">
				<table class="table table-striped">
				  <thead>
				    <tr>
				      <th ng-click="order('name')">Nazwa</th>
				      <th ng-click="order('count')">Liczba</th>
				      <th ng-click="order('type')">Kategoria</th>
				       <th ng-click="order('info')">Liczba dostępna</th>
				      <th>Dodaj</th>
				    </tr>
				  </thead>
				  <tbody>
				  		<div class="loading-spiner-holder" data-loading ><div class="loader">Trwa ładowanie...</div></div>
						<tr ng-repeat="data in eq | filter:search |orderBy:predicate:reverse track by $index "  ng-if="data!=null">
							
							<td><a href="{{url('/equipment/<%data.id%>')}}"><%data.name%></a></td>
							
							<td><%data.count%></td>
							<td><%data.type%></td>	
							<td><%data.info%></td>	
							<td><input type="number" ng-model="count_add" class="form-control" min="1"><button class="btn btn-info" ng-click="add_eq(count_add,data.id,data.info)">Dodaj</button></td>
						
						</tr>
				  </tbody>
				</table>
			</div>
		  	</div>
		  </div>
		  		
		  </div>
		   @endcan
		   @can('riders_show')
		  <div class="tab-pane" id="ridery">
		  	@can('riders')
		  	<button class="btn btn-default" data-toggle="modal" data-target="#add_rider">
							 Dodaj
			</button>
			@endcan
			<!--dodawanie rideru-->
			<div class="modal fade" id="add_rider" tabindex="-1" role="dialog" aria-labelledby="Dodajrider" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
			        <h4 class="modal-title" id="myModalLabel"> Dodaj rider</h4>
			      </div>
			      <div class="modal-body">
			      	<div class="alert alert-success alert-dismissable" ng-if="info_out!=null">
					  <button type="button" class="close" ng-click="close_out()">&times;</button>
					  <%info_out%>
					</div>
			      	<div class="form-group"><input ng-model="name_rider" type="text" placeholder="Nazwa" class="form-control"></div>
			        <div class="form-group"><select class="form-control" ng-model="type_rider">
			        	<option value="sound">Dźwiek</option>
			        	<option value="ligth">Światło</option>
			        	<option value="soundligth">Światło i Dźwięk</option>
			        	<option value="inne">Inny</option>
			        </select></div>
			        <input type="hidden" id="file_rider">
			        <span class="btn btn-success fileinput-button">
				        <i class="glyphicon glyphicon-plus"></i>
				        <span>Wybierz plik...</span>
				        <!-- The file input field used as target for the file upload widget -->
				        <input id="fileupload" type="file" name="files[]">
				    </span>
				    <div id="progress" class="progress" style="margin-top:10px;">
				        <div class="progress-bar progress-bar-success"></div>
				    </div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
			        <button type="button" class="btn btn-default" ng-click="add_rider(name_rider,type_rider)">Dodaj</button>
			      </div>
			    </div>
			  </div>
			</div>
			<!--dodawanie eideru END-->
			<table class="table table-striped">
				  <thead>
				    <tr>
				      <th ng-click="order('name')">Nazwa</th>
				      <th ng-click="order('type')">Typ</th>
				      @can('riders')<th>Usuń</th>@endcan
				    </tr>
				  </thead>
				  <tbody>
				  		<div class="loading-spiner-holder" data-loading ><div class="loader">Trwa ładowanie...</div></div>
						<tr ng-repeat="data in riders | filter:search |orderBy:predicate:reverse track by $index "  ng-if="data!=null">
							
							<td><a href="{{asset('<%data.file_url%>')}}"><%data.name%></a></td>
							<td><%data.type%></td>	
							 @can('riders')
						       <td><button class="btn btn-danger" ng-click="del_rider(data.id)">Usuń</button></td>
						      @endcan	
							
						</tr>
				  </tbody>
			</table>
		  </div>
		  @endcan
		  @can('inputlist_show')
		  <div class="tab-pane" id="inputlist">
		  	@can('inputlist')
		  	<form action="{{url('inputlist/create')}}">
		  		<input type="hidden" name="event_id" value="{{$data->id}}" />
		  		<input type="submit" class="btn btn-success" value="Dodaj inputlistę" />
		  	</form>
		  	@endcan
		  	<table class="table table-striped">
				  <thead>
				    <tr>
				      <th>Nazwa</th>
				      <th></th>
				      @can('inputlist')
				        <th>Usuń</th>
				      @endcan
				    </tr>
				  </thead>
				  <tbody>
				  		<div class="loading-spiner-holder" data-loading ><div class="loader">Trwa ładowanie...</div></div>
						<tr ng-repeat="data in inputlists">
							
							<td><a href="{{url('inputlist')}}/<%data.id%>"><%data.name%></a></td>
							<td><a class="btn btn-default" href="{{url('inputlist')}}/<%data.id%>/pdf">Pobierz</a></td>
							@can('inputlist')
							<td><button class="btn btn-danger" ng-click="del_inputlist(data.id)">Usuń</button></td>
							@endcan
						</tr>
				  </tbody>
			</table>
			
		  </div>
		  @endcan
		  @can('add_events')
		  <div class="tab-pane" id="alerts">
			<button class="btn btn-success" data-toggle="modal" data-target="#add_alerts">
				 Dodaj powiadmienie
			</button>

			<!--create pdf-->
			<div class="modal fade" id="add_alerts" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
			        <h4 class="modal-title" id="myModalLabel">Dodaj powiadomienie</h4>
			      </div>			     
			      <div class="modal-body">
			      	 <div class="alert alert-success alert-dismissable" ng-if="info_out!=null">
					  <button type="button" class="close" ng-click="close_out()">&times;</button>
					  <%info_out%>
					</div>
					<div class="form-group">
						<label>Powiadomienie dla:</label>	
			      	<select id="for_who" class="form-control"  ng-model="for_who">
							<option value="events_show">Wszyscy użytkownicy *</option>
							<option value="one">Konkretny użytkownik</option>
					</select>
					</div>
					<div class="form-group">
					<select id="for_one" class="form-control" ng-model="for_one" style="display: none;">
							@foreach($users as $user)
							<option value="{{$user->id}}">{{$user->name}}</option>
							@endforeach
					</select>
					</div>
					<div class="form-group">
					<label>Termin:</label>
					<select id="time" class="form-control" ng-model="time">
							<option value="now">Teraz</option>
							<option value="one_day">1 dzień przed</option>
							<option value="one_week">1 tydzień przed</option>
							<option value="two_week">2 tygodnie przed</option>
					</select>
					</div>
					<div class="form-group">
					<label>Forma:</label>
					<select id="by_what" class="form-control"  ng-model="by_what">
							<option value="app">Aplikacja</option>
							<option value="mail">Mail</option>
							<option value="sms" disabled="">Sms</option>
					</select>
					</div>
					<p>* Wszyscy użytkownicy którzy mogą zobaczyć to wydarzenie</p>	
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
			        <button type="submit" class="btn btn-default" ng-click="add_alert(for_who,for_one,time,by_what)">Dodaj</button>
			      </div>
			      </form>
			    </div>
			  </div>
			</div>
			<!--create pdf END-->
		  	<table class="table table-striped">
				  <thead>
				    <tr>
				      <th>Dla kogo</th>
				      <th>Czas</th>
				      <th>Forma</th>
				       <th>Usuń</th>

				    </tr>
				  </thead>
				  <tbody>
				  		<div class="loading-spiner-holder" data-loading ><div class="loader">Trwa ładowanie...</div></div>
						<tr ng-repeat="data in alert track by $index "  ng-if="data!=null">
							
							<td><%data.for_who%></td>
							<td><%data.times%></td>
							<td><%data.by_what%></td>		
							<td><button class="btn btn-danger" ng-click="del_alert(data.id)">Usuń</button></td>

						</tr>
				  </tbody>
			</table>
			
		  </div>
		  @endcan
		</div>
		</div>
		</div>
	</div>
@endsection
