@extends('app')

@section('content')
	
	<div class="container">
		<a href="{{url('/events_list')}}" class="back btn"><img class="icon" src="{{asset('icon/arrow.svg')}}">Powrót</a>
		
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<h2><div class="btn-default"> </div>Nowe wydarzenie</h2>
		
		<div class="event">
		{!! Form::open( array('route' => array('events.store'))) !!}
		<div class="row">
			<div class="col-md-8">
			 <div class="form-group">
			   {!! Form::text('name',null,array('placeholder'=>'Nazwa','class'=>'form-control'))!!}
			 </div>  
			 <div class="form-group">
			   {!! Form::textarea('description',null,array('placeholder'=>'Opis','class'=>'form-control'))!!}
			 </div> 
			 <div class="form-group">
			   {!! Form::text('localization',null,array('placeholder'=>'Miejsce','class'=>'form-control'))!!}
			 </div>
			 </div> 
			<div class="col-md-4 time">  
			  <div class="form-group">
			  	<label style="display:block;">Rozpoczęcie</label>
			  	{!!Form::selectRange('start_day',1,31,$data['day'])!!}
				{!!Form::select('start_month', array('Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'),($data['month']-1))!!}
				{!!Form::selectRange('start_year',2015,2035,$data['year'])!!}
				  - 
				{!!Form::selectRange('start_hour',0,23,12)!!}
				{!!Form::selectRange('start_minutes',00,59,00)!!}
		      </div>
		      <div class="form-group">
			  	<label style="display:block;">Zakończenie</label>
			  	{!!Form::selectRange('end_day',1,31,$data['day'])!!}
				{!!Form::select('end_month', array('Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'),($data['month']-1))!!}
				{!!Form::selectRange('end_year',2015,2035,$data['year'])!!}
				  - 
				{!!Form::selectRange('end_hour',0,23,12)!!}
				{!!Form::selectRange('end_minutes',00,59,00)!!}
				   <div class="checkbox">
				  		 <label>
						{!!Form::checkbox('all_day',1)!!} Pełny dzień/dni
						</label>
					</div>
		      </div>
		       <div class="form-group">
			  	<label style="display:block;">Gotowość</label>
			  	{!!Form::selectRange('check_day',1,31,$data['day'])!!}
				{!!Form::select('check_month', array('Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'),($data['month']-1))!!}
				{!!Form::selectRange('check_year',2015,2035,$data['year'])!!}
				  - 
				{!!Form::selectRange('check_hour',0,23,12)!!}
				{!!Form::selectRange('check_minutes',00,59,00)!!}
		      </div>
		      <div class="form-group">
			      <div class="checkbox">
					   <label>
						{!!Form::checkbox('confirmation',1)!!}Potwierdzenie wydarzenia
					   </label>
				  </div>
			 </div>
			  <div class="form-group">
			    <label>Typ wydarzenia</label>
			    {!!Form::select('type', array('koncert'=>'koncert','impreza firmowa'=>'impreza firmowa','wypożyczenie'=>'wypożyczenie', 'inne'=>'inne'),'koncert',array('class'=>'form-control'))!!}
			 </div>
		 </div>
		</div>
		 <div class="row">
		 	<div class="col-sm-12">
		  {!! Form::submit('Dodaj',array('class'=>'btn btn-success'))!!}
		  </div>
		  </div>
		{!! Form::close() !!}
		</div>
	</div>
@endsection
