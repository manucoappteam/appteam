@extends('app')

@section('content')
	<div class="container">
		<a href="{{URL::previous()}}" class="back btn"><img class="icon" src="{{asset('icon/arrow.svg')}}">Powrót</a>
		
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		
		<h2><div class="btn-default"> </div>Edytuj wydarzenie - {{$data->name}}</h2>
		
		<div class="event">
		{!! Form::model( $data,array('route' => array('events.update', $data->id),'method' => 'put')) !!}
		<div class="row">
			<div class="col-md-8">
			 <div class="form-group">
			   {!! Form::text('name',null,array('placeholder'=>'Nazwa','class'=>'form-control'))!!}
			 </div>  
			 <div class="form-group">
			   {!! Form::textarea('description',null,array('placeholder'=>'Opis','class'=>'form-control'))!!}
			 </div> 
			 <div class="form-group">
			   {!! Form::text('localization',null,array('placeholder'=>'Miejsce','class'=>'form-control'))!!}
			 </div>
			 </div> 
			<div class="col-md-4 time">  
			  <div class="form-group">
			  	<label style="display:block;">Rozpoczęcie</label>
			  	{!!Form::selectRange('start_day',1,31)!!}
				{!!Form::select('start_month', array('Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'))!!}
				{!!Form::selectRange('start_year',2015,2035)!!}
				  - 
				{!!Form::selectRange('start_hour',0,23)!!}
				{!!Form::selectRange('start_minutes',00,59)!!}
		      </div>
		      <div class="form-group">
			  	<label style="display:block;">Zakończenie</label>
			  	{!!Form::selectRange('end_day',1,31)!!}
				{!!Form::select('end_month', array('Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'))!!}
				{!!Form::selectRange('end_year',2015,2035)!!}
				  - 
				{!!Form::selectRange('end_hour',0,23)!!}
				{!!Form::selectRange('end_minutes',00,59)!!}
				   <div class="checkbox">
				  		 <label>
						{!!Form::checkbox('all_day',1)!!} Pełny dzień/dni
						</label>
					</div>
		      </div>
		       <div class="form-group">
			  	<label style="display:block;">Gotowość</label>
			  	{!!Form::selectRange('check_day',1,31)!!}
				{!!Form::select('check_month', array('Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'))!!}
				{!!Form::selectRange('check_year',2015,2035)!!}
				  - 
				{!!Form::selectRange('check_hour',0,23)!!}
				{!!Form::selectRange('check_minutes',00,59)!!}
		      </div>
		      <div class="form-group">
			      <div class="checkbox">
					   <label>
						{!!Form::checkbox('confirmation',1)!!}Potwierdzenie wydarzenia
					   </label>
				  </div>
			 </div>
			  <div class="form-group">
			    <label>Typ wydarzenia</label>
			    {!!Form::select('type', array('koncert'=>'koncert','impreza firmowa'=>'impreza firmowa','wypożyczenie'=>'wypożyczenie', 'inne'=>'inne'),'koncert',array('class'=>'form-control'))!!}
			 </div>
		 </div>
		</div>
		 <div class="row">
		 	<div class="col-sm-12">
		  {!! Form::submit('Zapisz zmiany',array('class'=>'btn btn-success'))!!}
		  </div>
		  </div>
		{!! Form::close() !!}
		</div>
	</div>
@endsection
