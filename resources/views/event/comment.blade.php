 <div class="tab-pane" id="comments">
				  		<div class="loading-spiner-holder" data-loading ><div class="loader">Trwa ładowanie...</div></div>
						<div ng-repeat="dat in comments" class="row" style="border-bottom:1px solid; padding-bottom:5px;padding-top:5px;">
							<div class="user_comment col-xs-2 col-sm-1 col-md-1">
								<img class="img-responsive img-circle" src="{{url('img')}}/<%dat.user.img%>" ng-if="dat.user.img!=''" style="width:50px; height:50px; margin:0 auto;" title="<%dat.user.name%>">
								<img class="img-responsive img-circle" src="{{url('img')}}/default.png" ng-if="dat.user.img==''" style="width:50px; height:50px; margin:0 auto;" title="<%dat.user.name%>">
								<p style="text-align: center;"><%dat.user.name%></p>
							</div>
							<div class="col-sx-10 col-sm-11 col-md-11">
								<p style="font-weight: bold"><%dat.title%></p>
								<p><%dat.description%></p>
								<a ng-click="comment_del(dat.id)" ng-if="dat.user.id=={{Auth::user()->id}}">Usuń</a>
							</div>
						</div>
					<div class="comment add row" style="margin-top:10px;">
						<div class="user_comment col-xs-2 col-sm-1 col-md-1">
							@if(Auth::user()->img)
							<img class="img-responsive img-circle" src="{{url('img')}}/{{Auth::user()->img}}" style="width:50px; height:50px; margin:0 auto;" title="<%dat.user.name%>">
							@else
							<img class="img-responsive img-circle" src="{{url('img')}}/default.png"  style="width:50px; height:50px; margin:0 auto;" title="<%dat.user.name%>">
							@endif
						</div>
						<div class="col-sx-10 col-sm-11 col-md-11">
								<div class="form-group">
								<input type="text" ng-model="comment.title" placeholder="Tytuł" ng-init="comment.title=''" class="form-control">
								</div>
								<div class="form-group">
								<textarea ng-model="comment.description" placeholder="Treść..." ng-init="comment.description=''" class="form-control"></textarea>
								</div>
								<button class="btn btn-success" ng-click="comment_add(comment.title,comment.description)">Dodaj</button>
							</div>
					</div>	
</div>						