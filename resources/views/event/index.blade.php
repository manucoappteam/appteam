@extends('app')

@section('content')
	<div class="container">
		<h2><div class="btn-default"> </div>Wydarzenia</h2>
		
		<?php
		$month=intval($month);
		$year=intval($year);
			if($month==1){
				$back=($year-1).'/12';
				$next=$year.'/'.($month+1);
			}
			elseif($month==12){
				$back=$year.'/'.($month-1);
				$next=($year+1).'/1';
			}
			else{
				$back=$year.'/'.($month-1);
				$next=$year.'/'.($month+1);
			}
		
		?>
		
		 <div>
		    <button type="button" class="btn btn-default active" id="calender">Kalendarz</button>
		    <button type="button" class="btn btn-default" id="list">Lista</button>
		    @can('add_events')
			<div class="pull-right"><a href="{{url('/events/create')}}" class="btn btn-default">Dodaj wydarzenie<img class="icon" src="{{asset('icon/plus_inver.svg')}}"></a></div>
			@endcan
		  </div>
		  <div class="nav_calender">
			<a href="{{url('/events_list/'.$back)}}" class="btn-info">&lt;</a>
			<?php
			$m=array('styczeń','luty','marzec','kwiecień','maj','czerwiec','lipiec','sierpień','wrzesień','październik','listopad','grudzień');
			?>
			<span>{{$m[date("n",strtotime($year.'-'.$month))-1]}} {{$year}}</span>
			<a href="{{url('/events_list/'.$next)}}" class="btn-info">&gt;</a>
		</div>
		<div class="events_list">
			<?php $days = date("t",strtotime($year.'-'.$month));?>
			<?php $days_week = date("N",strtotime($year.'-'.$month.'-01'));?>
			<ul class="calender">
				<li class="day week">Pn</li>
				<li class="day week">Wt</li>
				<li class="day week">Śr</li>
				<li class="day week">Czw</li>
				<li class="day week">Pt</li>
				<li class="day week">So</li>
				<li class="day week">N</li>
				<?php  $d=1; $end=null;?>
				@for($i=1;$i<=42;$i++)
					<li class="day" style="@if($i%7==1) clear:both @endif">
						@if($days_week<=$i&&$d<=$days)
						<div>
						<div class="number">{{$d}}</div>
						<div class="week_day">
						{{date("j.m.Y",strtotime($year.'-'.$month.'-'.$d))}}, 
						<?php switch(date("N",strtotime($year.'-'.$month.'-'.$d))){
							case 1 : echo 'Poniedziałek'; break;
							case 2 : echo 'Wtorek'; break;
							case 3 : echo 'Środa'; break;
							case 4 : echo 'Czwartek'; break;
							case 5 : echo 'Piątek'; break;
							case 6 : echo 'Sobota'; break;
							case 7 : echo 'Niedziela'; break;
						}
						?>
						</div>
						@can('add_events')
							{!! Form::open( array('route' => array('events.create'),'method'=>'get')) !!}
							{!!Form::hidden('year',$year)!!}
							{!!Form::hidden('month',$month)!!}
							{!!Form::hidden('day',$d)!!}
							 {!! Form::submit('Dodaj Wydarzenie',array('class'=>'btn btn-primary event add'))!!}
							{!! Form::close() !!}
						@endcan
						@if(isset($data[$d]))
							@foreach($data[$d] as $event)
							@can('show_events',$event['id'])
							<a class="event" href="{{asset('/events/'.$event['id'])}}" @if($event['type']=='wypożyczenie') style="color:rgb(40,40,120)" @endif>{{$event['name']}}</a>
							<?php 
							if($event['start']!=$event['end']){
							$end[$event['id']]['end']=$event['end'];
							$end[$event['id']]['start']=$d;		
							}
							?>
							@endcan
							@endforeach
						@endif
						@if($end!=null)
							@foreach($end as $key => $tab)
								@if($tab!=null)
									<?php  $ends=date("j",strtotime($tab['end']));?>
									@if($ends>=$d&&$d>$tab['start'])
										@foreach($data[$tab['start']] as $event)
										 @if($key==$event['id'])
										 @can('show_events')
										<a class="event" href="{{asset('/events/'.$event['id'])}}" @if($event['type']=='wypożyczenie') style="color:rgb(40,40,120)" @endif>{{$event['name']}}</a>
										@endcan
										 @endif		
										@endforeach
										<?php if($ends==$d)
											{$end[$key]=null;
											}
										?>
									@endif
								@endif	
							@endforeach
						@endif
						
						<?php $d++;?>
						</div>
						@endif
							
					 </li>
				@endfor
			</ul>
		</div>
	</div>
@endsection
