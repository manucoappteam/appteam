@extends('app')

@section('content')
	<div class="container">
		<div class="row">
		<div class="col-md-12">
		<h2><div class="btn-default"> </div>Moje planowane nieobecności</h2>
		<div class="equipment" ng-controller="absence">
		<div class="align-right"><button class="btn btn-default" data-toggle="modal" data-target="#addModal">Dodaj</button></div>	
		<!--okienko do dodawania-->
		<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
			        <h4 class="modal-title" id="myModalLabel">Dodaj planowaną nieobecność</h4>
			      </div>
			      <div class="modal-body">
			        <div class="alert alert-success alert-dismissable" ng-if="info_add!=null">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					  <%info_add%>
					</div>
					<form id="oddo">
					<div class="form-group">
					  	<label style="display:block;">Od</label>
					  	{!!Form::selectRange('start_day',1,31,date('d'))!!}
						{!!Form::select('start_month', array('Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'),date('m')-1)!!}
						{!!Form::selectRange('start_year',2015,2035,date('Y'))!!}
					</div>	
					<div class="form-group">
					  	<label style="display:block;">Do</label>
					  	{!!Form::selectRange('end_day',1,31,date('d'))!!}
						{!!Form::select('end_month', array('Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'),date('m')-1)!!}
						{!!Form::selectRange('end_year',2015,2035,date('Y'))!!}
					</div>	
					</form>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
			        <button type="button" class="btn btn-default" ng-click="add()">Dodaj</button>
			      </div>
			    </div>
			  </div>
			</div>	
			<div class="box">
				<table class="table table-striped">
				  <thead>
				    <tr>
				      <th>Od</th>
				      <th>Do</th>
				       <th>Usuń</th>
				    </tr>
				  </thead>
				  <tbody>
				  		<div class="loading-spiner-holder" data-loading ><div class="loader">Trwa ładowanie...</div></div>
						<tr ng-repeat="data in absence">
							<td><%data.start['all']%></a></td>
							<td><%data.end['all']%></td>
							<td><button class="btn btn-danger" ng-click="del(data.id)">Usuń</button></td>	
						</tr>
				  </tbody>
				</table>
			</div>
		</div>
		</div>
		</div>
	</div>
@endsection
