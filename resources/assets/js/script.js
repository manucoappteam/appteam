$('document').ready(function(){
			
			$('li.dropdown.mega-dropdown a').on('click', function (event) {
   				 $(this).parent().toggleClass('open');
				});
			$('body').on('click', function (e) {
			    if (!$('li.dropdown.mega-dropdown').is(e.target) 
			        && $('li.dropdown.mega-dropdown').has(e.target).length === 0 
			        && $('.open').has(e.target).length === 0
			    ) {
			        $('li.dropdown.mega-dropdown').removeClass('open');
			    }
			});		
			$('[name="start_day"]').change(function(){
				$('[name="end_day"]').val($(this).val());
				$('[name="check_day"]').val($(this).val());
			});
			$('[name="start_month"]').change(function(){
				$('[name="end_month"]').val($(this).val());
				$('[name="check_month"]').val($(this).val());
				if($(this).val()==1){
				$('[name="start_day"] [value="31"]').hide();
				$('[name="start_day"] [value="30"]').hide();
				}
				else if($(this).val()==0||$(this).val()==2||$(this).val()==4||$(this).val()==6||$(this).val()==7||$(this).val()==9||$(this).val()==11){
				$('[name="start_day"] [value="31"]').show();
				$('[name="start_day"] [value="30"]').show();
				}
				else{
				$('[name="start_day"] [value="31"]').hide();
				$('[name="start_day"] [value="30"]').show();	
				}
			});
			$('[name="end_month"]').change(function(){
				if($(this).val()==1){
				$('[name="end_day"] [value="31"]').hide();
				$('[name="end_day"] [value="30"]').hide();
				}
				else if($(this).val()==0||$(this).val()==2||$(this).val()==4||$(this).val()==6||$(this).val()==7||$(this).val()==9||$(this).val()==11){
				$('[name="end_day"] [value="31"]').show();
				$('[name="end_day"] [value="30"]').show();
				}
				else{
				$('[name="end_day"] [value="31"]').hide();
				$('[name="end_day"] [value="30"]').show();	
				}
			});
			$('[name="start_year"]').change(function(){
				$('[name="end_year"]').val($(this).val());
				$('[name="check_year"]').val($(this).val());
			});
			$('[name="start_hour"]').change(function(){
				$('[name="end_hour"]').val(parseInt($(this).val())+5);
				$('[name="check_hour"]').val(parseInt($(this).val())+2);
			});
			$('[name="start_minutes"]').change(function(){
				$('[name="end_minutes"]').val($(this).val());
				$('[name="check_minutes"]').val($(this).val());
			});
			$('[name="all_day"]').change(function(){
				if($('[name="all_day"]').prop('checked')){
				$('[name="start_hour"]').prop( "disabled", true );
				$('[name="start_minutes"]').prop( "disabled", true );
				$('[name="end_hour"]').prop( "disabled", true );
				$('[name="end_minutes"]').prop( "disabled", true );}
				else
				{$('[name="start_hour"]').prop( "disabled", false );
				$('[name="start_minutes"]').prop( "disabled", false );
				$('[name="end_hour"]').prop( "disabled", false );
				$('[name="end_minutes"]').prop( "disabled", false );}
			});
			$('[name="type"]').change(function(){
				if($(this).val()=='add')
				$('[name="other_type"]').show();
				else
				$('[name="other_type"]').hide();
			});
			$('#add_channel').click(function(){
				var l=$('tbody tr:last td.ch').text();
				l=parseInt(l)+1;
				$('tbody').append('<tr><td class="ch">'+l+'</td><td><input type="text" class="form-control"  name="ch'+l+'" placeholder="Podaj nazwę"></td></tr>');
				return false;
			});
			$('#calender').click(function(){
				$('.events_list ul').toggleClass('calender');
				$('.events_list ul').toggleClass('list');
				$('#list').removeClass('active');
				$(this).addClass('active');
			});
			$('#list').click(function(){
				$('.events_list ul').toggleClass('calender');
				$('.events_list ul').toggleClass('list');
				$('#calender').removeClass('active');
				$(this).addClass('active');
			});
			$('#for_who').change(function(){
				if($(this).val()=='one')
				$('#for_one').show();
				else
				$('#for_one').hide();
			});
			$('.pr').hover(function(){
				$(this).children('a').children('img.a').toggle();
				$(this).children('a').children('img.b').toggle();
			});
			$('#fileupload').fileupload({
		        url: '../file',
		        dataType: 'json',
		        done: function (e, data) {
		            $.each(data.result.files, function (index, file) {
		                $('#file_rider').val(file.name);
		            });
		        },
		        progressall: function (e, data) {
		            var progress = parseInt(data.loaded / data.total * 100, 10);
		            $('#progress .progress-bar').css(
		                'width',
		                progress + '%'
		            );
		        }
		    }).prop('disabled', !$.support.fileInput)
		        .parent().addClass($.support.fileInput ? undefined : 'disabled');

			$('[data-toggle="tooltip"]').tooltip();
			$('.gen_password_btn').click(function(){
				var id=$(this).prop('id');
				 var text = '';
    			 var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
				 for(var i=0; i < 8; i++)
			    {
			        text += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
				$('.gen_password.'+id).val(text);
				$('.pass.'+id).val(text);
				$('.gen_password.'+id).prop('disabled', false);;
				return false;
			})

		});
	function show_eq(data){
				var obj = data;
				var date = new Date();
				var d = date.getDate();
				var m = date.getMonth();
				var y = date.getFullYear();
				var time =new Date(y,m,d,1,0,0);
				for(i in obj){
					obj[i].info = obj[i].count;
					var status_act='';
					var temp =obj[i].count;
					if(obj[i].status!=''){
						for(k in obj[i].status){
							obj[i].status[k].termin=obj[i].status[k].start.split(' ')[0]+' - '+obj[i].status[k].end.split(' ')[0];
							var time_start=new Date(obj[i].status[k].start.split(' ')[0]+'T00:00:00Z');
							var time_end=new Date(obj[i].status[k].end.split(' ')[0]+'T00:00:00Z');
							if(time_start.getTime()<=time.getTime()&&time_end.getTime()>=time.getTime()){
								if((obj[i].status[k].pivot.check_before==1||obj[i].status[k].pivot.check_before==0)&&obj[i].status[k].pivot.check_after==1){
									//var status_act	= 'magazyn';
									if(obj[i].status[k].pivot.count_after!=null){
									obj[i].info = obj[i].info - (obj[i].status[k].pivot.count-obj[i].status[k].pivot.count_after);
									obj[i].status[k].c_after=obj[i].status[k].pivot.count-obj[i].status[k].pivot.count_after;
									}
									else{
									temp=obj[i].status[k].pivot.count;
									obj[i].status[k].c_after=obj[i].status[k].pivot.count;
									}
									
								}
								else if(obj[i].status[k].pivot.check_before==1){
									var status_act	= ' (zajęty i gotowy)';
									obj[i].info = obj[i].info - obj[i].status[k].pivot.count;
									obj[i].status[k].c_after=0;
								}
								else{
									var status_act	= ' (zajęty i niesprawdzony)';
									obj[i].info = obj[i].info - obj[i].status[k].pivot.count;
									obj[i].status[k].c_after=0;
								}
							}
							else{
									if((obj[i].status[k].pivot.check_before==1||obj[i].status[k].pivot.check_before==0)&&obj[i].status[k].pivot.check_after==1){
										//var status_act	= 'magazyn';
										
										if(obj[i].status[k].pivot.count_after!=null){
										obj[i].info = obj[i].info - (obj[i].status[k].pivot.count-obj[i].status[k].pivot.count_after);
										obj[i].status[k].c_after=obj[i].status[k].pivot.count-obj[i].status[k].pivot.count_after;
										}
										else
										obj[i].status[k].c_after=obj[i].status[k].pivot.count;;
										
									}
									else if(obj[i].status[k].pivot.check_before==1){
										//var status_act	= 'nie wrócił';
										obj[i].info= obj[i].info - obj[i].status[k].pivot.count;
										obj[i].status[k].c_after=0;
									}
									else{
										//var status_act	= 'magazyn';
										obj[i].info=obj[i].count;
										obj[i].status[k].c_after=obj[i].status[k].pivot.count;;
									}
								}
						}	
					}
				else{
				//var status_act = 'magazyn';
				obj[i].info=obj[i].count;
				}
			}
				return obj;

			}
		function show_eq_event(data,event,start,end){
				var obj = data;
				var time_start_g =new Date(start.substring(0,10)+'T'+start.substring(11,19));
				var time_end_g =new Date(end.substring(0,10)+'T'+end.substring(11,19));
				for(i in obj){
					obj[i].info = obj[i].count;
					var status_act='';
					var temp =obj[i].count;
					if(obj[i].status!=''){
						for(k in obj[i].status){
							if(obj[i].status[k].id==event){
								obj[i]=null;
								break;
							}
							obj[i].status[k].termin=obj[i].status[k].start.split(' ')[0]+' - '+obj[i].status[k].end.split(' ')[0];
							var time_start=new Date(obj[i].status[k].start.substring(0,10)+'T'+obj[i].status[k].start.substring(11,19));
							var time_end=new Date(obj[i].status[k].end.substring(0,10)+'T'+obj[i].status[k].end.substring(11,19));	
							if((time_start.getTime()<=time_start_g.getTime()&&time_end.getTime()>=time_end_g.getTime())||(time_start.getTime()>=time_start_g.getTime()&&time_end.getTime()<=time_end_g.getTime())){
								if((parseInt(obj[i].status[k].pivot.check_before)==1||parseInt(obj[i].status[k].pivot.check_before)==0)&&parseInt(obj[i].status[k].pivot.check_after)==1){
									//var status_act	= 'magazyn';
									if(obj[i].status[k].pivot.count_after!=null){
									obj[i].info = obj[i].info - (obj[i].status[k].pivot.count-obj[i].status[k].pivot.count_after);
									obj[i].status[k].c_after=obj[i].status[k].pivot.count-obj[i].status[k].pivot.count_after;
									}
									else{
									temp=obj[i].status[k].pivot.count;
									obj[i].status[k].c_after=obj[i].status[k].pivot.count;
									}
									
								}
								else if(parseInt(obj[i].status[k].pivot.check_before)==1){
									var status_act	= ' (zajęty i gotowy)';
									obj[i].info = obj[i].info - obj[i].status[k].pivot.count;
									obj[i].status[k].c_after=0;
								}
								else{
									var status_act	= ' (zajęty i niesprawdzony)';
									obj[i].info = obj[i].info - obj[i].status[k].pivot.count;
									obj[i].status[k].c_after=0;
								}
							}
							else{
									if((obj[i].status[k].pivot.check_before==1||obj[i].status[k].pivot.check_before==0)&&obj[i].status[k].pivot.check_after==1){
										//var status_act	= 'magazyn';
										
										if(obj[i].status[k].pivot.count_after!=null){
										obj[i].info = obj[i].info - (obj[i].status[k].pivot.count-obj[i].status[k].pivot.count_after);
										obj[i].status[k].c_after=obj[i].status[k].pivot.count-obj[i].status[k].pivot.count_after;
										}
										else
										obj[i].status[k].c_after=obj[i].status[k].pivot.count;;
										
									}
									else if(obj[i].status[k].pivot.check_before==1){
										//var status_act	= 'nie wrócił';
										obj[i].info= obj[i].info - obj[i].status[k].pivot.count;
										obj[i].status[k].c_after=0;
									}
									else{
										//var status_act	= 'magazyn';
										obj[i].info=obj[i].count;
										obj[i].status[k].c_after=obj[i].status[k].pivot.count;;
									}
								}
						}	
					}
				else{
				//var status_act = 'magazyn';
				obj[i].info=obj[i].count;
				}
			}
				return obj;

			}
	//change date to day
	function dat_to_day(dat){
		var date=[];
		date['all']=dat.split(' ')[0];
		var temp=date['all'].split('-');
		date['year']=temp[0];
		date['month']=temp[1];
		date['day']=temp[2];
		return date;
	}
	//user_to_events
	function user_to_events(data,id){
		for(i in data){
				data[i].status='Użytkownik dostępny';
				data[i].status_b='free';
			if(data[i].absence.length>0){
				data[i].status='Użytkownik nieobecny';
				data[i].status_b='absence';
			}
			if(data[i].events.length>0){
				data[i].status='Użytkownik jest dodany do innego wydarzenia';
				data[i].status_b='other';
				for(k in data[i].events){
					if(data[i].events[k].id==id){
						if(data[i].events[k].pivot.check==1||data[i].events[k].pivot.check==null){
							data[i]=null;
							break;
						}
						else{
							data[i].status='Użytkownik odmówił';
						data[i].status_b='odmowa';
						}
					}
				}
				
			}
		}
		return data;
	}
	//alerts
	function alerts(data){
		for(i in data.alerts){
				for(k in data.events){
					if(data.alerts[i].event_id==data.events[k].id){
						data.alerts[i].pot=data.events[k].pivot.check;
					}
				}
			var a = data.alerts[i].type.split(',');
				data.alerts[i].type=a[0];
				data.alerts[i].user_about=a[1];
		}
		return data;
	}
	var base=$('#global_link').val()+'/';
	//angular
	angular.module('app', ['ui.sortable'], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    })
    .directive('loading',   ['$http' ,function ($http)
    {
        return {
            restrict: 'A',
            link: function (scope, elm, attrs)
            {
                scope.isLoading = function () {
                    return $http.pendingRequests.length > 0;
                };

                scope.$watch(scope.isLoading, function (v)
                {
                    if(v){
                        elm.show();
                    }else{
                        elm.hide();
                    }
                });
            }
        };

    }])
    //alerts
    .controller('alerts', function($scope,$http,$timeout) {
    	//show
    	$scope.show_alert=function(){

  		$http.get( base+"alerts?alerts=all" ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
			$scope.alerts=alerts(response.data);
			if(response.data.count>0){
			$scope.counted='counted';
			$scope.a_act='act';
			}
			else{
			$scope.a_act='';
			$scope.counted='';
			}
			$timeout(function(){ $scope.show_alert();}, 120000);
        }, function errorCallback(response) {});
       		
      	 }
       $scope.show_alerts=function(){
       	 $scope.show_alert();
       	 $http.post( base+"alerts/showed" ,{alerts:$scope.alerts.alerts},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
        	$scope.a_act='';
        	$scope.counted='';
        	}, function errorCallback(response) {});
        		
       }
       $scope.show_alert();
       //del
       $scope.del_alert=function(id){
  		$http.delete( base+"alerts/"+id ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
			$scope.show_alert();
        }, function errorCallback(response) {});
       }
       //user ok (potwierdzenie udziału)
         $scope.user_ok=function(id){
  		$http.post( base+"events/"+id+"/users",{user:'ok'} ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
			$scope.show_alert();
        }, function errorCallback(response) {});
       }
       //user no (odrzucenie udziału)
         $scope.user_no=function(id){
  		$http.post( base+"events/"+id+"/users",{user:'no'} ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
			$scope.show_alert();
        }, function errorCallback(response) {});
       }
					
    })
     //alerts
    .controller('conf_by_user', function($scope,$http) {
       //user ok (potwierdzenie udziału)
         $scope.user_ok=function(id){
  		$http.post( base+"events/"+id+"/users",{user:'ok'} ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
			 $scope.info="Zaakceptowano udział w wydarzeniu";
			 setTimeout(location.reload(),4000);
        }, function errorCallback(response) {});
       }
       //user no (odrzucenie udziału)
         $scope.user_no=function(id){
  		$http.post( base+"events/"+id+"/users",{user:'no'} ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
			 $scope.info="Odrzucono udział w wydarzeniu";	
			 setTimeout(location.reload(),4000);
        }, function errorCallback(response) {});
       }
					
    })
    //user
    .controller('user', function($scope,$http) {
    	$http.get( base+"profile/show" ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
			$scope.user_activ=response.data.user;
        }, function errorCallback(response) {});
    })
    
    //equipment list controller
  	.controller('eq', function($scope,$http) {
  		$http.get( "./equipment?category=all" ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
			$scope.eq=show_eq(response.data);
   	   	    $scope.predicate = 'name';
    	    $scope.reverse = true;
      		$scope.order = function(predicate) {
        	$scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
       		$scope.predicate = predicate;
			angular.forEach($scope.eq, function (data) {
				data.info = parseFloat(data.info);
				data.count = parseFloat(data.count);
		   });	
         };
        }, function errorCallback(response) {});
					
    })
    //single information about equipment
    .controller('eq_single', function($scope,$http) {
    	var id=document.getElementById('id_hidden').value;
    	$scope.history_load=function(){
	  		$http.get( "./"+id+"?json=json" ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
				var dane=show_eq(response.data);
				$scope.info=dane[0].info;
				$scope.data=dane[0].status;
				 $scope.predicate = 'termin';
	    	    $scope.reverse = true;
	      		$scope.order = function(predicate) {
	        	$scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
	       		$scope.predicate = predicate;
	       	}
	        }, function errorCallback(response) {});
       }
       
       $scope.history_load();
       //show riders
        $scope.file_load=function(){
        	$http.post( "./"+id+"/files" ,{files:'all'},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.files=response.data;
					
		        },function errorCallback(response) { $scope.info='Wystąpił bład';});
        }
        //add riders
        $scope.type_file='manual';
       	$scope.add_file=function(name,type){
       		var url= document.getElementById('file_rider').value;
       		$http.post( "./"+id+"/files" ,{files:'add',name:name,type:type,file_url:url},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.info_out='Dodano plik';
					$scope.name_file='';
					$scope.file_load();
		        },function errorCallback(response) { $scope.info_out='Wystąpił bład';});
       	}
       	//del riders
       	$scope.del_file=function(id){
       		$http.post( "./"+id+"/files" ,{files:'del',id:id},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.info='Usunięto plik';
					$scope.file_load();
		        },function errorCallback(response) { $scope.info='Wystąpił bład';});
       	}
					
    })
    //event
    .controller('events', function($scope,$http) {
    		var id_global=document.getElementById('id_hidden').value;
    		var start_global=document.getElementById('start_hidden').value;
    		var end_global=document.getElementById('end_hidden').value;
    		$scope.predicate = 'name';
    	    $scope.reverse = true;
      		$scope.order = function(predicate) {
        	$scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
       		$scope.predicate = predicate;
       		}
       		$scope.type_rider='sound';
 
       //sprzet na wydarzeniu		
  		$scope.load_eq=function(){
  			
  			$http.get( "./"+id_global+"?equipment=equipment" ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
  			data=response.data;
  			for(i in data){
			data[i].count=parseInt(data[i].count);
			if(data[i].pivot!=null)
			data[i].pivot.count=parseInt(data[i].pivot.count);
			}
			$scope.data=data;
			
       	
        }, function errorCallback(response) {});
       }
       //all eguipment for add
       $scope.load_eq_all=function(){
  			$http.get( "../equipment?eq_event="+id_global ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
			$scope.eq=show_eq_event(response.data,id_global,start_global,end_global);
			$scope.count_add=1;
			angular.forEach($scope.eq, function (data) {
				if(data!=null){
				data.info = parseFloat(data.info);
				data.count = parseFloat(data.count);}
		   });	
       	
        }, function errorCallback(response) {});
       }
        //zmiany statusu sprzętu na wydarzeniu
		$scope.check_before=function(d,id,id_2){
			d=parseInt(d);
			if(id==null) id=id_2;
			$http.post( "./"+id_global+"/update_eq",{check_before:d,id:id},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
			$scope.info=response.data;
        }, function errorCallback(response) {});
		}
		$scope.check_after=function(d,id,id_2){
			d=parseInt(d);
			if(id==null) id=id_2;
			$http.post( "./"+id_global+"/update_eq",{check_after:d,id:id},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
			$scope.info=response.data;
        }, function errorCallback(response) {});
		}
		$scope.count_after=function(d,id,count,id_2){
			if(isNaN(d)==false&&d<=parseInt(count)){
				if(id==null) id=id_2;
			$http.post( "./"+id_global+"/update_eq",{count_after:d,id:id},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
			$scope.info=response.data;
        }, function errorCallback(response) {});
       }
       else
       $scope.info='Niepoprawne dane';
		}
		//add sprzet
		$scope.add_eq=function(d,id,count){
			if(isNaN(d)==false&&d<=parseInt(count)){
			$http.post( "./"+id_global+"/add_eq",{count:d,id:id},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
				$scope.info='Dodano sprzęt';
				//odświezenie listy
				$http.get( "../equipment?eq_event="+id_global ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
				$scope.eq=show_eq_event(response.data,id_global);
				$scope.count_add=1;
				}, function errorCallback(response) {$scope.info='Wystąpił bład';});
	        }, function errorCallback(response) {});
	       }
	       else
	       $scope.info='Niepoprawne dane';
		}
		//add sprzet out
		$scope.name_out='';
		$scope.count_out=1;
		$scope.add_eq_out=function(d,name){
			if(isNaN(d)==false&&name!=''){
			$http.post( "./"+id_global+"/add_eq_out",{count:d,name:name},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
				$scope.info_out='Dodano';
				 $scope.name_out='';
				 $scope.count_out=1;
	        }, function errorCallback(response) {});
	       }
	       else
	       $scope.info_out='Niepoprawne dane';
		}	
		//delete sprzet
		$scope.del_eq=function(id){
			$http.post( "./"+id_global+"/del_eq",{id:id},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
				$scope.info='Usunięto sprzęt';
				//odświezenie listy
				$http.get( "./"+id_global+"?equipment=equipment" ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.data=response.data;
			    });
	        }, function errorCallback(response) {$scope.info='Wystąpił bład';});
		}
		//edycja liczby sprzetu w wydarzeniu
		$scope.change_count=function(d,id,pivot_id){
			if(id==null){
				$http.post( "./"+id_global+"/update_eq",{pivot_id:pivot_id,count:d},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
						});
			}
			else{
			$http.get( "../equipment?eq_event="+id_global+"&eq_eq="+id ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
				var coun=show_eq_event(response.data,null);
				if(d<=coun[0].info){
					$http.post( "./"+id_global+"/update_eq",{pivot_id:pivot_id,count:d},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
						});	
				}
				else{
					 $scope.info='Niedostępna taka ilość sprzętu.';
				}
				
				}, function errorCallback(response) {});
		}
		}
		//ekipa for events
		$scope.load_ekipa=function(){
  			
  			$http.get( "./"+id_global+"?ekipa=ekipa" ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
  			data=response.data;
			$scope.ekipa=data;
        });
       }
       // add ekipa for events
		$scope.load_users_all=function(){
  			
  			$http.get( "./"+id_global+"?ekipa=all" ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
  			data=user_to_events(response.data,id_global);
			$scope.users=data;
        });
       }
       //add user for event
       $scope.add_user=function(id,status,func){
       		if(status=='absence'||status=='odmowa'){
       			$scope.info='Nie można dodać tego użytkownika';
       		}
       		else{
       			//var func= document.getElementById('function_user').value;
       			$http.post( "./"+id_global+"/users" ,{users:'add',id:id,func:func},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					if(status=='other'){
					$scope.info='Dodano użytkownika do wydarzenia, pomimo że użytkownik, w tym czasie, ma  już inne wydarzenie';
					}
					else{
					$scope.info='Dodano użytkownika do wydarzenia';	
					}
					$scope.load_users_all();
		        },function errorCallback(response) { $scope.info='Wystąpił bład';});
		       
       		}
       }
       //default function
       $scope.function_user="brak";
       //delete user form events
        $scope.del_user=function(id){
       			$http.post( "./"+id_global+"/users" ,{users:'del',id:id},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.info='Usunięto użytkownika';
					$scope.load_ekipa();
		        },function errorCallback(response) { $scope.info='Wystąpił bład';});
       }
       //add time work
       //add time work for single user all event time
        $scope.add_time_user_all=function(id){
       			$http.post( ".././timeworks",{event_id:id_global,user_id:id,timeworks:'add_all_one'},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.info='Dodano czas pracy';
					
		        },function errorCallback(response) { $scope.info='Wystąpił bład';});
       }
        //add time work for all user all event time
        $scope.add_time_all=function(){
       			$http.post( ".././timeworks",{event_id:id_global,timeworks:'add_all_all'},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.info='Dodano czas pracy';
					
		        },function errorCallback(response) { $scope.info='Wystąpił bład';});
       }
       //add time work for single user scecial time
        $scope.add_time_user_spec=function(){
        	var day=document.getElementById('oddo');
    		if(day.all_day.checked){
			var start=day.start_year.value+'-'+(parseInt(day.start_month.value)+1)+'-'+day.start_day.value+' 00:00:00';
			var end=day.end_year.value+'-'+(parseInt(day.end_month.value)+1)+'-'+day.end_day.value+' 23:59:59';
			}
			else{
			var start=day.start_year.value+'-'+(parseInt(day.start_month.value)+1)+'-'+day.start_day.value+' '+day.start_hour.value+':'+day.start_minutes.value+':00';
			var end=day.end_year.value+'-'+(parseInt(day.end_month.value)+1)+'-'+day.end_day.value+' '+day.end_hour.value+':'+day.end_minutes.value+':00';
			}
       			$http.post( ".././timeworks",{event_id:id_global,user_id:day.id.value,timeworks:'add_spec_one',start:start,end:end},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.info_add='Dodano czas pracy';
					
		        },function errorCallback(response) { $scope.info_add='Wystąpił bład';});
       }
       //show riders
        $scope.load_rider=function(){
        	$http.post( "./"+id_global+"/riders" ,{rider:'all'},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.riders=response.data;
					
		        },function errorCallback(response) { $scope.info='Wystąpił bład';});
        }
        //add riders
       	$scope.add_rider=function(name,type){
       		var url= document.getElementById('file_rider').value;
       		$http.post( "./"+id_global+"/riders" ,{rider:'add',name:name,type:type,url:url},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.info_out='Dodano rider do wydarzenia';
					$scope.name_rider='';
					$scope.load_rider();
		        },function errorCallback(response) { $scope.info_out='Wystąpił bład';});
       	}
       	//del riders
       	$scope.del_rider=function(id){
       		$http.post( "./"+id_global+"/riders" ,{rider:'del',id:id},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.info='Usunięto rider';
					$scope.load_rider();
		        },function errorCallback(response) { $scope.info='Wystąpił bład';});
       	}
       	//show inputlist
       	$scope.inputlist=function(id){
       		$http.get( base+"inputlist?event_id="+id_global,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.inputlists=response.data;
		        },function errorCallback(response) { $scope.info='Wystąpił bład';});
       	}
       	//delete inputlist
       	$scope.del_inputlist=function(id){
       		$http.delete( base+"inputlist/"+id,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.info='Usunięto inputlistę';
					$scope.inputlist();
		        },function errorCallback(response) { $scope.info='Wystąpił bład';});
       	}
       	//show alerts
       	$scope.alerts=function(){
       		$http.get( base+"alerts?event_id="+id_global,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					var data=response.data;
					for(i in data){
						if(data[i].for_who=='events_show')
							data[i].for_who='Wszyscy użytkownicy';
						data[i].times=data[i].times.substr(0,16);	
						if(data[i].for_who=='one')
							if(data[i].users.length>0)
								data[i].for_who=data[i].users[0].name;
							else
								data[i]=null;	
							
					}
					$scope.alert=data;
		        },function errorCallback(response) { $scope.info='Wystąpił bład';});
       	}
       	//add alerts
       	$scope.for_who='events_show';
       	$scope.for_one='2';
       	$scope.by_what='app';
       	$scope.time='now';
       	$scope.add_alert=function(for_who,for_one,time,by_what){
       		$http.post( base+"alerts/add",{event_id:id_global,for_who:for_who,for_one:for_one,time:time,by_what:by_what},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.info_out='Dodano alert';
					$scope.alerts();
		        },function errorCallback(response) { $scope.info_out='Wystąpił bład';});
       	}
       	//del alerts
       	$scope.del_alert=function(id){
       		$http.post( base+"alerts/"+id+"/del",{alerts:'alert_del'},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.info='Usunięto alert';
					$scope.alerts();
		        },function errorCallback(response) { $scope.info='Wystąpił bład';});
       	}
       	//show comments
       	$scope.comments_load=function(){
       		$http.post( base+"events/"+id_global+"/comments",{comments:'show'},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.comments=response.data;
		        });
       	}
       	 //add comments
       	$scope.comment_add=function(title,desc){
       		if(title==''||desc==''){
       			$scope.info='Prosimy podać tytuł i treść komentarza.';
       		}
       		else{
       		$http.post( base+"events/"+id_global+"/comments",{comments:'add',title:title,description:desc},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.info='Dodano komentarz';
					$scope.comments_load();
		        },function errorCallback(response) { $scope.info='Wystąpił bład';});
		    }    
       	}
       	 //del comments
       	$scope.comment_del=function(id){
       		$http.post( base+"events/"+id_global+"/comments",{comments:'del',id:id},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.info='Usunięto komentarz';
					$scope.comments_load();
		        },function errorCallback(response) { $scope.info='Wystąpił bład';});  
       	}
		//close alerst
		$scope.close=function(){
			$scope.info=null
		}
		$scope.close_out=function(){
			$scope.info_out=null
		}
		
		$scope.load_ekipa();			
    })
    //absence user
    .controller('absence', function($scope,$http) {
    	//show
    	$http.get( "?absence=absence" ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
    				var data=response.data;
    				for(i in data){
    					data[i].start=dat_to_day(data[i].start);
    					data[i].end=dat_to_day(data[i].end);
    				}
					$scope.absence=data;
			    });
		//add
		$scope.add=function(){
			var day=document.getElementById('oddo');
			var start=day.start_year.value+'-'+(parseInt(day.start_month.value)+1)+'-'+day.start_day.value+' 00:00:00';
			var end=day.end_year.value+'-'+(parseInt(day.end_month.value)+1)+'-'+day.end_day.value+' 23:59:59';
			$http.post( "./absence",{start:start,end:end} ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.info_add='Dodano';
					$http.get( "?absence=absence" ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
    				var data=response.data;
    				for(i in data){
    					data[i].start=dat_to_day(data[i].start);
    					data[i].end=dat_to_day(data[i].end);
    				}
					$scope.absence=data;
			  		});
			    });
		}	
		//delete
		$scope.del=function(id){
			$http.delete( "./absence/"+id,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
					$scope.info_add='Usunięto';
					$http.get( "?absence=absence" ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
    				var data=response.data;
    				for(i in data){
    					data[i].start=dat_to_day(data[i].start);
    					data[i].end=dat_to_day(data[i].end);
    				}
					$scope.absence=data;
			  		});
			    });
		}    
    })
    .controller('timework', function($scope,$http) {
    	//show single
    	$scope.load=function(){
    		$http.get( "?timework=single" ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
    				var data=response.data;
					$scope.timework=data;
			    });
		}	
		$scope.load()	    
    	//add without events
    	$scope.add=function(desc){
    		var day=document.getElementById('oddo');
    		var id=document.getElementById('user_id').value;
    		if(day.all_day.checked){
			var start=day.start_year.value+'-'+(parseInt(day.start_month.value)+1)+'-'+day.start_day.value+' 00:00:00';
			var end=day.end_year.value+'-'+(parseInt(day.end_month.value)+1)+'-'+day.end_day.value+' 23:59:59';
			}
			else{
			var start=day.start_year.value+'-'+(parseInt(day.start_month.value)+1)+'-'+day.start_day.value+' '+day.start_hour.value+':'+day.start_minutes.value+':00';
			var end=day.end_year.value+'-'+(parseInt(day.end_month.value)+1)+'-'+day.end_day.value+' '+day.end_hour.value+':'+day.end_minutes.value+':00';
			}
    		$http.put( base+"/timeworks/"+id,{timeworks:'add_one',start:start,end:end,description:desc} ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
    			$scope.info_out='Dodano';
				$scope.load();
		        },function errorCallback(response) { $scope.info_out='Wystąpił bład';});
		}
		$scope.close_out=function(){
			$scope.info_out=null
		}	  	    
    })
    .controller('timeworks', function($scope,$http) {
    	//show single
    	$scope.load=function(){$http.get( "?timeworks=show" ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
    				var data=response.data;
					$scope.timeworks=data;
			    });
			   }
		$scope.load();	   
		//del all
    	$scope.del_all=function(){
    		$http.put( "",{timeworks:'del_all'},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
    			$scope.load();
			    });
		}
		//del one
    	$scope.del=function(id){
    		$http.put( "",{timeworks:'del',id:id} ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
    			$scope.load();
			    });
		}
		//add without events
    	$scope.add=function(desc){
    		var day=document.getElementById('oddo');
    		if(day.all_day.checked){
			var start=day.start_year.value+'-'+(parseInt(day.start_month.value)+1)+'-'+day.start_day.value+' 00:00:00';
			var end=day.end_year.value+'-'+(parseInt(day.end_month.value)+1)+'-'+day.end_day.value+' 23:59:59';
			}
			else{
			var start=day.start_year.value+'-'+(parseInt(day.start_month.value)+1)+'-'+day.start_day.value+' '+day.start_hour.value+':'+day.start_minutes.value+':00';
			var end=day.end_year.value+'-'+(parseInt(day.end_month.value)+1)+'-'+day.end_day.value+' '+day.end_hour.value+':'+day.end_minutes.value+':00';
			}
    		$http.put( "",{timeworks:'add_one',start:start,end:end,description:desc} ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
    			$scope.info_out='Dodano';
				$scope.load();
		        },function errorCallback(response) { $scope.info_out='Wystąpił bład';});
		}
		$scope.close_out=function(){
			$scope.info_out=null
		}	  	    
    })
      .controller('inputlist', function($scope,$http) {
      	//close alerst
		$scope.close=function(){
			$scope.info=null
		}
		
    	//show
    	$scope.list=function(){
    		$http.get( "" ,{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {	
    			var data=response.data;
    			for(i in data){
    				var tmp=data[i];
    				var li = parseInt(i)+1;
    				data[i]={text:tmp,id:li}
    			}
				tmpchannels=data;
				$scope.channels = tmpchannels;

			  $scope.sortableOptions = {
			    stop: function(e, ui) {
					
			      var logEntry = tmpchannels.map(function(i){
			        return i.text;
			      }).join(';');
					$http.put( "" ,{sort:"sort",content:logEntry},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
						$scope.info=response.data;
						$scope.list();
					});
			    }
			  };
			    });
		  }
		 $scope.list(); 
		 $scope.add_channel=function(data){
		 		var l=data.length;
		 		var la=l-1;
		 		var id=data[la].id;
				id=parseInt(id)+1;
				data[l]={text:"",id:id}
				$scope.channels=data;
		 }
		 $scope.change=function(data){
		 	 var dat = data.map(function(i){
			        return i.text;
			      }).join(';');
		 	$http.put( "" ,{add:"add",content:dat},{headers:{'X-Requested-With': 'XMLHttpRequest'}}).then(function successCallback(response) {
						$scope.info=response.data;
						$scope.list();
					});
		 }
		});		    