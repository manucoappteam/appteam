<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\HistoryType;

class History extends Model
{
    protected $fillable = ['event_id','user_id','description','type_id'];
	
	public function event(){
		return $this->belongsTo('App\Event');
	}
	public function type(){
		return $this->belongsTo('App\HistoryType');
	}
	public function user(){
		return $this->belongsTo('App\User');
	}
	public static function add($type,$event=null,$desc=""){
		$type_id=null;
		$type_id=HistoryType::where('name',$type)->first()->id;
		$user_id=Auth::user()->id;
		return History::create(array('type_id'=>$type_id,'user_id'=>$user_id,'event_id'=>$event,'description'=>$desc));

	}
}
