<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;
		
    protected $fillable = ['title', 'description', 'event_id','user_id'];
	
	protected $dates = ['deleted_at'];
	
	 public function user()
    {
       return $this->belongsTo('App\User');
    }
}
