<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipmentFile extends Model
{
   	 use \Venturecraft\Revisionable\RevisionableTrait;
	 
	 protected $fillable = ['name', 'type','file_url','equipment_id'];
	 
	public function equipment(){
		return $this->belongsTo('App\Equipment');
	}
}
