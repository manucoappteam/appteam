<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timework extends Model
{
     protected $table = 'timeworks';
	 
	protected $fillable = ['start','end','description'];
	
	public function event()
    {
        return $this->belongsTo('App\Event');
	}
	public function user()
    {
        return $this->hasOne('App\User');
	}

}
