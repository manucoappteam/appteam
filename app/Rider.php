<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rider extends Model
{
     protected $table = 'riders';
	 
	protected $fillable = ['name', 'band', 'type','file_url'];
	
	
}
