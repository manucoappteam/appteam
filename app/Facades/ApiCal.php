<?php

namespace App\Facades;
use Illuminate\Support\Facades\Facade;

class ApiCal extends Facade{
    protected static function getFacadeAccessor() { return 'Api'; }
}	