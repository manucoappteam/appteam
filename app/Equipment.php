<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Equipment extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait;
	use SoftDeletes;
	
	protected $revisionEnabled = true;
    protected $revisionCleanup = true;
    protected $historyLimit = 500;
		
    protected $table = 'equipment';
	 
    protected $fillable = ['name', 'description', 'type','count','power','price'];
	
	public function status(){
		 return $this->belongsToMany('App\Event', 'equipment_event', 'equipment_id','event_id')->withPivot('count', 'check_before','check_after','count_after');  
	}
	
	public function files(){
		return $this->hasMany('App\EquipmentFile');
	}

}
