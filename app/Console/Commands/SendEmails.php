<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send e-mails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $alerts=\App\Alert::where('by_what','mail')->where('type','events')->where('times','<=',date('Y-m-d H:i:s'))->get();
			foreach($alerts as $alert){
				$ok=true;
				foreach($alert->users as $user){
					$mail=trim($user->email," ");
					//$this->info($mail);
					$dat['login']=$user->email;
					$dat['event']=$alert->event->name;
					$dat['id']=$alert->event->id;
					$dat['date']=substr($alert->event->start,0,16);
					$dat['form']=env('MAIL');
					$dat['nam']=env('NAME');
					$subject='Przypomnienie o wydarzeniu';
					$send=\Mail::send(['html' => 'mail.alert'], $dat, function ($message) use ($subject,$dat,$mail) {
	  			  	$message->from($dat['form'], $dat['nam']);
				  	$message->subject($subject);
	   			  	$message->to($mail);
					});
					if($send){
						 $alert->users()->detach($user->id);
						
					}
					else{
					   $ok=false;
					}
				}
				if($ok==true){
					$alert->delete();
				}
			}
    }
}
