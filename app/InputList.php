<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InputList extends Model
{
    protected $table = 'input_lists';
	 
    protected $fillable = ['id','event_id','content','name'];
	
	public function event()
    {
        return $this->belongsTo('App\Event');
    }
}
