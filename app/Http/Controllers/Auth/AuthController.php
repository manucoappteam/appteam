<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Roles;
use Validator;
use Gate;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\History;
use Mail;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {  
        //$this->middleware('guest', ['except' => 'getLogout']);
    }
	/*create view for register users*/
	//all user
	public function all(Request $request){
		if($request->isJson()||$request->ajax()){
			if (Gate::denies('add_events')) {
            abort(403);
        	}
			$data=User::where('id','<>',1)->select('name','id')->get()->toArray();
			return response()->json($data);
		}

	}
	//show profile
	public function show(Request $request){
		$data=Auth::user();
		if($request->isJson()||$request->ajax()){
			$role=Auth::user()->roles;
			return response()->json(['user'=>$data,'role'=>$role]);
		}
		return view('auth.profile.show')->with('data',$data);
	}
	//show edit
	public function updateProfile(Request $request)
    {
        if ($request->user()) {
        	$data=Auth::user();
            return view('auth.profile.edit')->with('data',$data);
        }	
		
    }

	//save edit
	  public function updateProfile_store(Request $request)
    {
        if ($request->user()) {
             $this->validate($request, [
           'name' => 'required|max:255',
            'mail' => 'email|max:255|unique:users,email',
            'phone'=>'integer',
            'new_password'=>'confirmed|min:6',
        ]);
		
		$user = Auth::user();
	    $user->name = $request->input('name');
		if(!empty($request->input('phone'))){
		$user->phone = $request->input('phone');
		}
		if(!empty($request->input('new_password'))){
		$user->password=bcrypt($request->input('new_password'));
		}
		if(!empty($request->input('mail'))){
		$user->email=$request->input('mail');
		}
		if ($request->hasFile('img')) {
			$fileName='img_'.$user->id.'.jpg';
			$destinationPath='img';
   			$request->file('img')->move($destinationPath, $fileName);
			$user->img=$fileName;
		}
 		$save=$user->save();
		History::add('user_edit');
		($save) ? $a='Zapisano zmiany' : $a='Wystąpił błąd';
        return redirect('/profile/show')->with('info',$a);
        }
		
    }
	public function getRegister_new(){
		 if (Gate::denies('users')) {
            abort(403);
        	}
		
			$roles= Roles::where('name','<>','root')->get();
			return view('auth.register')->with('roles',$roles);
	}
	public function ListProfile(){
		if (Gate::denies('users')) {
            abort(403);
        }
		$roles= Roles::where('name','<>','root')->get();
		$data=User::where('id','<>',1)->get();
		return view('auth.list')->with('data',$data)->with('roles',$roles);
	}
	public function ListProfile_edit($id,Request $request){
		if (Gate::denies('users')) {
            abort(403);
        }
		 if ($request->user()) {
             $this->validate($request, [
           'name' => 'required|max:255',
            'mail' => 'email|max:255|unique:users,email',
            'new_password'=>'confirmed|min:6',
        ]);
		 }
		$user=User::find($id);
		$user->name=$request->input('name');
		$user->type=$request->input('type');
		$user->role=$request->input('role');
		if(!empty($request->input('new_password'))){
		$user->password=bcrypt($request->input('new_password'));
		}
		if(!empty($request->input('mail'))){
		$user->email=$request->input('mail');
		}
		 $save = $user->save();
		 History::add('user_edit',null,$user->name);
		($save) ? $a='Zapisano zmiany' : $a='Wystąpił błąd';
		if($save){
		if($request->has('send_mail')&&$request->input('send_mail')==true){
			$data['pass']=$request->input('new_password');
			$data['login']=$user->email;
			$data['sys']=env('NAME');
			$subject='Zmieniono dane logowania';
			$send=Mail::send(['html' => 'mail.reset'], $data, function ($message) use ($subject,$data) {
  			  $message->from(env('MAIL'), env('NAME'));
			  $message->subject($subject);
   			  $message->to($data['login']);
			});
			($send) ? $a.=' i wysłano wiadomość' : $a.=' i nie udało się wysłać wiadomości';
		}
		}
		return redirect('/users')->with('info',$a);
	}
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'role' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public function create(Request $request)
    {
    	 if (Gate::denies('users')) {
            abort(403);
        }
		  $this->validate($request, [
		 	'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'role' => 'required',
            ]);
		 $user=new User;
		 $user->name=$request->input('name');
		 $user->email=$request->input('email');
		  $user->password=bcrypt($request->input('password'));
		   $user->role=$request->input('role');
		    $user->type=$request->input('type');
        $save=$user->save();
		History::add('user_add',null,$user->name);
		($save) ? $a='Utworzono poprawnie' : $a='Wystąpił błąd';
		//sending mail
		if($save){
		if($request->has('send_mail')&&$request->input('send_mail')==true){
			$data['pass']=$request->input('password');
			$data['login']=$request->input('email');
			$data['sys']=env('NAME');
			$subject='Dodano użytownika';
			$send=Mail::send(['html' => 'mail.create'], $data, function ($message) use ($subject,$data) {
  			  $message->from(env('MAIL'), env('NAME'));
			  $message->subject($subject);
   			  $message->to($data['login']);
			});
			($send) ? $a.=' i wysłano wiadomość' : $a.=' i nie udało się wysłać wiadomości';
		}
		}
		
        return redirect('/users')->with('info',$a);
    }
	public function delete($id){
		if (Gate::denies('users_del')) {
            abort(403);
        }
		$user=User::find($id);
		$save=$user->delete();
		($save) ? $a='Usunięto użytkownika' : $a='Wystąpił błąd';
		 return redirect('/users')->with('info',$a);
	}
}
