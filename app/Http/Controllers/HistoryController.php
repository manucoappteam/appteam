<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gate;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\History;

class HistoryController extends Controller
{
	
 	public function __construct()
    {
       if (Gate::denies('history')) {
            abort(403);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$history = History::orderBy('created_at','desc')->simplePaginate(40);
        return view('history.index')->with('history',$history);
    }

}
