<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alert;
use App\User;
use Gate;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Event;
use Mail;

class AlertsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    		
    	if($request->input('alerts')=='all'&&($request->isJson()||$request->ajax())){
        $alerts=Auth::user()->alerts()->where('times','<=',date('Y-m-d H:i:s'))->where('by_what','=','app')->orderBy('created_at','desc')->with('event')->get()->toArray();	
		$event=Auth::user()->events()->get()->toArray();
		$count=Auth::user()->alerts()->where('times','<=',date('Y-m-d H:i:s'))->where('by_what','=','app')->where('alert_user.chcek','=',0)->count();
		$data=array('events'=>$event,'alerts'=>$alerts,'count'=>$count);
      	return  response()->json($data);
		}
		if($request->has('event_id')&&($request->isJson()||$request->ajax())){
			$data=Alert::where('event_id',$request->input('event_id'))->where('type','events')->with('users')->get();
			return  response()->json($data);
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
	public function showed(Request $request){
		if($request->has('alerts')&&($request->isJson()||$request->ajax())){
			foreach($request->input('alerts') as $a){
			$alert=Auth::user()->alerts()->find($a['id']);
			$alert->pivot->chcek=1;
			$alert->pivot->save();
			//return $alert;
			}
		//return Auth::user()->alerts()->find($a['id']);
		}
	}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function store($id_event,$for_who,$by_what,$tim,$type)
    {
    	
        $alert= new Alert;
		$alert->event_id=$id_event;
		if($for_who!='all'&&$for_who!='user_on_event'&&$for_who!='users_to_event'&&$for_who!='riders_show'&&$for_who!='events_show'&&$for_who!='inputlist_show'&&$for_who!='comment'){
			$alert->for_who='one';
		}
		else{
			$alert->for_who=$for_who;
		}
		$alert->by_what=$by_what;
		//czas wyswietlenia alertu
		if($tim=='now'){
		$now=time()+15;
		$alert->times=date('Y-m-d H:i:s',$now);
		}
		else if($tim=='one_day'){
			$event=Event::find($id_event)->start;
			$times=new \DateTime($event);
			$times->modify('-1 day');
			$alert->times=$times->format('Y-m-d H:i:s');
		}
		else if($tim=='one_week'){
			$event=Event::find($id_event)->start;
			$times=new \DateTime($event);
			$times->modify('-1 week');
			$alert->times=$times->format('Y-m-d H:i:s');
		}
		else if($tim=='two_week'){
			$event=Event::find($id_event)->start;
			$times=new \DateTime($event);
			$times->modify('-2 week');
			$alert->times=$times->format('Y-m-d H:i:s');
		}
		$alert->type=$type;
		$alert->save();
		//dodanie urzytkowników do alertu
		if($for_who!='all'&&$for_who!='user_on_event'&&$for_who!='users_to_event'&&$for_who!='riders_show'&&$for_who!='events_show'&&$for_who!='inputlist_show'&&$for_who!='comment'){
			$alert->users()->attach($for_who);
		}
		else if($for_who=='users_to_event'){
			$users=User::all();
			foreach($users as $user){
				if (Gate::forUser($user)->allows('users_to_event')) {
					
				    $alert->users()->attach($user->id);
				}
			}
		}
		else if($for_who=='all'){
			$users=User::all();
			foreach($users as $user){
				    $alert->users()->attach($user->id);
			}
		}
		else if($for_who=='user_on_event'){
			$users=User::all();
			foreach($users as $user){
				if (Gate::forUser($user)->allows('show_events',$event_id)) {
				    $alert->users()->attach($user->id);
				}
			}
		}
		else if($for_who=='riders_show'){
			$users=User::all();
			foreach($users as $user){
				if (Gate::forUser($user)->allows('show_events',$id_event)&&Gate::forUser($user)->allows('riders_show')) {
				    $alert->users()->attach($user->id);
				}
			}
		}
		else if($for_who=='events_show'){
			$users=User::all();
			foreach($users as $user){
				if (Gate::forUser($user)->allows('show_events',$id_event)) {
				    $alert->users()->attach($user->id);
				}
			}
		}
		else if($for_who=='inputlist_show'){
			$users=User::all();
			foreach($users as $user){
				if (Gate::forUser($user)->allows('show_events',$id_event)&&Gate::forUser($user)->allows('inputlist_show')) {
				    $alert->users()->attach($user->id);
				}
			}
		}
		else if($for_who=='comment'){
			$users=User::all();
			foreach($users as $user){
				if (Gate::forUser($user)->allows('show_events',$id_event)&&Gate::forUser($user)->allows('comments')) {
				    $alert->users()->attach($user->id);
				}
			}
		}
		//wysłanie maila	
    	if($by_what=='mail'&&$tim=='now'){
    		if($type=='events'){
    			foreach($alert->users as $user){
					$dat['login']=$user->email;
					$dat['event']=$alert->event->name;
					$dat['id']=$alert->event->id;
					$dat['date']=substr($alert->event->start,0,16);
					$subject='Przypomnienie o wydarzeniu';
					$send=Mail::send(['html' => 'mail.alert'], $dat, function ($message) use ($subject,$dat) {
	  			  	$message->from(env('MAIL'), env('NAME'));
				  	$message->subject($subject);
	   			  	$message->to($dat['login']);
					});
					if($send){
						 $alert->users()->detach($user->id);
					}
				}
    		}
			if($type=='add_to_event'){
    			foreach($alert->users as $user){
					$dat['login']=$user->email;
					$dat['event']=$alert->event->name;
					$dat['id']=$alert->event->id;
					$dat['date']=substr($alert->event->start,0,16);
					$subject='Dodano do wydarzenia';
					$send=Mail::send(['html' => 'mail.add'], $dat, function ($message) use ($subject,$dat) {
	  			  	$message->from(env('MAIL'), env('NAME'));
				  	$message->subject($subject);
	   			  	$message->to($dat['login']);
					});
					if($send){
						 $alert->users()->detach($user->id);
					}
				}
    		}	
    		
    	}
		return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
    	if($request->input('for_who')=='one'){
    		$for_who=$request->input('for_one');
    	}
		else{
			$for_who=$request->input('for_who');
		}
        if(self::store($request->input('event_id'),$for_who,$request->input('by_what'),$request->input('time'),'events')){
        	return 1;
        }
		else 
			return 0;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
       	
       $alert=Alert::find($id);
	   $user=Auth::user()->id;
	   $alert->users()->detach($user);
	   
    }
	 public function del($id,Request $request)
    {
       if($request->input('alerts')=='alert_del'&&($request->isJson()||$request->ajax())){
       	  $alert=Alert::find($id);
		  $user=Auth::user()->id;
	  	  $alert->users()->detach();
		  $alert->delete();
		  return 1;
       }	
  
    }
}
