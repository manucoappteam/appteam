<?php

namespace App\Http\Controllers;
use App\User;
use App\Roles;
use App\Event;
use Auth;
use Gate;
use Illuminate\Http\Request;
class Gc extends Controller
{
    public function login(){
   		$api=\Api::url();
		return redirect(filter_var($api, FILTER_SANITIZE_URL));
	}
	public function auth(Request $request){
		$api=\Api::auth($_GET['code']);
		$authObj = json_decode($api);
		\Session::put('api_google', $api);
		if(isset($authObj->refresh_token)) {
		     $a=$authObj->refresh_token;
		 }
		else{
			return redirect('profile/show')->with('info','Wystąpił błąd');
		}
		Auth::user()->api()->create(array('key'=>$a));
		return redirect('profile/show')->with('info','Pomyślnie połączono z google');
	}
	public static function save($event,$for_who,$all_day=null){
		if($for_who=='events_show'){
			$users=User::all();
			foreach($users as $user){
				if (Gate::forUser($user)->allows('show_events',$event->id)) {
				    if($user->api){							
				    	 if($all_day==1){
				    	 	// $objDateTime_start = new \DateTime(substr($event->start,0,9));
							// $isoDate_start = $objDateTime_start->format(\DateTime::ISO8601);
						 	// $objDateTime_end = new \DateTime(substr($event->end,0,9));
							// $isoDate_end = $objDateTime_start->format(\DateTime::ISO8601);
							 $start=array( 'date' =>  substr($event->start,0,9),'timeZone' => 'Europe/Warsaw');
							 $end=array( 'date' =>  substr($event->end,0,9),'timeZone' => 'Europe/Warsaw');
						 }
						 else{
						 	 $objDateTime_start = new \DateTime($event->start);
							 $isoDate_start = $objDateTime_start->format(\DateTime::ISO8601);
						 	 $objDateTime_end = new \DateTime($event->end);
							 $isoDate_end = $objDateTime_end->format(\DateTime::ISO8601);
						 	 $start=array( 'dateTime' =>  $isoDate_start,'timeZone' => 'Europe/Warsaw');
							 $end=array( 'dateTime' =>  $isoDate_end,'timeZone' => 'Europe/Warsaw');
						 }
						 if($event->confirmation==1){
						 	$event->name=$event->name.' - potwierdzone';
						 }
						 $calendarId = 'primary';
						 $event_google = new \Google_Service_Calendar_Event(array(
						  'summary' =>  $event->name,
						  'location' => $event->localization,
						  'description' => $event->description,
						  'start' => $start,
						  'end' => $end,
						  'reminders' => array(
						    'useDefault' => FALSE,
						  ),
						));
						$api=\Api::start($user->api->key);
						$event_google = $api->events->insert($calendarId, $event_google);
						$user->apiid()->create(array('event_id'=>$event->id,'google_id'=>$event_google->id));
					}
				}
			}
		}	
	}
	public static function update($event,$for_who,$all_day=null){
		if($for_who=='events_show'){
			$users=User::all();
			foreach($users as $user){
				if (Gate::forUser($user)->allows('show_events',$event->id)) {
				    if($user->api){
				    	$api=\Api::start($user->api->key);
						$event_id=$user->apiid()->where('event_id',$event->id)->first();
						if($event_id && $api->events->get('primary', $event_id->google_id)){								
				    	 if($all_day==1){
							 $start=array( 'date' =>  substr($event->start,0,9),'timeZone' => 'Europe/Warsaw');
							 $end=array( 'date' =>  substr($event->end,0,9),'timeZone' => 'Europe/Warsaw');
						 }
						 else{
						 	 $objDateTime_start = new \DateTime($event->start);
							 $isoDate_start = $objDateTime_start->format(\DateTime::ISO8601);
						 	 $objDateTime_end = new \DateTime($event->end);
							 $isoDate_end = $objDateTime_end->format(\DateTime::ISO8601);
						 	 $start=array( 'dateTime' =>  $isoDate_start,'timeZone' => 'Europe/Warsaw');
							 $end=array( 'dateTime' =>  $isoDate_end,'timeZone' => 'Europe/Warsaw');
						 }
						 if($event->confirmation==1){
						 	$event->name=$event->name.' - potwierdzone';
						 }
						 $calendarId = 'primary';
						 $event_google = new \Google_Service_Calendar_Event(array(
						  'summary' =>  $event->name,
						  'location' => $event->localization,
						  'description' => $event->description,
						  'start' => $start,
						  'end' => $end,
						  'reminders' => array(
						    'useDefault' => FALSE,
						  ),
						));
						$event_google = $api->events->update($calendarId, $event_id->google_id,$event_google);
						}
					   else{
						Gc::save($event,$for_who,$all_day);
					   }
					}
				}
			}
		}	
	}
	public static function delete($event){
			$users=User::all();
			foreach($users as $user){
				    if($user->api){
				    	$api=\Api::start($user->api->key);
						$event_id=$user->apiid()->where('event_id',$event->id)->first();
						 $calendarId = 'primary';
						if($event_id && $api->events->get('primary', $event_id->google_id)){								
						$event_google = $api->events->delete($calendarId, $event_id->google_id);
						}
					}
				}	
	}
}
