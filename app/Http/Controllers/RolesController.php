<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gate;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Roles;
use App\History;

class RolesController extends Controller
{
	
 	public function __construct()
    {
       if (Gate::denies('roles')) {
            abort(403);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$roles = Roles::where('id','<>', 1)->get();;
        return view('roles.index')->with('roles',$roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('roles.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255|unique:roles,name',
            'description' => 'required|max:255',
            'users' => 'boolean',
            'add_events' => 'boolean',
            'show_events' => 'boolean',
            'add_equipment' => 'boolean',
            'edit_equipment' => 'boolean',
            'delete_equipment' => 'boolean',
            'show_equipment' => 'boolean',
            'equipment_to_event' => 'boolean',
            'show_equipment_to_event' => 'boolean',
            'check_equipment_to_event' => 'boolean',
            'absence' => 'boolean',
            'users_to_event' => 'boolean',
            'check_users_on_event' => 'boolean',
            'riders' => 'boolean',
            'riders_show' => 'boolean',
            'timework' => 'boolean',
            'timeworks' => 'boolean',
            'inputlist' => 'boolean',
            'inputlist_show' => 'boolean',
            'history' => 'boolean',
            'timework_self' => 'boolean',
            'comments' => 'boolean',
            'want_event' => 'boolean'
        ]);
		
		$role = Roles::firstOrNew(['name'=>$request->input('name')]);
	    $role->name = $request->input('name');
		 $role->description = $request->input('description');
 		 (!empty($request->input('users'))) ? $role->users = $request->input('users') : $role->users = 0;
		 (!empty($request->input('add_events'))) ? $role->add_events = $request->input('add_events') : $role->add_events = 0;
		 (!empty($request->input('show_events'))) ? $role->show_events = $request->input('show_events') : $role->show_events = 0;
		 (!empty($request->input('add_equipment'))) ? $role->add_equipment = $request->input('add_equipment') : $role->add_equipment = 0;
		 (!empty($request->input('edit_equipment'))) ? $role->edit_equipment = $request->input('edit_equipment') : $role->edit_equipment = 0;
		 (!empty($request->input('delete_equipment'))) ? $role->delete_equipment = $request->input('delete_equipment') : $role->delete_equipment = 0;
		 (!empty($request->input('show_equipment'))) ? $role->show_equipment = $request->input('show_equipment') : $role->show_equipment = 0;
		 (!empty($request->input('equipment_to_event'))) ? $role->equipment_to_event = $request->input('equipment_to_event') : $role->equipment_to_event = 0;
		 (!empty($request->input('show_equipment_to_event'))) ? $role->show_equipment_to_event = $request->input('show_equipment_to_event') : $role->show_equipment_to_event = 0;
		 (!empty($request->input('check_equipment_to_event'))) ? $role->check_equipment_to_event = $request->input('check_equipment_to_event') : $role->check_equipment_to_event = 0;
		 (!empty($request->input('absence'))) ? $role->absence = $request->input('absence') : $role->absence = 0;
		 (!empty($request->input('users_to_event'))) ? $role->users_to_event = $request->input('users_to_event') : $role->users_to_event = 0;
		 (!empty($request->input('check_users_on_event'))) ? $role->check_users_on_event = $request->input('check_users_on_event') : $role->check_users_on_event = 0;
		 (!empty($request->input('riders'))) ? $role->riders = $request->input('riders') : $role->riders = 0;
		 (!empty($request->input('riders_show'))) ? $role->riders_show = $request->input('riders_show') : $role->riders_show = 0;
		 (!empty($request->input('timework'))) ? $role->timework = $request->input('timework') : $role->timework = 0;
		 (!empty($request->input('timeworks'))) ? $role->timeworks = $request->input('timeworks') : $role->timeworks = 0;
		 (!empty($request->input('inputlist'))) ? $role->inputlist = $request->input('inputlist') : $role->inputlist = 0;
		 (!empty($request->input('inputlist_show'))) ? $role->inputlist_show = $request->input('inputlist_show') : $role->inputlist_show = 0;
		 (!empty($request->input('history'))) ? $role->history = $request->input('history') : $role->history = 0;
		 (!empty($request->input('timework_self'))) ? $role->timework_self = $request->input('timework_self') : $role->timework_self = 0;
		 (!empty($request->input('comments'))) ? $role->comments = $request->input('comments') : $role->comments = 0;
		 (!empty($request->input('want_event'))) ? $role->want_event = $request->input('want_event') : $role->want_event = 0;
		$save = $role->save();
		History::add('role_add',null,$role->name);
		($save) ? $a='Utworzono poprawnie' : $a='Wystąpił błąd';
        return redirect('/roles')->with('info',$a);
		
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	if($id==1){
    		 abort(403);
    	}
		else{
        $roles = Roles::where('id','=', $id)->first();
        return view('roles.edit')->with('roles',$roles);
		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $this->validate($request, [
            'name' => 'required|max:255|unique:roles,name,'.$id,
            'description' => 'required|max:255',
            'users' => 'boolean',
            'add_events' => 'boolean',
            'show_events' => 'boolean',
            'add_equipment' => 'boolean',
            'edit_equipment' => 'boolean',
            'delete_equipment' => 'boolean',
            'show_equipment' => 'boolean',
            'equipment_to_event' => 'boolean',
            'show_equipment_to_event' => 'boolean',
            'check_equipment_to_event' => 'boolean',
            'absence' => 'boolean',
            'users_to_event' => 'boolean',
            'check_users_on_event' => 'boolean',
            'riders' => 'boolean',
            'riders_show' => 'boolean',
            'timework' => 'boolean',
            'timeworks' => 'boolean',
            'inputlist' => 'boolean',
            'inputlist_show' => 'boolean',
            'history' => 'boolean',
            'timework_self' => 'boolean',
            'comments' => 'boolean',
            'want_event' => 'boolean'
        ]);
			$role = Roles::find($id);
	    $role->name = $request->input('name');
		 $role->description = $request->input('description');
 			 (!empty($request->input('users'))) ? $role->users = $request->input('users') : $role->users = 0;
		 (!empty($request->input('add_events'))) ? $role->add_events = $request->input('add_events') : $role->add_events = 0;
		 (!empty($request->input('show_events'))) ? $role->show_events = $request->input('show_events') : $role->show_events = 0;
		 (!empty($request->input('add_equipment'))) ? $role->add_equipment = $request->input('add_equipment') : $role->add_equipment = 0;
		 (!empty($request->input('edit_equipment'))) ? $role->edit_equipment = $request->input('edit_equipment') : $role->edit_equipment = 0;
		 (!empty($request->input('delete_equipment'))) ? $role->delete_equipment = $request->input('delete_equipment') : $role->delete_equipment = 0;
		 (!empty($request->input('show_equipment'))) ? $role->show_equipment = $request->input('show_equipment') : $role->show_equipment = 0;
		 (!empty($request->input('equipment_to_event'))) ? $role->equipment_to_event = $request->input('equipment_to_event') : $role->equipment_to_event = 0;
		 (!empty($request->input('show_equipment_to_event'))) ? $role->show_equipment_to_event = $request->input('show_equipment_to_event') : $role->show_equipment_to_event = 0;
		 (!empty($request->input('check_equipment_to_event'))) ? $role->check_equipment_to_event = $request->input('check_equipment_to_event') : $role->check_equipment_to_event = 0;
		 (!empty($request->input('absence'))) ? $role->absence = $request->input('absence') : $role->absence = 0;
		 (!empty($request->input('users_to_event'))) ? $role->users_to_event = $request->input('users_to_event') : $role->users_to_event = 0;
		 (!empty($request->input('check_users_on_event'))) ? $role->check_users_on_event = $request->input('check_users_on_event') : $role->check_users_on_event = 0;
		 (!empty($request->input('riders'))) ? $role->riders = $request->input('riders') : $role->riders = 0;
		 (!empty($request->input('riders_show'))) ? $role->riders_show = $request->input('riders_show') : $role->riders_show = 0;
		 (!empty($request->input('timework'))) ? $role->timework = $request->input('timework') : $role->timework = 0;
		 (!empty($request->input('timeworks'))) ? $role->timeworks = $request->input('timeworks') : $role->timeworks = 0;
		 (!empty($request->input('inputlist'))) ? $role->inputlist = $request->input('inputlist') : $role->inputlist = 0;
		 (!empty($request->input('inputlist_show'))) ? $role->inputlist_show = $request->input('inputlist_show') : $role->inputlist_show = 0;
		 (!empty($request->input('history'))) ? $role->history = $request->input('history') : $role->history = 0;
		 (!empty($request->input('timework_self'))) ? $role->timework_self = $request->input('timework_self') : $role->timework_self = 0;
		 (!empty($request->input('comments'))) ? $role->comments = $request->input('comments') : $role->comments = 0;
		 (!empty($request->input('want_event'))) ? $role->want_event = $request->input('want_event') : $role->want_event = 0;
		$save = $role->save();
		History::add('role_edit',null,$role->name);
		($save) ? $a='Zapisano zmiany' : $a='Nie zapisano zmian';
        return redirect('/roles')->with('info',$a);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	if($id==1){
    		 abort(403);
    	}
        $role = Roles::find($id);
		$name=$role->name;
 		$del=$role->delete();
		History::add('role_del',null,$name);
		($del) ? $a='Usunięto role' : $a='Wystąpił błąd';
        return redirect('/roles')->with('info',$a);
    }
}
