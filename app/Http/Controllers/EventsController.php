<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Add_equipment;
use Gate;
use Auth;
use App\Equipment;
use App\User;
use App\Rider;
use App\History;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use PDF;


class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Event::all();
		return view('event.index')->with('data',$data);
    }
	 public function index_list(Request $request,$year=null,$month=null)
    {
    	
		
		if(empty($year)||empty($month)){
			$year=date("Y");
			$month=date("n");
		}
		
		($month<10) ? $month='0'.$month : $month;		
		
		$data=Event::where('start','like',"$year-$month%")->get();
		$data=$data->groupBy(function ($item, $key) {
    		return date("j",strtotime($item['start']));
		})->sortBy('start')->toArray();
		if(($request->isJson()||$request->ajax())){
			return response()->json($data);
		}
		return view('event.index')->with('data',$data)->with('year',$year)->with('month',$month);
    }
	 
	 public function index_api(Request $request,$year=null,$month=null)
    {
    	if(empty($year)||empty($month)){
			$year=date("Y");
			$month=date("n");
		}
		
		($month<10) ? $month='0'.$month : $month;	
		if (Gate::denies('show_events')) {
			$user=Auth::user()->id;
            $data=Event::where('start','like',"$year-$month%")->whereHas('users', function ($query) use ($user){
    			$query->where('user_id',$user);
				})->get();
			$data=$data->groupBy(function ($item, $key) {
	    		return date("j",strtotime($item['start']));
			})->sortBy('start')->toJson();
			if(($request->isJson()||$request->ajax())){
				return $data;
			}
        }
		else{
					
			$data=Event::where('start','like',"$year-$month%")->get();
			$data=$data->groupBy(function ($item, $key) {
	    		return date("j",strtotime($item['start']));
			})->sortBy('start')->toJson();
			if(($request->isJson()||$request->ajax())){
				return $data;
			}
		}
		
		//return view('event.index')->with('data',$data)->with('year',$year)->with('month',$month);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (Gate::denies('add_events')) {
            abort(403);
        }
		empty($request->input('year')) ? $data['year']=date("Y") : $data['year']=$request->input('year');
		empty($request->input('month')) ? $data['month']=date("m") : $data['month']=$request->input('month');
		empty($request->input('day')) ? $data['day']=date("d"): $data['day']=$request->input('day');
		$data['users']=User::where('id','<>',1)->get();

		return view('event.add')->with('data',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       if (Gate::denies('add_events')) {
            abort(403);
        }
	   $this->validate($request, [
            'name' => 'required|max:255|',
           
           
        ]);
		$event = new Event();
	    $event->name = $request->input('name');
		if($request->has('description'))
			 	$event->description = $request->input('description');
		else
				$event->description='';
		if($request->has('localization'))
				$event->localization = $request->input('localization');
		else
				$event->localization = '';
		$event->type = $request->input('type');
		$event->user_id=Auth::user()->id;
		($request->input('confirmation')==1) ? $event->confirmation=$request->input('confirmation') : $event->confirmation=0;
		if($request->input('all_day')==1){
		$event->start=$request->input('start_year').'-'.($request->input('start_month')+1).'-'.$request->input('start_day').' 00:00:00';
		$event->end=$request->input('end_year').'-'.($request->input('end_month')+1).'-'.$request->input('end_day').' 23:59:59';
	    }
		else{
 		$event->start=$request->input('start_year').'-'.($request->input('start_month')+1).'-'.$request->input('start_day').' '.$request->input('start_hour').':'.$request->input('start_minutes').':00';
		$event->end=$request->input('end_year').'-'.($request->input('end_month')+1).'-'.$request->input('end_day').' '.$request->input('end_hour').':'.$request->input('end_minutes').':00';
		}
		$event->check=$request->input('check_year').'-'.($request->input('check_month')+1).'-'.$request->input('check_day').' '.$request->input('check_hour').':'.$request->input('check_minutes').':00';
		
		$save = $event->save();
		if($save){
			AlertsController::store($event->id,'events_show','app','now','add_event');
			History::add('event_add',$event->id);
			Gc::save($event,'events_show',$request->input('all_day'));
			if(($request->isJson()||$request->ajax())){
				return response()->json(array('code'=>true));
			}
		}
		($save) ? $a='Utworzono poprawnie' : $a='Wystąpił błąd';
        return redirect('/events_list')->with('info',$a);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
    	if (Gate::denies('show_events',$id)) {
           abort(403);
        	}
    	//uprawnienia do wyświetlania eventów
    	if($request->input('equipment')=='equipment'&&($request->isJson()||$request->ajax())){
    		 if (Gate::denies('show_equipment_to_event',$id)) {
           return false;
        	}
    		$data1=Event::find($id)->equipment()->get()->toArray();
			$data2=Event::find($id)->add_equipment()->where('equipment_id',null)->get()->toArray();
    		$data=array_merge($data1,$data2);
			
    		return response()->json($data);
   
    	}
		//wyswietlenie ekipy 
		if($request->input('ekipa')=='ekipa'&&($request->isJson()||$request->ajax())){
    		 if (Gate::denies('show_events',$id)) {
           return false;
        	}
    		$data=Event::find($id)->users()->where(function($query){$query->where('check','=',1)->orwhere('check','=',null);})->get()->toJson();
			
    		return $data;
   
    	}
		//wyswietlenie uzytkowników do dodania do eventu
		if($request->input('ekipa')=='all'&&($request->isJson()||$request->ajax())){
    		 if (Gate::denies('users_to_event')) {
           return false;
        	}
		    $dat = Event::find($id)->end;
			$dat2 = Event::find($id)->start;
    		$data=User::where('id','<>',1)
    				->with(['absence' => function ($query) use ($dat,$dat2){
   							 $query->where(function($query) use ($dat,$dat2){
   							 	$query->where('end', '<=', $dat)->where('start','>=',$dat2);
   							 })
   							 ->orWhere(function($query) use ($dat,$dat2){
   							 	$query->where('end', '>=', $dat)->where('start','<=',$dat2);
   							 });
			}])
			->with(['events' => function ($query) use ($dat,$dat2){
   							 $query->where(function($query) use ($dat,$dat2){
   							 	$query->where('end', '<=', $dat)->where('start','>=',$dat2);
   							 })
   							 ->orWhere(function($query) use ($dat,$dat2){
   							 	$query->where('end', '>=', $dat)->where('start','<=',$dat2);
   							 });
			}])
			
			->get()->toJson();
			
    		return $data;
   
    	}
		if($request->input('all')=='all'&&($request->isJson()||$request->ajax())){
			 $data = Event::find($id);
			 $category=Equipment::all()->unique('type')->pluck('type');
			 $users = User::where('id','<>',1)->get();
			 return response()->json(array('data'=>$data,'category'=>$category,'users'=>$users));
		}	
        $data = Event::find($id);
		$category=Equipment::all()->unique('type')->pluck('type');
		$users = User::where('id','<>',1)->get();
		return view('event.single')->with('data',$data)->with('category',$category)->with('users',$users);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         if (Gate::denies('add_events')) {
            abort(403);
        }
		 $data = Event::find($id);
		 $start=strtotime($data->start);
		 $data->start_day=date("d",$start);
		 $data->start_month=date("m",$start)-1;
		 $data->start_year=date("Y",$start);	
		 $data->start_minutes=date("i",$start);
		 $data->start_hour=date("H",$start);	
		 $end=strtotime($data->end);
		 $data->end_day=date("d",$end);
		 $data->end_month=date("m",$end)-1;
		 $data->end_year=date("Y",$end);	
		 $data->end_minutes=date("i",$end);
		 $data->end_hour=date("H",$end);	
		 $check=strtotime($data->check);
		 $data->check_day=date("d",$check);
		 $data->check_month=date("m",$check)-1;
		 $data->check_year=date("Y",$check);	
		 $data->check_minutes=date("i",$check);
		 $data->check_hour=date("H",$check);			
        return view('event.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('add_events')) {
            abort(403);
        }
		$this->validate($request, [
            'name' => 'required|max:255|', 
        ]);
		
		$event = Event::find($id);
	    $event->name = $request->input('name');
		$event->description = $request->input('description');
		$event->localization = $request->input('localization');
		$event->type = $request->input('type');
		($request->input('confirmation')==1) ? $event->confirmation=$request->input('confirmation') : $event->confirmation=0;
		if($request->input('all_day')==1){
		$event->start=$request->input('start_year').'-'.($request->input('start_month')+1).'-'.$request->input('start_day').' 00:00:00';
		$event->end=$request->input('end_year').'-'.($request->input('end_month')+1).'-'.$request->input('end_day').' 23:59:59';
	    }
		else{
 		$event->start=$request->input('start_year').'-'.($request->input('start_month')+1).'-'.$request->input('start_day').' '.$request->input('start_hour').':'.$request->input('start_minutes').':00';
		$event->end=$request->input('end_year').'-'.($request->input('end_month')+1).'-'.$request->input('end_day').' '.$request->input('end_hour').':'.$request->input('end_minutes').':00';
		}
		$event->check=$request->input('check_year').'-'.($request->input('check_month')+1).'-'.$request->input('check_day').' '.$request->input('check_hour').':'.$request->input('check_minutes').':00';
		
		$save = $event->save();
		if($save){
			Gc::update($event,'events_show',$request->input('all_day'));
			History::add('event_edit',$event->id);
		}
		if(($request->isJson()||$request->ajax())){
				return response()->json(array('code'=>true));
			}
		($save) ? $a='Zapisano poprawnie' : $a='Wystąpił błąd';
        return redirect('/events/'.$id)->with('info',$a);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('add_events')) {
            abort(403);
        }
		$event= Event::find($id);
		
		$event->users()->delete();
		$event->add_equipment()->delete();
		History::add('event_del',$event->id);
 		$del=$event->delete();
		//if($del){
		//	AlertsController::store($event->id,'events_show','app','now','del_event');
		//}
		Gc::delete($event);
		($del) ? $a='Usunięto wydarzenie' : $a='Wystąpił błąd';
        return redirect('/events_list')->with('info',$a);
    }
	//equipment
	 public function update_eq(Request $request, $id)
    {
    	if(!empty($request->input('pivot_id'))&&($request->isJson()||$request->ajax())){
    		if (Gate::denies('equipment_to_event')) {
           return false;
        	}
			$data=Add_equipment::find($request->input('pivot_id'));
			 $data->count=$request->input('count');
			 $save=$data->save();
		}	
    	if(!empty($request->input('id'))&&($request->isJson()||$request->ajax())){
         if (Gate::denies('check_equipment_to_event',$id)) {
           return false;
        	}
		 $save=false;
		 if($request->input('check_before')===0||$request->input('check_before')===1){
		 	$data=Add_equipment::find($request->input('id'));
			 $data->check_before=$request->input('check_before');
			 $save=$data->save();
			 
		 }
		 if($request->input('check_after')===0||$request->input('check_after')===1){
		 	$data=Add_equipment::find($request->input('id'));
			 $data->check_after=$request->input('check_after');
			 $save=$data->save();
		 }
		
		  	($request->input('count_after')=='') ? $count_after=null : $count_after=$request->input('count_after');
		 	$data=Add_equipment::find($request->input('id'));
			 $data->count_after=$count_after;
			 $save=$data->save();
		 
		 ($save) ? $a='Zmieniono' : $a='Wystąpił błąd';
        return $a;
		}
    }
	public function add_eq($id, Request $request)
    {
    	if(!empty($request->input('count'))&&($request->isJson()||$request->ajax())){
         if (Gate::denies('equipment_to_event')) {
           return false;
        	}

		$save=Event::find($id)->equipment()->attach($request->input('id'),['count' => $request->input('count')]);
		 ($save==null) ? $a=1 : $a=0;
        return $a;
		}
	}
	public function add_eq_out($id, Request $request)
    {
    	if(!empty($request->input('count'))&&($request->isJson()||$request->ajax())){
         if (Gate::denies('equipment_to_event')) {
           return false;
        	}
		$eq=new Add_equipment;
		$eq->event_id=$id;
		$eq->oder=$request->input('name');	
		$eq->count=$request->input('count');	
		$save=$eq->save();
		 ($save) ? $a=1 : $a=0;
        return $a;
		}
	}
	public function del_eq($id, Request $request)
    {
    	if(!empty($request->input('id'))&&($request->isJson()||$request->ajax())){
         if (Gate::denies('equipment_to_event')) {
           return false;
        	}

		$save=Add_equipment::where('id',$request->input('id'))->delete();
		 ($save==null) ? $a=1 : $a=0;
        return $save;
		}
	}
    // confirmation 
    public function confirmation($id, Request $request){
    	 if (Gate::denies('add_events')) {
            abort(403);
        }
		$event=Event::find($id);
		$event->confirmation = true;
		$save=$event->save();
		if($save){
			AlertsController::store($event->id,'events_show','app','now','confirm_event');
			Gc::update($event,'events_show');
			History::add('event_confirm',$event->id);
			if(($request->isJson()||$request->ajax())){
				return response()->json(array('code'=>true));
			}
		}
		return redirect('/events/'.$id);
    }
	//events users
	public function users($id, Request $request){
		 
		 //add
		 if($request->input('users')=='add'&&($request->isJson()||$request->ajax())){
         if (Gate::denies('users_to_event')) {
           return false;
        	}

		$event=Event::find($id);
		$save=$event->users()->attach($request->input('id'),['function' => $request->input('func')]);
		//alerts
		AlertsController::store($id,$request->input('id'),'app','now','add_to_event');
		AlertsController::store($id,$request->input('id'),'mail','now','add_to_event');
		 ($save) ? $a=1 : $a=0;
        return $save;
		}
		//del
		 if($request->input('users')=='del'&&($request->isJson()||$request->ajax())){
         if (Gate::denies('users_to_event')) {
           return false;
        	}

		$event=Event::find($id);
		$save=$event->users()->detach($request->input('id'));
		//alerts
		AlertsController::store($id,$request->input('id'),'app','now','del_from_event');
		 ($save) ? $a=1 : $a=0;
        return $a;
		}
		 //user potwierdza udział
		 if($request->input('user')=='ok'&&($request->isJson()||$request->ajax())){
  		$event=Event::find($id);
		$user=Auth::user();
		$func=$event->users()->where('user_id',$user->id)->first()->pivot->function;
		
		$event->users()->detach($user->id);
		$save=$event->users()->attach($user->id,['check' => 1,'function'=>$func]);
		Gc::update($event,'events_show');
	
		//alerts
		AlertsController::store($id,'users_to_event','app','now','user_ok_on_event,'.$user->name);
		 ($save) ? $a=1 : $a=0;
        return $a;
		}
		 //user nie potwierdza udział
		 if($request->input('user')=='no'&&($request->isJson()||$request->ajax())){
  		$event=Event::find($id);
		$user=Auth::user();
			$func=$event->users()->where('user_id',$user->id)->first()->pivot->function;
		$event->users()->detach($user->id);	 
		$save=$event->users()->attach($user->id,['check' => 0,'function'=>$func]);
	
		//alerts
		AlertsController::store($id,'users_to_event','app','now','user_no_on_event,'.$user->name);
		 ($save) ? $a=1 : $a=0;
        return $a;
		}
		return false; 
	} 
	public function riders($id, Request $request){
		//show
		 if($request->input('rider')=='all'&&($request->isJson()||$request->ajax())){
		 	if (Gate::denies('riders_show')) {
           return false;
        	}
			$data=Event::find($id)->riders;
			return response()->json($data);
			
		}
		//add
		 if($request->input('rider')=='add'&&($request->isJson()||$request->ajax())){
         if (Gate::denies('riders')) {
           return false;
        	}
		//towrzenie rideru
		$event=Event::find($id);
		$data= new Rider;
		$data->name=$request->input('name');
		$data->type=$request->input('type');
		$data->file_url=$request->input('url');
		$save=$data->save();
		//dolaczanie rideru
		$save=$event->riders()->attach($data->id);
		//alerts
		AlertsController::store($id,'riders_show','app','now','add_rider_to_event');
		History::add('rider_add',$event->id,$data->name);
		 ($save) ? $a=1 : $a=0;
        return $a;
		}
		//delete
		  if($request->input('rider')=='del'&&($request->isJson()||$request->ajax())){
         if (Gate::denies('riders')) {
           return false;
        	}
		$data=Event::find($id);
		History::add('rider_del',$data->id);
		$save=$data->riders()->detach($request->input('id'));
		 ($save) ? $a=1 : $a=0;
        return $a;
		}
		
	}
	public function comments($id,Request $request){
		 if (Gate::denies('comments')) {
		           return false;
		    }
		 if($request->input('comments')=='show'&&($request->isJson()||$request->ajax())){
	  		$event=Event::find($id);
			$comments=$event->comments()->orderby('created_at','desc')->with('user')->get()->toJson();
	        return $comments;
		}
		if($request->input('comments')=='add'&&($request->isJson()||$request->ajax())){
			$event=Event::find($id);
			$comments=$event->comments()->create(array('title'=>$request->input('title'),'description'=>$request->input('description'),'user_id'=>Auth::user()->id));
			AlertsController::store($id,'comment','app','now','add_comment');
			($comments) ? $a=1 : $a=0;
       		return $a;
		}
		if($request->input('comments')=='del'&&($request->isJson()||$request->ajax())){
			$event=Event::find($id);
			$comments=$event->comments()->find($request->input('id'))->delete();
			($comments) ? $a=1 : $a=0;
       		return $a;
		}
		
	}
    //pdf
    public function pdf($id,$type,Request $request){
    	if($type=='all'){
    	$event=Event::find($id);
		$data=array('event'=>$event);	
		History::add('pdf',$event->id);
    	$pdf = PDF::loadView('pdf.all', $data);
		return $pdf->download($event->name.'.pdf');
		}
		if($type=='eq'){
		$event=Event::find($id);
		$data=array('event'=>$event,'cat'=>$request->input('cat'));	
		History::add('pdf',$event->id);
    	$pdf = PDF::loadView('pdf.eq', $data);
		return $pdf->download($event->name.'_sprzet.pdf');
		
		}
    }
}
