<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gate;
use Auth;
use App\Timework;
use App\Event;
use App\User;
use App\History;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class TimeworkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::denies('timeworks')) {
            abort(403);
        }
		$users=User::where('type','godziny')->orwhere('type','dni')->with('timework')->get();
		$data=null;
		$i=0;
		foreach($users as $user){
			$sum=0;
			$data[$i]['name']=$user->name;
			$data[$i]['type']=$user->type;
			$data[$i]['id']=$user->id;
			foreach($user->timework as $a){
				$start=new Carbon($a->start);
				$end=new Carbon($a->end);
				$diff=$start->diff($end);
				if($data[$i]['type']=='godziny'){
					$a->hours=($diff->d*24)+($diff->h);
					$sum=$sum+$a->hours;
				}
				else{
					$b=($diff->d*24)+($diff->h);
					$a->hours=ceil($b/24);
					$sum=$sum+$a->hours;
				}	
			}
			$data[$i]['time']=$sum;
			$i++;
		}
		if($request->isJson()||$request->ajax()){
			return response()->json($data);
		}
		return view('timework/index')->with('users',$data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('check_users_on_event',$request->input('event_id'))&&Gate::denies('timework_self')) {
            abort(403);
        }
		//add time with events 
		 if($request->input('timeworks')=='add_all_one'&&($request->isJson()||$request->ajax())){
        	$user=User::find($request->input('user_id'));
			$event=Event::find($request->input('event_id'));
			$data=new Timework; 
			$data->user_id=$user->id;
			$data->event_id=$event->id;
			$data->start=$event->start;
			$data->end=$event->end;
			$save=$data->save();
			History::add('timework_add',null,$user->name);
			($save) ? $a=1 : $a=0;
       		return $a;
		}

		//add time with events , part time
		 if($request->input('timeworks')=='add_spec_one'&&($request->isJson()||$request->ajax())){
        	$user=User::find($request->input('user_id'));
			$event=Event::find($request->input('event_id'));
			$data=new Timework; 
			$data->user_id=$user->id;
			$data->event_id=$event->id;
			$data->start=$request->input('start');
			$data->end=$request->input('end');
			$save=$data->save();
			History::add('timework_add',null,$user->name);
			($save) ? $a=1 : $a=0;
       		return $a;
		}	
        
		//add time with events for all users
		 if($request->input('timeworks')=='add_all_all'&&($request->isJson()||$request->ajax())){
		 	$event=Event::find($request->input('event_id'));
        	$user=User::where(function($q){
        		$q->where('type','godziny')->orwhere('type','dni');
        	})->whereHas('events', function ($query) use ($event){
   				 $query->where('events.id' ,$event->id)->where('event_user.check','<>',0);
			})->get();
			//return $user;
			foreach($user as $user_one){
			$data=new Timework; 
			$data->user_id=$user_one->id;
			$data->event_id=$event->id;
			$data->start=$event->start;
			$data->end=$event->end;
			$save=$data->save();
			History::add('timework_add',null,$user_one->name);
			}
			($save) ? $a=1 : $a=0;
       		return $a;
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        if (Gate::denies('timeworks')) {
            abort(403);
        }
		if($request->input('timeworks')=='show'&&($request->isJson()||$request->ajax())){
			$type=User::find($id)->type;
        	$user=User::find($id)->timework()->with('event')->get();
			$sum=0;
			foreach($user as $a){
				$start=new Carbon($a->start);
				$end=new Carbon($a->end);
				$diff=$start->diff($end);
				if($type=='godziny'){
					$a->hours=($diff->d*24)+($diff->h);
					$sum=$sum+$a->hours;
				}
				else{
					$b=($diff->d*24)+($diff->h);
					$a->hours=ceil($b/24);
					$sum=$sum+$a->hours;
				}	
			}
		
		return response()->json(array('elements'=>$user,'sum'=>$sum));	
		}
		$data=User::find($id);
        return view('timework/one')->with('data',$data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
    	 if (Gate::denies('timeworks')&&Gate::denies('timework_self')) {
            abort(403);
        }
		//add without events 
		 if($request->input('timeworks')=='add_one'&&($request->isJson()||$request->ajax())){
        	$user=User::find($id);
			$data=new Timework; 
			$data->user_id=$user->id;
			$data->start=$request->input('start');
			$data->end=$request->input('end');
			$data->description=$request->input('description');
			$save=$data->save();
			History::add('timework_add',null,$user->name);
			($save) ? $a=1 : $a=0;
       		return $a;
		}
		 //del all	
        if($request->input('timeworks')=='del_all'&&($request->isJson()||$request->ajax())){
        	$data=User::find($id);
			$save=$data->timework()->delete();
			History::add('timework_del',null,$data->name);
			($save) ? $a=1 : $a=0;
       		return $a;
		}
		//del one
		 if($request->input('timeworks')=='del'&&($request->isJson()||$request->ajax())){
        	$data=User::find($id);
			$save=Timework::find($request->input('id'))->delete();
			 History::add('timework_del',null,$data->name);
			($save) ? $a=1 : $a=0;
       		return $a;
		}

    }
	public function single(Request $request){
		if (Gate::denies('timework')) {
            abort(403);
        }
		$type=Auth::user()->type;
		if($request->input('timework')=='single'&&($request->isJson()||$request->ajax())){
		$user=Auth::user()->timework()->with('event')->get();
			$sum=0;
			foreach($user as $a){
				$start=new Carbon($a->start);
				$end=new Carbon($a->end);
				$diff=$start->diff($end);
				if($type=='godziny'){
					$a->hours=($diff->d*24)+($diff->h);
					$sum=$sum+$a->hours;
				}
				else{
					$b=($diff->d*24)+($diff->h);
					$a->hours=ceil($b/24);
					$sum=$sum+$a->hours;
				}	
			}
		
		return response()->json(array('elements'=>$user,'sum'=>$sum));	
		}
		
		return view('timework.single')->with('type',$type);
	}
}
