<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gate;
use App\Absence;
use Auth;
use App\History;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AbsenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	if (Gate::denies('absence')) {
            abort(403);
        }
		if($request->input('absence')&&($request->isJson()||$request->ajax())){
			return Auth::user()->absence()->where('start','>=',date('Y-m-d 00:00:00'))->orWhere(function ($query) {
				$query->where('start','<=',date('Y-m-d 00:00:00'))->where('end', '>=', date('Y-m-d 23:59:59'))->where('user_id',Auth::user()->id);
			})->orderBy('start')->get()->toJson();
    	}
		if($request->ajax()){
			return false;
		}
        return view('absence.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('absence')) {
            abort(403);
        }
		if($request->input('end')&&$request->input('start')&&($request->isJson()||$request->ajax())){
			$user=Auth::user();
			$absence=new Absence(['start'=>$request->input('start'),'end'=>$request->input('end')]);
			$save=$user->absence()->save($absence);
			History::add('absence_add');
			($save) ? $a='Utworzono poprawnie' : $a='Wystąpił błąd';
       		 return $a;
    	}
		
		return false;
    }

    public function destroy($id)
    {
        if (Gate::denies('absence')) {
            abort(403);
        }	
        $save=Absence::find($id)->delete();
		History::add('absence_del');
		($save) ? $a='Usunięto' : $a='Wystąpił błąd';
       		 return $a;
    }
}
