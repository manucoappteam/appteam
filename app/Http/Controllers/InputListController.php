<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\InputList;
use Gate;
use App\Event;
use App\History;
use PDF;

class InputListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::denies('inputlist_show')) {
           abort(403);
        	}
		if($request->has('event_id')){
    		$data=Event::find($request->input('event_id'))->inputlist;
			return response()->json($data);
        	}
		
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('inputlist.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      	if (Gate::denies('inputlist')) {
           abort(403);
        	}	
      	$this->validate($request, [
            'name' => 'required|max:255|', 
        ]);
		if(($request->isJson()||$request->ajax())){
			$channels=$request->input('inputlists');
			$tab=array();
			foreach($channels as $a){
				$tab[count($tab)]=$a['text'];
			}
			$channels = implode(';', $tab);
			$save = InputList::create(['name' => $request->input('name'),'event_id'=>$request->input('event_id'),'content'=>$channels]);
			AlertsController::store($request->input('event_id'),'inputlist_show','app','now','add_inputlist_to_event');
			return 1;
		}
        $channels = $request->except(['name','event_id','_token']);
		$channels = implode(';', $channels);
		$save = InputList::create(['name' => $request->input('name'),'event_id'=>$request->input('event_id'),'content'=>$channels]);
		History::add('inputlist_add',$request->input('event_id'));
		AlertsController::store($request->input('event_id'),'inputlist_show','app','now','add_inputlist_to_event');
		($save) ? $a='Utworzono poprawnie inputlistę' : $a='Wystąpił błąd';
		return redirect('/events/'.$request->input('event_id'))->with('info',$a);;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
    	if (Gate::denies('inputlist_show')) {
           abort(403);
        	}
		if(($request->isJson()||$request->ajax())){
			 $data=InputList::find($id);
			$list=explode(';', $data->content);
			return response()->json($list);
		}
        $data=InputList::find($id);
        return view('inputlist/show')->with('data',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('inputlist')) {
           abort(403);
        	}
		if($request->input('sort')=='sort'&&($request->isJson()||$request->ajax())){
			 $data=InputList::find($id);
			$data->content=$request->input('content');
			History::add('inputlist_edit');
			$save=$data->save();
			($save) ? $a= 'Pomyślnie zmieniniono kolejność' : $a= "Wystąpił błąd";
			return $a;
		}
		
		if($request->input('add')=='add'&&($request->isJson()||$request->ajax())){
			 $data=InputList::find($id);
			$data->content=$request->input('content');
			History::add('inputlist_edit');
			$save=$data->save();
			($save) ? $a= 'Pomyślnie zapisano' : $a= "Wystąpił błąd";
			return $a;
		}

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=InputList::find($id);
		History::add('inputlist_del');
		$data->delete();
    }
	public function pdf($id){
		$input=Inputlist::find($id);
		$data=array('input'=>$input);	
    	$pdf = PDF::loadView('pdf.input', $data);
		return $pdf->download($input->name.'.pdf');
	}
}
