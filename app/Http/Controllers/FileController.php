<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
class FileController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function save(Request $request)
	{
		 // Grab our files input
    $files = $request->file('files');
    // We will store our uploads in public/uploads/basic
    $assetPath = '/upload';
    $uploadPath = storage_path($assetPath);
    // We need an empty arry for us to put the files back into
    $results = array();
    foreach ($files as $file) {
        // store our uploaded file in our uploads folder
        $file->move($uploadPath, $file->getClientOriginalName());
        // set our results to have our asset path
        $name = $assetPath . '/' . $file->getClientOriginalName();
        $results[] = compact('name');
    }

    // return our results in a files object
    return array(
        'files' => $results
		
    );
	}
	public function open($name){
		return response()->download(storage_path('upload/'.$name));
	}

}
