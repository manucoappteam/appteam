<?php

namespace App\Http\Controllers;
use App\User;
use App\Roles;
use App\Event;
use Auth;
use Gate;
use Illuminate\Http\Request;
class Home extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function start(Request $request){
		
		if (Gate::denies('show_events')) {	
			$events_today=null;
			$events_week=null;
		}
		else{
			$events_today=Event::where(function($query){
   							 	$query->where('end', '<=', date('Y-m-d 23:59:59'))->where('start','>=',date('Y-m-d 00:00:00'));
   							 })
   							 ->orWhere(function($query){
   							 	$query->where('end', '>=', date('Y-m-d 23:59:59'))->where('start','<=',date('Y-m-d 00:00:00'));
   							 })->where('deleted_at',null)->with('users')->get();
			$events_week=Event::where(function($query){
   							 	$query->where('start', '>=', date('Y-m-d 23:59:59'))->where('start','<=',date('Y-m-d 00:00:00',strtotime('+7 days', time())));
   							 })->where('deleted_at',null)->with('users','inputlist','riders','add_equipment')->get();				 
		}					 
		$events_today_my=Event::where(function($query){
								$query->where(function($query){
   							 	$query->where('end', '<=', date('Y-m-d 23:59:59'))->where('start','>=',date('Y-m-d 00:00:00'));
   							 })
   							 ->orWhere(function($query){
   							 	$query->where('end', '>=', date('Y-m-d 23:59:59'))->where('start','<=',date('Y-m-d 00:00:00'));
   							 });
							})->where('deleted_at',null)->whereHas('users', function ($query) {
								    $query->where('users.id', Auth::user()->id)->where('check',1);
								})->get();
		$events_week_my=Event::where(function($query){
   							 	$query->where('start', '>=', date('Y-m-d 23:59:59'))->where('start','<=',date('Y-m-d 00:00:00',strtotime('+7 days', time())));
   							 })->where('deleted_at',null)->whereHas('users', function ($query) {
								    $query->where('users.id', Auth::user()->id)->where('check',1);
								})->get();
		$events_for_check=Event::whereHas('users', function ($query) {
								    $query->where('users.id', Auth::user()->id)->where('check',null);
								})->where('deleted_at',null)->get();																			 
		if (Gate::denies('add_events')) {
			$events_wait=null;
		}
		else{
			$events_wait=Event::where('confirmation',false)->where('start','>=',date('Y-m-d 00:00:00'))->orderBy('start')->where('deleted_at',null)->get();
		}
		
		
		
        return view('start',compact('events_today','events_today_my','events_wait','events_week','events_week_my','events_for_check'));
		
	}
	
}
