<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Equipment;
use App\History;
use Gate;
use App\Event;
use App\EquipmentFile;

class EquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	 if (Gate::denies('show_equipment')) {
            abort(403);
        }
    	if($request->input('category')&&($request->isJson()||$request->ajax())){
    		if($request->input('category')=='all')
			return Equipment::with(['status' => function ($query) {
   							 $query->where(function($query){$query->where('end', '<=', date('Y-m-d 24:00:00'))->orWhere('start','<=',date('Y-m-d 00:00:00'));})->where(function($query){
   							 	$query->where('check_after', '=', 0)->orWhereNotNull('count_after');
   							 })->orderBy('end');
			}])->get()->toJson();
    	}
		if($request->input('eq_event')&&$request->input('eq_eq')&&($request->isJson()||$request->ajax())){
    		if($request->input('eq_event')>=1&&$request->input('eq_eq')>=1){
				$dat = Event::find($request->input('eq_event'))->pluck('end');
				$dat2 = Event::find($request->input('eq_event'))->pluck('start');
			 $result=Equipment::where('id',$request->input('eq_eq'))->with(['status' => function ($query) use ($dat,$dat2) {
   							 $query->where('end', '<=', $dat)->orWhere('start','>=',$dat2)->orderBy('end');
			}])->get()->toJson();
			return $result;
    	}
		}
		if($request->input('eq_event')&&($request->isJson()||$request->ajax())){
    		if($request->input('eq_event')>=1){
				$dat = Event::find($request->input('eq_event'))->pluck('end');
				$dat2 = Event::find($request->input('eq_event'))->pluck('start');
			 $result=Equipment::with(['status' => function ($query) use ($dat,$dat2) {
   							 $query->where('end', '<=', $dat)->orWhere('start','>=',$dat2)->orderBy('end');
			}])->get()->toJson();
			return $result;
    	}
		}
		
		if($request->ajax()||$request->ajax()){
			return response()->json(Equipment::all()->unique('type')->pluck('type'));
		}
        $data=Equipment::with('status')->get();
		$category=Equipment::all()->unique('type')->pluck('type');
		return view('equipment.index')->with('data',$data)->with('category',$category);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	if (Gate::denies('add_equipment')) {
            abort(403);
        }	
    	$category=Equipment::all()->unique('type')->pluck('type');
        return view('equipment.add')->with('category',$category);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('add_equipment')) {
            abort(403);
        }
	   $this->validate($request, [
            'name' => 'required|max:255|',
            'description' => 'max:255',
           	'count' => 'int|min:1',
           	'type'=>'required'
        ]);
		
		$inputs=$request->all();
		($inputs['type']=='add') ? $inputs['type']=$inputs['other_type'] : $inputs['type']=$inputs['type'];
		if($inputs['description']==null) $inputs['description'] = '';
		if(empty($inputs['count'])) $inputs['count']=1;
 		$save=Equipment::create($inputs);
		History::add('equipment_add',null,$save->name);
		if($request->ajax()||$request->ajax()){
			return response()->json(array('code'=>true));
		}
		($save) ? $a='Utworzono poprawnie' : $a='Wystąpił błąd';
        return redirect('/equipment')->with('info',$a);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
         if (Gate::denies('show_equipment')) {
            abort(403);
        }
		 if($request->input('json')&&($request->isJson()||$request->ajax())){
			return Equipment::where('id',$id)->with(['status' => function ($query) {
   							 $query->where('end', '<=', date('Y-m-d 24:00:00'))->orWhere('start','<=',date('Y-m-d 00:00:00'))->orderBy('end');
			}])->get()->toJson();
    	}
		
		if($request->ajax()){
			return false;
		}
		$data=Equipment::find($id);	
        return view('equipment.single')->with('data',$data);
		 
    }
	public function files($id, Request $request){
		
		//show
		 if($request->input('files')=='all'&&($request->isJson()||$request->ajax())){
		 	 if (Gate::denies('show_equipment')) {
		            abort(403);
		        }
			$data=Equipment::find($id)->files;
			return response()->json($data);
			
		}
		//add
		 if($request->input('files')=='add'&&($request->isJson()||$request->ajax())){
        if (Gate::denies('edit_equipment')) {
            abort(403);
        }
		 $this->validate($request, [
            'file_url' => 'required|max:255|',
            'name' => 'required|max:255',
        ]);
		$inputs=$request->except('files','_token');
		$save=Equipment::find($id)->files()->create($inputs);
		 ($save) ? $a=1 : $a=0;
        return $a;
		}
		//delete
		  if($request->input('files')=='del'&&($request->isJson()||$request->ajax())){
         if (Gate::denies('edit_equipment')) {
            abort(403);
        }
		 $save=EquipmentFile::find($request->input('id'))->delete();
		 ($save) ? $a=1 : $a=0;
        return $a;
		}
	}
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	 if (Gate::denies('edit_equipment')) {
            abort(403);
        }
    	$category=Equipment::all()->unique('type')->pluck('type');
       $data=Equipment::find($id);	
        return view('equipment.edit')->with('equipment',$data)->with('category',$category);;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         if (Gate::denies('edit_equipment')) {
            abort(403);
        }
	   $this->validate($request, [
            'name' => 'required|max:255|',
            'description' => 'max:255',
           	'count' => 'int|min:1',
           	'type'=>'required'
        ]);
		
		$eq = Equipment::find($id);
		$inputs=$request->all();
		($inputs['type']=='add') ? $inputs['type']=$inputs['other_type'] : $inputs['type']=$inputs['type'];
		if(empty($inputs['count'])) $inputs['count']=1;
 		$save=$eq->update($inputs);
		History::add('equipment_edit',null,$eq->name);
		if($request->ajax()||$request->ajax()){
			return response()->json(array('code'=>true));
		}
		($save) ? $a='Zapisano poprawnie' : $a='Wystąpił błąd';
        return redirect('/equipment/'.$id)->with('info',$a);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('delete_equipment')) {
            abort(403);
        }
		$eq = Equipment::find($id);
		$eq->status()->detach();
		History::add('equipment_del',null,$eq->name);
 		$del=$eq->delete();
		($del) ? $a='Usunięto sprzęt' : $a='Wystąpił błąd';
        return redirect('/equipment')->with('info',$a);
    }
}
