<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//homepage
Route::get('/','Home@start');
Route::get('/home','Home@start');
//logowanie
Route::get('/login', 'Auth\AuthController@getLogin');
Route::get('/auth/login', 'Auth\AuthController@getLogin');
Route::post('/login', 'Auth\AuthController@postLogin');
Route::get('/logout', 'Auth\AuthController@getLogout');
//logowanie api
Route::post('/authenticate', 'AuthenticateController@authenticate');
 //pass reset
 // Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

//admin
Route::group(['middleware' => 'auth'], function () {
//api google
Route::get('googleapi_login', 'Gc@login');
Route::get('googleapi_auth', 'Gc@auth');
Route::get('google_test', 'Gc@test');
//create user
Route::get('users/register', 'Auth\AuthController@getRegister_new');
Route::post('users/register', 'Auth\AuthController@create');
Route::delete('users/delete/{id}',['as'=>'users.destroy','uses'=>'Auth\AuthController@delete'] );
//edit, delete users
//dopisac

//users profile
Route::get('/profile/show', 'Auth\AuthController@show');

//users profile update
Route::get('/profile/edit', 'Auth\AuthController@updateProfile');
Route::put('/profile/edit', 'Auth\AuthController@updateProfile_store');
//by admin
Route::get('/users', 'Auth\AuthController@ListProfile');
Route::put('/users/{id}/edit', 'Auth\AuthController@ListProfile_edit');

//roles
Route::resource('roles', 'RolesController',['except' => ['show']]);
//history
Route::resource('history', 'HistoryController');
//events
Route::resource('events', 'EventsController');
Route::get('/events_list/{year?}/{month?}', ['as'=>'events_list','uses'=>'EventsController@index_list']);
//events confirmation 
Route::post('/events/{id}/confirmation', ['as'=>'events.confirmation','uses'=>'EventsController@confirmation']);
//equipment
Route::resource('equipment', 'EquipmentController');
Route::post('/equipment/{id}/files', 'EquipmentController@files');
//events equipment
Route::post('/events/{id}/update_eq', ['as'=>'update_eq','uses'=>'EventsController@update_eq']);
Route::post('/events/{id}/add_eq', ['as'=>'add_eq','uses'=>'EventsController@add_eq']);
Route::post('/events/{id}/add_eq_out', ['as'=>'add_eq_out','uses'=>'EventsController@add_eq_out']);
Route::post('/events/{id}/del_eq', ['as'=>'del_eq','uses'=>'EventsController@del_eq']);
//events users
Route::post('/events/{id}/users', ['as'=>'events.users','uses'=>'EventsController@users']);
//events riders
Route::post('/events/{id}/riders', ['as'=>'events.riders','uses'=>'EventsController@riders']);
//events pdf
Route::get('/events/{id}/pdf/{type}', ['as'=>'events.pdf','uses'=>'EventsController@pdf']);
//events comments
Route::post('/events/{id}/comments', ['as'=>'events.comments','uses'=>'EventsController@comments']);
//events input list
Route::resource('inputlist', 'InputListController');
//events input list pdf
Route::get('/inputlist/{id}/pdf', ['as'=>'inputlist.pdf','uses'=>'InputListController@pdf']);
//absence
Route::resource('absence', 'AbsenceController',
                ['only' => ['index', 'create','store','destroy']]);
//alerts
Route::resource('alerts', 'AlertsController',
                ['only' => ['index','destroy']]);

Route::post('/alerts/add', ['as'=>'alerts.add','uses'=>'AlertsController@add']);
Route::post('/alerts/showed', ['as'=>'alerts.showed','uses'=>'AlertsController@showed']);
Route::post('/alerts/{id}/del', ['as'=>'alerts.del','uses'=>'AlertsController@del']);
//timeworks
Route::resource('timeworks', 'TimeworkController',['only' => ['index', 'show','store','update']]);
//timework (current user)
Route::get('/timework', 'TimeworkController@single');
//save file
Route::post('/file', 'FileController@save');	
//open file
Route::get('/upload/{name}', 'FileController@open');	
});
//all api
Route::group(['prefix' => 'api'], function()
{
	//logowanie 
    Route::post('authenticate', 'AuthenticateController@authenticate');
	//api
	Route::group(['middleware' => 'jwt.auth'], function()
	{
		Route::get('/home','HomeApi@start');
		//alerts
		Route::resource('alerts', 'AlertsController',
                ['only' => ['index','destroy']]);
		Route::post('/alerts/showed', ['as'=>'alerts.showed','uses'=>'AlertsController@showed']);
		Route::post('/alerts/{id}/del', ['uses'=>'AlertsController@del']);
		Route::post('/alerts/add', ['uses'=>'AlertsController@add']);
		//profile
		Route::get('/profile/show', 'Auth\AuthController@show');
		//all_users
		Route::get('/users_all', 'Auth\AuthController@all');		
		//events
		Route::resource('events', 'EventsController');
		Route::get('/events_list/{year?}/{month?}', ['as'=>'events_list','uses'=>'EventsController@index_api']);
		//auth	
    	Route::resource('authenticate', 'AuthenticateController', ['only' => ['index']]);
		//events users
		Route::post('/events/{id}/users', ['as'=>'events.users','uses'=>'EventsController@users']);
		//events riders
		Route::post('/events/{id}/riders', ['uses'=>'EventsController@riders']);
		//events comments
		Route::post('/events/{id}/comments', ['as'=>'events.comments','uses'=>'EventsController@comments']);
		//equipment
		Route::resource('equipment', 'EquipmentController');
		//absence
		Route::resource('absence', 'AbsenceController',
                ['only' => ['index', 'create','store','destroy']]);
		//timework (current user)
		Route::get('/timework', 'TimeworkController@single');
		//timeworks
		Route::resource('timeworks', 'TimeworkController',['only' => ['index', 'show','store','update']]);		
		//events confirmation 
		Route::post('/events/{id}/confirmation', ['uses'=>'EventsController@confirmation']);
		//events equipment
		Route::post('/events/{id}/update_eq', ['uses'=>'EventsController@update_eq']);
		Route::post('/events/{id}/add_eq', ['uses'=>'EventsController@add_eq']);
		Route::post('/events/{id}/add_eq_out', ['uses'=>'EventsController@add_eq_out']);
		Route::post('/events/{id}/del_eq', ['uses'=>'EventsController@del_eq']);
		//events input list
		Route::resource('inputlist', 'InputListController');
	});
	
});

