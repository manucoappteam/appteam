<?php

namespace App\Google;
use Auth;
class Api 
{
	public function __construct(){
		$this->client=new \Google_Client();
		$this->client->setApplicationName('Google Calendar API AppTeam');
		$this->client->setAuthConfigFile(base_path('client_secret.json'));
		$this->client->addScope(\Google_Service_Calendar::CALENDAR);
		$this->client->setAccessType('offline');
		$this->url_redirect=asset('googleapi_auth');
		$this->client->setRedirectUri($this->url_redirect);
		$this->url=$this->client->createAuthUrl();
	}
 
	public function auth($code){
		return $this->client->authenticate($code);
		
	}
	public function url(){
		return $this->url;
	}
	public function start($code){
		//var_dump($this->client->getRefreshToken());
		//$this->client->setAccessToken(session('api_google'));
	//	var_dump($this->client->getRefreshToken());
		
		 if ($this->client->isAccessTokenExpired()) {
    	$this->client->refreshToken($code);
		
			 
			 
  		}
		 
			 
		return new \Google_Service_Calendar($this->client);
	}
}
