<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Add_equipment extends Model
{
    protected $table = 'equipment_event';
	 
    protected $fillable = ['event_id','equimpent_id','count','oder','check_before','check_after','count_after'];
}
