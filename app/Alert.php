<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
     protected $table = 'alerts';
	 
     protected $fillable = ['event_id', 'times', 'type','for_who','by_what'];
	 
	  public function event()
    {
        return $this->belongsTo('App\Event');
    }
	 public function users()
    {
       return $this->belongsToMany('App\User', 'alert_user', 'alert_id', 'user_id')->withPivot('chcek')->withTimestamps();
    }
}
