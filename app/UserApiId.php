<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserApiId extends Model
{
    protected $fillable = ['google_id', 'user_id','event_id'];
}
