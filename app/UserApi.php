<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserApi extends Model
{
    protected $fillable = ['key', 'user_id'];
}
