<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    //
     protected $table = 'roles';
	 
     protected $fillable = [
     		'name', 
		    'description', 
		    'users',
		    'roles',
		    'add_events',
		    'show_events',
            'add_equipment',
            'edit_equipment',
            'delete_equipment',
            'show_equipment',
            'equipment_to_event',
            'show_equipment_to_event',
            'check_equipment_to_event',
            'absence',
            'users_to_event',
            'check_users_on_event',
            'riders',
            'riders_show',
            'timework',
            'timeworks',
            'inputlist',
            'inputlist_show',
            'history',
            'timework_self',
            'comments'];
	 
	 /*users*/
	 public function users()
    {
        return $this->hasMany('App\User','id', 'role');
    }
}
