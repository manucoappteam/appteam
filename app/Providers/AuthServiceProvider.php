<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Event;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);
		
		$gate->before(function ($user, $ability) {
		    if ($user->id==1&&$user->roles->name==='root'&&$user->roles->id==1) {
		        return true;
		    }
		});

        $gate->define('users', function ($user) {
        	
            return $user->roles->users==1;
        });
		 $gate->define('roles', function ($user) {
        	
            return $user->roles->roles==1;
        });
		 $gate->define('users_del', function ($user) {
        	
            return false;
        });
		$gate->define('add_events', function ($user) {
        	
            return $user->roles->add_events==1;
        });
		$gate->define('show_events', function ($user, $event=null) {
        		
           	if($event==null){
        	if($user->roles->show_events==1)
           		return true;
			}
			else{
			if($user->roles->show_events==1)
           		return true;	
			foreach(Event::find($event)->users as $a_user){
				return $a_user->id==$user->id;
			}
			}
		   
        });
		//equipment
		$gate->define('add_equipment', function ($user) {
        	
            return $user->roles->add_equipment==1;
        });
		$gate->define('edit_equipment', function ($user) {
        	
            return $user->roles->edit_equipment==1;
        });
		$gate->define('delete_equipment', function ($user) {
        	
            return $user->roles->delete_equipment==1;
        });
		$gate->define('show_equipment', function ($user) {
        	
            return $user->roles->show_equipment==1;
        });
		$gate->define('equipment_to_event', function ($user) {
        	
            return $user->roles->equipment_to_event==1;
        });
		$gate->define('show_equipment_to_event', function ($user,$event) {
        	//dodatkowo szef ekipy
        	if($user->roles->show_equipment_to_event==1)
            return true;
			foreach(Event::find($event)->users as $a_user){
				return $a_user->pivot->function=='Szef ekipy';
			}
        });
		$gate->define('check_equipment_to_event', function ($user,$event) {
        	//dodatkowo szef ekipy
        	if($user->roles->check_equipment_to_event==1)
            return true;
			foreach(Event::find($event)->users as $a_user){
				return $a_user->pivot->function=='Szef ekipy';
			}
        });
		//uzytkownicy nieobecnosci planowane
		$gate->define('absence', function ($user) {
        	
            return $user->roles->absence==1;
        });
		//event ekipa
		$gate->define('users_to_event', function ($user) {
        	
            return $user->roles->users_to_event==1;
        });
		$gate->define('check_users_on_event', function ($user,$event) {
        	//dodatkowo szef ekipy
            if($user->roles->check_users_on_event==1)
			return true;
			foreach(Event::find($event)->users as $a_user){
				return $a_user->pivot->function=='Szef ekipy';
			}
        });
		$gate->define('riders', function ($user) {
        	
            return $user->roles->riders==1;
        });
		$gate->define('riders_show', function ($user) {
        	
             return $user->roles->riders_show==1;
        });
		$gate->define('timework', function ($user) {
        	
            return $user->roles->timework==1&&($user->type=='godziny'||$user->type=='dni');
        });
		$gate->define('timeworks', function ($user) {
        	
            return $user->roles->timeworks==1;
        });
		$gate->define('inputlist', function ($user) {
        	
           return $user->roles->inputlist==1;
        });
		$gate->define('inputlist_show', function ($user) {
        	
             return $user->roles->inputlist_show==1;
        });
		$gate->define('history', function ($user) {
        	
             return $user->roles->history==1;
        });
		$gate->define('timework_self', function ($user) {
        	
             return $user->roles->timework_self==1;
        });
		$gate->define('comments', function ($user) {
        	
             return $user->roles->comments==1;
        });
    }
}
