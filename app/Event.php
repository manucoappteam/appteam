<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Event extends Model
{
	 use SoftDeletes;
	 
  	 protected $table = 'events';
	 
     protected $fillable = ['name', 'description', 'type','localization','input_list','start','end','check','confirmation','rider'];
	 
	 protected $dates = ['deleted_at'];
	 
	 public function riders()
    {
        return $this->belongsToMany('App\Rider', 'event_rider', 'event_id', 'rider_id');
    } 
	 public function users()
    {
       return $this->belongsToMany('App\User', 'event_user', 'event_id', 'user_id')->withPivot('check', 'function')->withTimestamps();
    }
	public function alerts()
    {
       return $this->hasMany('App\Alert', 'event_id', 'id');
    }
	public function equipment()
    {
       return $this->belongsToMany('App\Equipment', 'equipment_event', 'event_id', 'equipment_id')->withPivot('id','count', 'oder','check_before','check_after','count_after')->withTimestamps();   	
	   
    }
	public function add_equipment()
	{
		return $this->hasMany('App\Add_equipment', 'event_id', 'id');
	}
	public function inputlist(){
		return $this->hasMany('App\InputList','event_id','id');
	}
	public function comments(){
		return $this->hasMany('App\Comment','event_id','id');
	}

}
