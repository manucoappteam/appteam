<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);
		DB::table('roles')->insert([
			'name'=>'root',
			'description'=>'the most imporant users',
			'updated_at'=>date("Y-m-d H:i:s"),
			'created_at'=>date("Y-m-d H:i:s")
        ]);
		DB::table('users')->insert([
			'name'=>'root',
			'email'=>'admin@admin.pl',
			'password'=>Hash::make('12345'),
			'role'=>1,
			'updated_at'=>date("Y-m-d H:i:s"),
			'created_at'=>date("Y-m-d H:i:s")
        ]);
        Model::reguard();
    }
}
