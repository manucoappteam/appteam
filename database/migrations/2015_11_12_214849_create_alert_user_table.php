<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alert_user', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('alert_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->boolean('chcek');
            $table->timestamps();
			$table->foreign('alert_id')->references('id')->on('alerts');
			$table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alert_user');
    }
}
