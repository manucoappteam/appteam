<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->string('description');
			$table->string('type');
			$table->boolean('confirmation');
			$table->string('localization');
			$table->string('input_list');
			$table->dateTime('start');
			$table->dateTime('end');
			$table->dateTime('check');
			$table->softDeletes();
			$table->integer('user_id')->unsigned()->nullable();
            $table->timestamps();
			$table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
