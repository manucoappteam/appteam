<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histories', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('user_id')->unsigned()->nullable();
			$table->integer('event_id')->unsigned()->nullable();
			$table->integer('type_id')->unsigned()->nullable();
            $table->timestamps();
			$table->string('description');
			$table->softDeletes();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
			$table->foreign('event_id')->references('id')->on('events')->onDelete('set null');
			$table->foreign('type_id')->references('id')->on('history_types')->onDelete('set null');
		});	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('histories');
    }
}
