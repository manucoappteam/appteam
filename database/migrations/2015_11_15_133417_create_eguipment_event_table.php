<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEguipmentEventTable extends Migration
{
  public function up()
    {
        Schema::create('equipment_event', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('event_id')->unsigned();
			$table->integer('equipment_id')->unsigned()->nullable();
			$table->string('oder')->nullable();
			$table->integer('count');
			$table->boolean('check_before');
			$table->boolean('check_after');
			$table->integer('count_after')->nullable();
			$table->foreign('event_id')->references('id')->on('events');
			$table->foreign('equipment_id')->references('id')->on('equipment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('equipment_event');
    }
}
