<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name')->unique();
			$table->string('description');
			$table->boolean('users');
			$table->boolean('roles');
			$table->boolean('show_events');
			$table->boolean('add_equipment');
			$table->boolean('edit_equipment');
			$table->boolean('delete_equipment');
			$table->boolean('show_equipment');
			$table->boolean('equipment_to_event');
			$table->boolean('show_equipment_to_event');
			$table->boolean('check_equipment_to_event');
			$table->boolean('absence');
			$table->boolean('users_to_event');
			$table->boolean('check_users_on_event');
			$table->boolean('riders');		
			$table->boolean('riders_show');
			$table->boolean('timework');
			$table->boolean('timeworks');
			$table->boolean('inputlist');	
			$table->boolean('inputlist_show');					
			$table->boolean('add_events');
			$table->boolean('history');
			$table->boolean('timework_self');
			$table->boolean('comments');
			$table->boolean('want_event');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roles');
    }
}
