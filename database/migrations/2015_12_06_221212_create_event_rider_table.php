<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventRiderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_rider', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('event_id')->unsigned();
			$table->integer('rider_id')->unsigned();
            $table->timestamps();
			$table->foreign('event_id')->references('id')->on('events');
			$table->foreign('rider_id')->references('id')->on('riders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_rider');
    }
}
